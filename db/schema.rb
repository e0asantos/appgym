# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.2].define(version: 2025_02_24_160720) do
  create_table "accesses", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "user_id"
    t.integer "establecimiento_id"
    t.integer "level_access"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "adduser"
    t.integer "addestablecimiento"
    t.string "networkid"
  end

  create_table "asistencia", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.datetime "arrival_time", precision: nil
    t.datetime "leaving_time", precision: nil
    t.integer "user_id"
    t.integer "establecimiento_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "networkid"
    t.string "transition"
    t.integer "activo"
    t.integer "minutos"
    t.datetime "startwod", precision: nil
    t.datetime "endwod", precision: nil
  end

  create_table "box_schedules", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "weekday"
    t.string "timetable"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "timetable_end"
    t.integer "establecimiento_id"
    t.integer "networkid"
  end

  create_table "comentarios", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "establecimiento_id"
    t.integer "wod_item_id"
    t.integer "wod_root_id"
    t.integer "usuario_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "comentario"
  end

  create_table "correos", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "titulo"
    t.text "contenido"
    t.integer "creadopor"
    t.string "usuarioscomo"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "corte_cajas", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "usuarios_id"
    t.datetime "fecha_inicio", precision: nil
    t.datetime "fecha_fin", precision: nil
    t.integer "autorizo"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "dashboards", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "user_in"
  end

  create_table "device_types", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "nombre"
    t.string "descripcion"
    t.decimal "precio", precision: 10
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "devices", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "uid"
    t.string "description"
    t.string "name"
    t.string "action"
    t.integer "post_action"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "networkid"
    t.integer "establecimiento_id"
    t.integer "pluggedas"
    t.string "token"
    t.string "secret"
  end

  create_table "diccionarios", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "termino"
    t.string "definicion"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "ejercicios", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "clip"
    t.integer "author_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "nivel"
    t.string "networkid"
    t.integer "globalavailable"
    t.string "pesorx"
    t.string "pesoescalado"
  end

  create_table "entradas", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "establecimientos_id"
    t.integer "usuarios_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "paquete"
    t.string "networkid"
    t.string "status"
  end

  create_table "entrenamientos", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "wod_id"
    t.integer "ejercicio_id"
    t.datetime "start_time", precision: nil
    t.datetime "end_time", precision: nil
    t.integer "user_id"
    t.integer "establecimiento_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "establecimientos", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "nombre"
    t.string "calle"
    t.string "numero"
    t.string "colonia"
    t.string "servicios"
    t.string "horario"
    t.string "descripcion"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "foto"
    t.string "coordenadas"
    t.string "openpayid"
    t.string "openpaypub"
    t.string "openpaypri"
    t.string "uniq"
    t.string "networkid"
    t.string "beacon"
    t.string "minor"
    t.string "major"
    t.string "telefono", limit: 30
    t.string "email", limit: 50
    t.integer "openpayprueba", default: 0
    t.string "uid"
  end

  create_table "eventos", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "fecha", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "imagen"
    t.string "networkdid"
    t.integer "public"
    t.string "networkid"
  end

  create_table "horarios", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "valor"
    t.string "etiqueta"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "likes", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "establecimiento_id"
    t.integer "wod_item_id"
    t.integer "wod_root_id"
    t.integer "usuario_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "mcards", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "digits"
    t.string "ccv"
    t.string "expiration"
    t.integer "usuario_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "notificaciones", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "diastopay"
    t.integer "mailtoadmin"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "networkid"
  end

  create_table "nutritions", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "appointment", precision: nil
    t.string "description"
    t.string "feedback"
    t.string "comments"
    t.integer "nutritionist_id"
    t.string "status"
    t.datetime "expire_on", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "online"
    t.string "grasa"
    t.string "azucar"
    t.string "calorias"
    t.string "proteina"
  end

  create_table "pagos", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "usuarios_id"
    t.datetime "vence", precision: nil
    t.string "cantidad"
    t.string "gateway"
    t.string "autorizacion"
    t.integer "cobrado_por"
    t.string "descuento"
    t.string "cantidad_original"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "paquete"
    t.integer "pausa"
    t.integer "restantes"
    t.string "networkid"
  end

  create_table "paquetes", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "nombre"
    t.string "precio"
    t.integer "meses"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "horario"
    t.string "horarioto"
    t.integer "inscripcion"
    t.string "networkid"
  end

  create_table "recompensa_obtenidas", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "usuarios_id"
    t.integer "rewards_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "networkid"
    t.integer "active"
  end

  create_table "rewards", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "asistencia"
    t.integer "numpagos"
    t.string "peso"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "descripcion"
    t.string "titulo"
    t.string "networkid"
  end

  create_table "sessions", charset: "utf8mb4", collation: "utf8mb4_general_ci", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "sexos", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "mtipo"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "tickets", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "tipo_cuenta", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "titulo"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "trackers", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "caracteristica"
    t.string "valor"
    t.integer "usuario_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "usuarios", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "tipo"
    t.string "nombre"
    t.string "apellido"
    t.string "email"
    t.string "calle"
    t.string "numero"
    t.string "colonia"
    t.string "password"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "peso"
    t.string "altura"
    t.datetime "nacimiento", precision: nil, default: "2014-01-01 00:00:00"
    t.string "foto"
    t.integer "numsocio"
    t.string "nfc"
    t.integer "biometric"
    t.string "notas"
    t.string "casillero"
    t.string "contactoDireccion"
    t.string "contactoEmpresa"
    t.string "contactoOcupacion"
    t.integer "paquete"
    t.string "apellidopaterno"
    t.integer "cpostal"
    t.string "sexo"
    t.string "estados"
    t.string "telefonotrabajo"
    t.string "telefonocelular"
    t.string "ciudad"
    t.integer "gymorigin"
    t.integer "isaservice", default: 0
    t.datetime "finalfecha", precision: nil
    t.integer "networkid", default: 0
  end

  create_table "wod_items", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "workout_id"
    t.integer "reps"
    t.string "time"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "wod_id"
  end

  create_table "wod_roots", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "time"
    t.datetime "date", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "usuario_id"
    t.string "networkid"
    t.integer "globalavailable"
    t.integer "likes"
    t.string "portada"
  end

  create_table "wod_schedules", id: :integer, charset: "utf8mb3", collation: "utf8mb3_general_ci", force: :cascade do |t|
    t.integer "wod_id"
    t.string "weekday"
    t.string "hour"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end
end
