class CreateDeviceTypes < ActiveRecord::Migration
  def change
    create_table :device_types do |t|
      t.string :nombre
      t.string :descripcion
      t.decimal :precio

      t.timestamps
    end
  end
end
