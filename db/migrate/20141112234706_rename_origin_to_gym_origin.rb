class RenameOriginToGymOrigin < ActiveRecord::Migration
  def self.up
    rename_column :usuarios, :origin, :gymorigin
  end

  def self.down
    # rename back if you need
  end
end

