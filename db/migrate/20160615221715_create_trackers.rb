class CreateTrackers < ActiveRecord::Migration
  def change
    create_table :trackers do |t|
      t.string :caracteristica
      t.string :valor
      t.integer :usuario_id

      t.timestamps
    end
  end
end
