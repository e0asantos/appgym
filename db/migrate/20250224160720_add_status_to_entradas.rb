class AddStatusToEntradas < ActiveRecord::Migration[7.2]
  def change
    add_column :entradas, :status, :string
  end
end
