class CreatePaquetes < ActiveRecord::Migration
  def change
    create_table :paquetes do |t|
      t.integer :id
      t.string :nombre
      t.string :precio
      t.integer :meses

      t.timestamps
    end
  end
end
