class ChangeFormatInCorreos < ActiveRecord::Migration
  
  def change
    change_column :correos, :contenido, :text
  end
end
