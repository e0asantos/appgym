class CreateBoxSchedules < ActiveRecord::Migration
  def change
    create_table :box_schedules do |t|
      t.string :weekday
      t.string :timetable

      t.timestamps
    end
  end
end
