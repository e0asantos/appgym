class AddBeaconToEstablecimientos < ActiveRecord::Migration
  def change
    add_column :establecimientos, :beacon, :string
  end
end
