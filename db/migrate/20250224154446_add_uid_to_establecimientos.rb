class AddUidToEstablecimientos < ActiveRecord::Migration[7.2]
  def change
    add_column :establecimientos, :uid, :string
  end
end
