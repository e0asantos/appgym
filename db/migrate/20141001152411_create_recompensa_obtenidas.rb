class CreateRecompensaObtenidas < ActiveRecord::Migration
  def change
    create_table :recompensa_obtenidas do |t|
      t.integer :id
      t.integer :usuarios_id
      t.integer :rewards_id

      t.timestamps
    end
  end
end
