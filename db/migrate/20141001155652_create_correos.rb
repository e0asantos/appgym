class CreateCorreos < ActiveRecord::Migration
  def change
    create_table :correos do |t|
      t.integer :id
      t.string :titulo
      t.string :contenido
      t.integer :creadopor
      t.string :usuarioscomo

      t.timestamps
    end
  end
end
