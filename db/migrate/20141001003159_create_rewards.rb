class CreateRewards < ActiveRecord::Migration
  def change
    create_table :rewards do |t|
      t.integer :id
      t.integer :asistencia
      t.integer :numpagos
      t.string :peso

      t.timestamps
    end
  end
end
