class CreateNotificaciones < ActiveRecord::Migration
  def change
    create_table :notificaciones do |t|
      t.integer :id
      t.integer :diastopay
      t.integer :mailtoadmin

      t.timestamps
    end
  end
end
