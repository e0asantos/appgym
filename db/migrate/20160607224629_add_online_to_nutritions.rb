class AddOnlineToNutritions < ActiveRecord::Migration
  def change
    add_column :nutritions, :online, :integer
  end
end
