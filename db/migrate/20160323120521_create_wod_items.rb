class CreateWodItems < ActiveRecord::Migration
  def change
    create_table :wod_items do |t|
      t.integer :workout_id
      t.integer :reps
      t.string :time

      t.timestamps
    end
  end
end
