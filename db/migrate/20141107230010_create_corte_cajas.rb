class CreateCorteCajas < ActiveRecord::Migration
  def change
    create_table :corte_cajas do |t|
      t.integer :usuarios_id
      t.datetime :fecha_inicio
      t.datetime :fecha_fin
      t.integer :autorizo

      t.timestamps
    end
  end
end
