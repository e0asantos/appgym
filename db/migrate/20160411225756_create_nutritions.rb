class CreateNutritions < ActiveRecord::Migration
  def change
    create_table :nutritions do |t|
      t.integer :user_id
      t.datetime :appointment
      t.string :description
      t.string :feedback
      t.string :comments
      t.integer :nutritionist_id
      t.string :status
      t.datetime :expire_on

      t.timestamps
    end
  end
end
