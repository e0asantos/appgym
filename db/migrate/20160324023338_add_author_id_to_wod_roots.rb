class AddAuthorIdToWodRoots < ActiveRecord::Migration
  def change
    add_column :wod_roots, :author_id, :integer
  end
end
