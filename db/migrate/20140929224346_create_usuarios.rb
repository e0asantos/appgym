class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.integer :tipo
      t.string :nombre
      t.string :apellido
      t.string :email
      t.string :calle
      t.string :numero
      t.string :colonia
      t.string :password

      t.timestamps
    end
  end
end
