class CreateWodSchedules < ActiveRecord::Migration
  def change
    create_table :wod_schedules do |t|
      t.integer :wod_id
      t.string :weekday
      t.string :hour

      t.timestamps
    end
  end
end
