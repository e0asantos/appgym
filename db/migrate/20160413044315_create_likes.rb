class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.integer :establecimiento_id
      t.integer :wod_item_id
      t.integer :wod_root_id
      t.integer :usuario_id

      t.timestamps
    end
  end
end
