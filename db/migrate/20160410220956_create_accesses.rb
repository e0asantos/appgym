class CreateAccesses < ActiveRecord::Migration
  def change
    create_table :accesses do |t|
      t.integer :user_id
      t.integer :establecimiento_id
      t.integer :level_access

      t.timestamps
    end
  end
end
