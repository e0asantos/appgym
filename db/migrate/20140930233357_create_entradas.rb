class CreateEntradas < ActiveRecord::Migration
  def change
    create_table :entradas do |t|
      t.integer :id
      t.integer :establecimientos_id
      t.integer :usuarios_id

      t.timestamps
    end
  end
end
