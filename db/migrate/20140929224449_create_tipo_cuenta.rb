class CreateTipoCuenta < ActiveRecord::Migration
  def change
    create_table :tipo_cuenta do |t|
      t.integer :id
      t.string :titulo

      t.timestamps
    end
  end
end
