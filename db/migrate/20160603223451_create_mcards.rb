class CreateMcards < ActiveRecord::Migration
  def change
    create_table :mcards do |t|
      t.string :digits
      t.string :ccv
      t.string :expiration
      t.integer :usuario_id

      t.timestamps
    end
  end
end
