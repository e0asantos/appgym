class AddCoordenadasToEstablecimientos < ActiveRecord::Migration
  def change
    add_column :establecimientos, :coordenadas, :string
  end
end
