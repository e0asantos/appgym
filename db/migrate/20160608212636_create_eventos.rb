class CreateEventos < ActiveRecord::Migration
  def change
    create_table :eventos do |t|
      t.string :name
      t.string :description
      t.datetime :fecha

      t.timestamps
    end
  end
end
