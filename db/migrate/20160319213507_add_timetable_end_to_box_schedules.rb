class AddTimetableEndToBoxSchedules < ActiveRecord::Migration
  def change
    add_column :box_schedules, :timetable_end, :string
  end
end
