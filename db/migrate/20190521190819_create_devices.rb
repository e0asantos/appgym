class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :uid
      t.string :description
      t.string :name
      t.string :action
      t.integer :post_action

      t.timestamps
    end
  end
end
