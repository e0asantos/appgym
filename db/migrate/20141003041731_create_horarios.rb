class CreateHorarios < ActiveRecord::Migration
  def change
    create_table :horarios do |t|
      t.integer :id
      t.string :valor
      t.string :etiqueta

      t.timestamps
    end
  end
end
