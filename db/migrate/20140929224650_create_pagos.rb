class CreatePagos < ActiveRecord::Migration
  def change
    create_table :pagos do |t|
      t.integer :id
      t.integer :usuarios_id
      t.datetime :vence
      t.string :cantidad
      t.string :gateway
      t.string :autorizacion
      t.integer :cobrado_por
      t.string :descuento
      t.string :cantidad_original

      t.timestamps
    end
  end
end
