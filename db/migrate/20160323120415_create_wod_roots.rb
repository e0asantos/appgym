class CreateWodRoots < ActiveRecord::Migration
  def change
    create_table :wod_roots do |t|
      t.string :name
      t.string :description
      t.string :time
      t.datetime :date

      t.timestamps
    end
  end
end
