class CreateEstablecimientos < ActiveRecord::Migration
  def change
    create_table :establecimientos do |t|
      t.integer :id
      t.string :nombre
      t.string :calle
      t.string :numero
      t.string :colonia
      t.string :servicios
      t.string :horario
      t.string :descripcion

      t.timestamps
    end
  end
end
