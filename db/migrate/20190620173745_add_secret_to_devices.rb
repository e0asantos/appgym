class AddSecretToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :secret, :string
  end
end
