class CreateEjercicios < ActiveRecord::Migration
  def change
    create_table :ejercicios do |t|
      t.string :name
      t.string :description
      t.string :clip
      t.integer :author_id

      t.timestamps
    end
  end
end
