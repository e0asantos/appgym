class CreateEntrenamientos < ActiveRecord::Migration
  def change
    create_table :entrenamientos do |t|
      t.integer :wod_id
      t.integer :ejercicio_id
      t.datetime :start_time
      t.datetime :end_time
      t.integer :user_id
      t.integer :establecimiento_id

      t.timestamps
    end
  end
end
