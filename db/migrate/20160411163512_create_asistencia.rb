class CreateAsistencia < ActiveRecord::Migration
  def change
    create_table :asistencia do |t|
      t.datetime :arrival_time
      t.datetime :leaving_time
      t.integer :user_id
      t.integer :establecimiento_id

      t.timestamps
    end
  end
end
