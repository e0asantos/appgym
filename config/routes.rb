Rails.application.routes.draw do
  resources :devices

  # Redirects with constraints
  # Commented out in original; updated syntax for Rails 7
  # get "*path", to: redirect("https://www.sportfit.app/%{path}"), constraints: { protocol: "http://" }
  # get "*path", to: redirect("https://www.sportfit.app/%{path}"), constraints: { subdomain: "" }

  # Trackers
  match "/trackers/weightStats", to: "trackers#weightStats", via: [:get, :post] # Keeping match for multiple verbs
  resources :trackers

  # Eventos
  match "/events/getAll", to: "eventos#getActiveEvents", via: [:get, :post]
  resources :eventos

  resources :mcards
  resources :likes

  # Comentarios
  match "/comments/getAll", to: "comentarios#getAll", via: [:get, :post]
  resources :comentarios do # Evita generar rutas automáticas innecesarias
    member do
      delete :destroy # Ruta DELETE estándar
      post :destroy   # Permitir POST para simular DELETE
    end
  end

  # Nutritions
  match "/nutrition/appointments", to: "nutritions#getMyAppointments", via: [:get, :post]
  resources :nutritions

  resources :entrenamientos

  # Asistencia
  match "/asistencia/stats", to: "asistencia#estadistica", via: [:get, :post]
  match "/asistencia/lista", to: "asistencia#listaDeAsistencia", via: [:get, :post]
  match "/asistencia/startWod", to: "asistencia#startWod", via: [:get, :post]
  match "/asistencia/endWod", to: "asistencia#endWod", via: [:get, :post]
  match "/asistencia/activeWods", to: "asistencia#activeWod", via: [:get, :post]
  get '/asistencia/stats/montly', to: 'asistencia#monthly_chart'
  resources :asistencia

  resources :accesses
  resources :diccionarios
  resources :wod_schedules
  resources :wod_items

  # Wod_roots
  get "/wod_roots/mywods", to: "wod_roots#getMyWods"
  post "/wod_roots/getSingleWodItem", to: "wod_roots#getSingleWodItem"
  post "/wod_roots/getWodItems", to: "wod_roots#getWodItems"
  resources :wod_roots

  resources :ejercicios
  resources :box_schedules
  resources :tickets
  resources :corte_cajas

  # Dashboards
  get "/dashboards/salesInfo", to: "dashboards#salesInfo"
  get "/dashboards/salesInfo2", to: "dashboards#salesInfo2"
  get "/dashboards/salesInfo3", to: "dashboards#salesInfo3"
  post "/", to: "dashboards#dummy"
  resources :dashboards

  resources :paquetes
  resources :correos

  match "/contactme", to: "correos#contactus", via: [:get, :post]

  resources :notificaciones

  # Recompensa_obtenidas
  match "/rewards/myRewards", to: "recompensa_obtenidas#getMyRewards", via: [:get, :post]
  resources :recompensa_obtenidas

  resources :rewards
  # Entradas
  get "/entradas/salir", to: "entradas#logOut"
  post "/entradas/logWithCard", to: "entradas#logWithCard"
  get "/entradas/keypad/:uid", to: "entradas#showKeypad"
  get "/entradas/contactless/:uid", to: "entradas#showNFC"
  get "/entradas/biometric/:uid", to: "entradas#showBiometric"
  post "/entradas/logWithUser", to: "entradas#logWithUser" # Assuming GET; adjust if POST is intended
  match "/entradas/logWithKeyPad", to: "entradas#logWithKeypad", via: [:post, :options]
  resources :entradas

  resources :establecimientos
  resources :pagos
  resources :tipo_cuenta

  # Usuarios
  match "/coinLogin", to: "usuarios#coinlessLogin", via: [:get, :post]
  match "/coinRegister", to: "usuarios#coinlessRegister", via: [:get, :post]
  match "/setPrice", to: "usuarios#coinlessPrice", via: [:get, :post]
  match "/getPrice", to: "usuarios#getCoinlessPrice", via: [:get, :post]
  get "/usuarios/fakeUser", to: "usuarios#fakeUser"
  get "/usuarios/welcomeUser", to: "usuarios#welcomeUser"
  get "/usuarios/openDoor", to: "usuarios#openDoor"
  get "/usuarios/closeDoor", to: "usuarios#closeDoor"
  get "/usuarios/createToken", to: "usuarios#createToken"
  match "/usuarios/fromMobile", to: "usuarios#createMobile", via: [:post, :options]
  match "/usuarios/loginMobile", to: "usuarios#loginMobile", via: [:post, :options]
  match "/usuarios/downloadBeacons", to: "usuarios#downloadBeacons", via: [:post, :options]
  get "/usuarios/downloadPlacesFromNetwork", to: "usuarios#downloadPlacesFromNetwork"
  match "/usuarios/mostActive", to: "usuarios#mostActiveUsers", via: [:post, :get]
  get "/usuarios/stats", to: "usuarios#statsByUser"
  resources :usuarios do
    # Add this line inside the resources block if mylogin is a member action (specific to a user)
    # get 'mylogin', on: :member
    # Or as a collection action (not tied to a specific user)
    get 'login', on: :collection
  end

  post "/usuarios/buscar", to: "usuarios#buscarNombre"

  # Pagos (merged duplicate routes)
  get "/pagos/makePayment", to: "pagos#makePayment" # Original match
  post "/pagos/makePayment", to: "pagos#makePayment" # Explicit POST route
  post "/pagos/corteCaja", to: "pagos#corteCaja"
  get "/pagos/makeCardPayment", to: "pagos#makeCardPayment"
  get "/pagos/pauseUser", to: "pagos#pauseUser"
  get "/pagos/incomingFromGateway", to: "pagos#incomingFromGateway"

  # Removed legacy wildcard route comment
end