var video;
var dataURL;

function setup() {
  navigator.myGetMedia = (navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);
  navigator.myGetMedia({ video: true }, connect, error);
}

function connect(stream) {
  video = document.getElementById("cameraVideo");
  video.width = 400;
  video.height = 320;
  video.srcObject = stream;
  video.play();
}

function error(e) { console.log(e); }

function captureImage() {
  var canvas = document.createElement('canvas');
  canvas.id = 'hiddenCanvas';
  document.body.appendChild(canvas);
  document.getElementById('canvasHolder').appendChild(canvas);
  var ctx = canvas.getContext('2d');
  
  // Set canvas to match thumbnail dimensions
  canvas.width = 500;
  canvas.height = 150;
  
  // Draw image with aspect ratio preservation
  const videoRatio = video.videoWidth / video.videoHeight;
  const canvasRatio = canvas.width / canvas.height;
  let sx, sy, sWidth, sHeight;
  if (videoRatio > canvasRatio) {
    // Video is wider than canvas: crop sides
    sHeight = video.videoHeight;
    sWidth = sHeight * canvasRatio;
    sx = (video.videoWidth - sWidth) / 2;
    sy = 0;
  } else {
    // Video is taller than canvas: crop top/bottom
    sWidth = video.videoWidth;
    sHeight = sWidth / canvasRatio;
    sx = 0;
    sy = (video.videoHeight - sHeight) / 2;
  }
  ctx.drawImage(video, sx, sy, sWidth, sHeight, 0, 0, canvas.width, canvas.height);
  
  // Save canvas image as data URL
  dataURL = canvas.toDataURL('image/png');
  
  // Update preview image with constrained size
  const preview = document.getElementById('preview');
  preview.src = dataURL;
  preview.style.maxWidth = '500px';
  preview.style.maxHeight = '150px';
  preview.style.objectFit = 'contain';
  
  // Set hidden input value
  document.getElementById('imageToForm').value = dataURL;
  
  canvas.remove();
  $('#basic-camera').modal('hide');
}