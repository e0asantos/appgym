/*the given values are also placed in hidden input fields for backend*/

/*filter function for array*/
function isPause(ell, i, arr){
     return parseInt(arr[i+1]) - parseInt(arr[i]) > 1;
}  

/*sort function*/
function sortAsc(a, b){    
  return a - b;
}

/*remove last char from string function*/
function removeLast(str, i){
    var last = str.lastIndexOf(i);
        if (last != -1) {
            str = str.substr(0, last) ;
        }
  return str;
}
  

var i, j, filtered = {}, hours = {};
var mystring = "";
var mystringday = "";
// var selectedDays = [];
// var days = {
//   "mon" : [],
//   "tue" : [],
//   "wed" : [],
//   "thu" : [],
//   "fri" : [], 
//   "sat" : [],
//   "sun" : []
// };

$('.days div').click(function(){
  var dayText = $(this).attr('data-day-select');
  /*remove selected days*/
  if( $(this).hasClass('selected')){
    $('.select-days').find("div[data-day-select='"+dayText+"']:not(.selected)").show();
    $(this).removeClass('selected'); 
    $(this).parents().siblings('table').find('td').removeClass(dayText);
    for (var hour in hours) {
      if (hour.indexOf(dayText)!=-1) {
        hours[hour]=null;
        delete hours[hour];
        days[dayText]=[];
      }
    }
    selectedDays.splice(selectedDays.indexOf(dayText), 1);
    days[dayText]=[];
    // for (var i = selectedDays.length; i > -1; i--) {
    //   if($(this).attr('data-day-select') == selectedDays[i]){
    //     selectedDays.splice(i, 1);
    //     /*remove unselected day from hidden input*/
    //    // $('form').find('.'+ selectedDays[i]).remove();
    //   } 
    // }
  }
  /*add selected days*/
  else{
    $(this).addClass('selected');
    $(this).parents().siblings('table').find('td').addClass(dayText);
    selectedDays.push($(this).attr('data-day-select'));
    $('.select-days').find("div[data-day-select='"+dayText+"']:not(.selected)").hide();
    //$(".:not(#thisid)").doAction();
  }

  /*when new day is selected add already selected hours to new selected day or remove it*/
  if($(this).hasClass('selected')){   
    $(this).parent().siblings('table').addClass('transition-hours');
       for(var i in selectedDays){
           $($(this).parent('.days').siblings('table').find('.selected')).each(function() {
            if(days[selectedDays[i]].indexOf($(this).text())==-1){
              days[selectedDays[i]].push($(this).text());
            }
      });
          }
  }
  else{
    for(var i in selectedDays){
      for (var j in days[i]) {
        
        if(days[selectedDays[i]][j] == $(this).parent('.days').siblings('table').find('.selected').text()){
          $($(this).parent('.days').siblings('table').find('.selected')).each(function() {
        days[selectedDays[i]].splice(j, 1);
      });
          
        }   
      }
    }  
  }
});
  
$('td').on("click", function(){
  if($(this).hasClass('selected')){
    $(this).removeClass('selected');
    //remove hours from days
    for(var i in days){
      for (var j in days[i]) {
        
        if(days[i][j] == $(this).text()){   
        days[i].splice(j, 1);
        }   
      }
    }  
  }
  //add hours to days
  else{
    $(this).addClass('selected');
    for(var i in days ){
      if($(this).hasClass(i)){
        if (days[i].indexOf($(this).text())==-1) {}
        days[i].push($(this).text());
      }  
    }
  }
});


$('body').on("click", ".icon-wrapper", function(ev){ 
  //disable
  $('.text-danger').remove();

  $("#fulltimetable").val(JSON.stringify(days));
  $(this).siblings(".days").toggleClass('transition');  
  //$(this).siblings('table').removeClass('transition-hours');
  if ($(this).find('.rounded').hasClass('fa-plus')){
    $(this).find('.rounded').removeClass('fa-plus');
    $(this).find('.rounded').addClass('fa-check');  
  } else if(true){  
    /*show only text hours where days class is selected in this*/
    $(this).siblings('table').removeClass('transition-hours');
    $(this).parent('.select-days').css('height', '50px');
    $(this).css('height', '50px');
  var parsedTime=false;
   for(i in days){  
    /*sort arrays in days object*/
    days[i].sort(sortAsc)
    
    /*filter days in filtered object with same key as in days*/
    filtered[i] = days[i].filter(isPause);
        
    if(filtered[i].length == 1){
      
      currentI = filtered[i].toString();
      var filteredIndex = days[i].indexOf(currentI);
      hours[i + 0] = days[i].slice(0,filteredIndex+1);
      hours[i + 1] = days[i].slice(filteredIndex+1, days[i].length);
    }
    
    /*add hidden form with time values*/
    if(filtered[i].length < 1 && days[i].length != 0){
       if($('form').find('#'+(i + 0)) || $('form').find('#'+(i + 1))){
        /*remove duplicate empty form fields*/
        $('form').find('#'+(i + 0)).remove();
        $('form').find('#'+(i + 1)).remove();
        
      } 
      
      /*form*/
      if($(this).siblings('.days').find('div[data-day-select="'+i+'"]').hasClass("selected")){
        /*on all next select-days, disable this selected class, so that you can't select the classes twice*/
        //$('.select-days').nextUntil('form').find("div[data-day-select='"+i+"']").off().addClass('selected disabled'); 
        //3$('.select-days').nextUntil('form').find("div[data-day-select='"+i+"']").addClass('selected'); 
          
        /*form*/
        $('form').append('<input type="hidden" class="hour from ' + i +'" name="hour" id="'+ (i + 0) + '"/>');
    $('form').append('<input type="hidden" class="hour to ' + i +'" name="hour" id="'+ (i + 1) + '"/>');
      }
       
      if($(this).siblings('.days').find('div[data-day-select="'+i+'"]').hasClass("selected")){
        mystringday += i + "/";
        if (!parsedTime) {
          // mystring += " : "  + days[i][0] + ":00 - ";
          // mystring += days[i][days[i].length - 1] + ":00";   
          mystring+=generateTimeString(i);
          parsedTime=true;
          $("#fulltimetable").val(JSON.stringify(days));
        }
        
      }

      /*from - to values*/
    $('#' + (i+0)).val( days[i][0] + ":00");
    $('#' + (i+1)).val( days[i][days[i].length - 1] + ":00");
    }
    else if(filtered[i].length >= 1){
      currentI = filtered[i].toString();
      var filteredIndex = days[i].indexOf(currentI);
      hours[i + 0] = days[i].slice(0,filteredIndex+1);
      hours[i + 1] = days[i].slice(filteredIndex+1, days[i].length);
      if($(this).siblings('.days').find('div[data-day-select="'+i+'"]').hasClass("selected")){
        mystringday += i + "/";
        if(!parsedTime){
          // mystring += " : "  + hours[i + 0][0] + ":00 - ";
          // mystring += hours[i + 0][hours[i + 0].length - 1] + ":00" + " & ";
          // mystring += hours[i + 1][0] + ":00 - ";
          // mystring += hours[i + 1][hours[i + 1].length - 1] + ":00";  
          mystring+=generateTimeString(i);
          parsedTime=true;
          $("#fulltimetable").val(JSON.stringify(days));
        }
        
      }
      /*form*/
      if($('form').find('#'+(i + 0)) || $('form').find('#'+(i + 1)) || $('form').find('#'+(i + 2)) || $('form').find('#'+(i + 3))){
        /*remove duplicate empty form fields*/
        $('form').find('#'+(i + 0)).remove();
        $('form').find('#'+(i + 1)).remove();
        $('form').find('#'+(i + 2)).remove();
        $('form').find('#'+(i + 3)).remove();
      }    
      if($(this).siblings('.days').find('div[data-day-select="'+i+'"]').hasClass("selected")){
        //$('.select-days').nextUntil('form').find("div[data-day-select='"+i+"']").off().addClass('disabled'); 
        //$('.select-days').nextUntil('form').find("div[data-day-select='"+i+"']").off().addClass('disabled'); 
        /*add form inputs with pause*/
        $('form').append('<input type="hidden" class="hour before-pause from ' + i +'" name="hour" id="'+ (i + 0) + '"/>');
         $('form').append('<input type="hidden" class="hour to before-pause ' + i +'" name="hour" id="'+ (i + 1) + '"/>');
         $('form').append('<input type="hidden" class="hour from after-pause ' + i +'" name="hour" id="'+ (i + 2) + '"/>');
         $('form').append('<input type="hidden" class="hour to after-pause ' + i +'" name="hour" id="'+ (i + 3) + '"/>');
      }
      
      /*values from - to / before - after break*/
      /*before break*/
    //   $('#' + (i+0)).val( hours[i + 0][0] + ":00");
    // $('#' + (i+1)).val( hours[i + 0][hours[i + 0].length - 1] + ":00");
      /*after break*/
    //   $('#' + (i+2)).val( hours[i + 1][0] + ":00");
    // $('#' + (i+3)).val( hours[i + 1][hours[i + 1].length - 1] + ":00");
    }
  }
    
  /*check if no hours were selected*/
  var count = 0;
  for(j in days){
    if(days[j].length === 0){
      count++;
    }
  }
  if (count === Object.keys(days).length){
      $('.select-days').prepend('<span class="text-danger" style="line-height:60px">No hay hora seleccionada</span>')
  }  
  
    var values = [];
    if($('.hour').length){

      $(".hour").each(function( index ) {
        values.push($(this).val());
      });

      var numClass;

     
      /*remove duplicates from strings*/
       var arr =  $.unique(mystring.split(/:00/));
       mystring = arr.join(":00");
      var arr2 =  $.unique(mystringday.split(/:00/));
       mystringday = arr2.join(":00");
      
      /*remove last / */
      mystringday = removeLast(mystringday, "/");

      /*display string*/
      $(this).siblings('.from-to').text(mystringday + mystring);
      if ($(this).siblings('.days.transition').length == 0){
        $(this).siblings('.from-to').removeClass('hidden').delay(500).queue(function(next){
          $(this).addClass("transition");  
          next();
      });
      } else {
        $(this).siblings('.from-to').addClass('hidden').delay(500).queue(function(next){
          $(this).addClass("transition");  
          next();
      });
      }
      
     
      /*as long as not all days are selected display hidden field*/
      if($(this).siblings('.days').find('.selected').length != 7){
        $(this).parent('.select-days').next().removeClass('hidden');
      }
      
      $(this).find('.rounded').removeClass('fa-check').addClass('fa-pencil-alt');
    }     
  }
    
   if($(this).find('.rounded').hasClass('fa-pencil-alt') && $(this).siblings('.days').hasClass('transition')){ 
     $(this).find('.table').addClass('transition-hours');
     $(this).parent('.select-days').css('height', 'inherit');
     $(this).css('height', '120px');

  }
    
  /*empty strings so we can start again on next hour form*/
  mystring = "";
  mystringday = "";
});
function runLabelSetter(){

  for (var i = 6; i >= 0; i--) {
    var singleHours=$("#mschedule-"+i).children(".days").children(".selected");
    var mystringLabel="";
    var mystringday="";
    if (singleHours.length>0) {
      for (var ii = singleHours.length - 1; ii >= 0; ii--) {
        mystringday+=singleHours[ii].innerText+"/";
        mystringLabel=generateTimeString(singleHours[ii].innerText.toLowerCase());
      }
      mystringday=removeLast(mystringday,"/");
    }

    //var singleDays=$("#mschedule-"+i).find("#days").find(".selected");
    // var mystringLabel=generateTimeString(i);
    //$("#fulltimetable").val(JSON.stringify(days));
    $("#mschedule-"+i).children('.from-to').text(mystringday + mystringLabel);
    $("#mschedule-"+i).children('.from-to').removeClass('hidden');
    $("#mschedule-"+i).children('.from-to').addClass("transition");

    for(mdia in days){  

    }
  }
}
function generateTimeString(mday){
  var timeTable="";
  var inicio=false;
  for (var i = 0;  i < days[mday].length; i++) {
    if (!inicio) {
      timeTable+=days[mday][i]+":00";
      inicio=true;
    }

    if(Number(days[mday][i])+1!=Number(days[mday][i+1])){
      inicio=false;
      timeTable+=" - "+String(Number(days[mday][i])+1)+":00 & "
    }
  }
  return " "+removeLast(timeTable, "&");;
}
$(function() {
    console.log( "ready!" );

    runLabelSetter();
});
