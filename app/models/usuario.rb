class Usuario < ActiveRecord::Base
  sync :all
  has_many :pagos, foreign_key: "usuarios_id"
end
