#!/bin/bash

# Directory to search for model files
MODELS_DIR="app/models"

# Check if the directory exists
if [ ! -d "$MODELS_DIR" ]; then
  echo "Error: $MODELS_DIR does not exist. Are you in the root of a Rails project?"
  exit 1
fi

# Find all Ruby files in the models directory
MODEL_FILES=$(find "$MODELS_DIR" -type f -name "*.rb")

# Check if any files were found
if [ -z "$MODEL_FILES" ]; then
  echo "No Ruby files found in $MODELS_DIR."
  exit 0
fi

# Counter for modified files
MODIFIED_COUNT=0

# Process each model file
for file in $MODEL_FILES; do
  # Check if the file contains attr_accessible
  if grep -q "attr_accessible" "$file"; then
    echo "Processing: $file"

    # Create a backup of the original file
    cp "$file" "$file.bak"
    echo "  Created backup: $file.bak"

    # Remove lines containing attr_accessible (including any trailing commas or whitespace)
    sed -i '/^[[:space:]]*attr_accessible/d' "$file"
    echo "  Removed attr_accessible from $file"

    ((MODIFIED_COUNT++))
  fi
done

# Summary
if [ "$MODIFIED_COUNT" -eq 0 ]; then
  echo "No files contained attr_accessible. Nothing to do."
else
  echo "Done! Modified $MODIFIED_COUNT file(s)."
  echo "Backups are saved with .bak extension in case you need to revert."
fi