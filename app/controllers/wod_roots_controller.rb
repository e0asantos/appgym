#encoding: utf-8
class WodRootsController < ApplicationController
  # GET /wod_roots
  # GET /wod_roots.json
  def index
    if session[:cuser].tipo != isAdmin()
      redirect_to "/ejercicios"
      return
    end
    @wod_roots = WodRoot.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @wod_roots }
    end
  end

  # GET /wod_roots/1
  # GET /wod_roots/1.json
  def show
    @wod_root = WodRoot.find(params[:id])
    @ejercicios=Ejercicio.all
    if session[:cuser].tipo!=1
      @ejercicios=Ejercicio.where("networkid='"+session[:cuser].networkid.to_s+"' or globalavailable=1")
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @wod_root }
    end
  end

  def getWodItems
    ejercicios=WodItem.where(:wod_id=>params[:id]).index_by(&:workout_id).keys
    finalEjercicios=Ejercicio.where(:id=>ejercicios)
    finalJson=[]
    finalEjercicios.each do |singleWod|
      # singleWod.created_at=singleWod.created_at.strftime("at %I:%M%p")
      hash=singleWod.as_json
      puts singleWod.inspect
      author=Usuario.find_by_id(singleWod.author_id).nombre+" "+Usuario.find_by_id(singleWod.author_id).apellido
      foto=Usuario.find_by_id(singleWod.author_id).foto
      hash["nombre"]=author
      hash["foto"]=foto
      hash["created_at"]=singleWod.created_at.strftime("%m/%d/%Y")
      hash["likes"]=Like.where(:wod_root_id=>params[:id],:wod_item_id=>singleWod.id).length
      hash["comments"]=Comentario.where(:wod_root_id=>params[:id],:wod_item_id=>singleWod.id).length
      finalJson.push(hash)
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: finalJson.to_json }
    end
  end

  def getSingleWodItem
    ejercicio=Ejercicio.where(:id=>params[:id])
    finalJson=[]
    ejercicio.each do |singleWod|
      # singleWod.created_at=singleWod.created_at.strftime("at %I:%M%p")
      hash=singleWod.as_json
      puts singleWod.inspect
      author=Usuario.find_by_id(singleWod.author_id).nombre+" "+Usuario.find_by_id(singleWod.author_id).apellido
      foto=Usuario.find_by_id(singleWod.author_id).foto
      hash["nombre"]=author
      hash["foto"]=foto
      hash["created_at"]=singleWod.created_at.strftime("%m/%d/%Y")
      hash["likes"]=Like.where(:wod_root_id=>params[:id],:wod_item_id=>singleWod.id).length
      hash["comments"]=Comentario.where(:wod_root_id=>params[:id],:wod_item_id=>singleWod.id).length
      finalJson.push(hash)
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: finalJson.to_json }
    end
  end

  def getMyWods
    # extraer los wods del establecimiento donde esta el usuario y los publicos
    @mywods=nil
    @finalwod=[]
    # revisar si esta en un establecimiento
    asistencia=Asistencium.where('created_at>=?',Date.today).where(:user_id=>session[:cuser].id).last
    if asistencia!=nil
      if (asistencia.activo==1 or session[:cuser].tipo==1) and asistencia.transition=="enter" and (asistencia.arrival_time+3.hours)>Time.now
        # los wods del establecimiento por el networkid
        @mywods=WodRoot.where("networkid="+asistencia.networkid+" or globalavailable=1")
      else
        @mywods=WodRoot.where("globalavailable=1")
      end
    else
      @mywods=WodRoot.where("globalavailable=1")
    end
    # puts WodRoot.joins(:usuario).select("usuarios.nombre,usuarios.apellido,usuarios.email,wod_roots.*").where("wod_roots.globalavailable=1").inspect
    @mywods.each do |singleWod|
      # singleWod.created_at=singleWod.created_at.strftime("at %I:%M%p")
      hash=singleWod.as_json
      author=Usuario.find_by_id(singleWod.usuario_id).nombre+" "+Usuario.find_by_id(singleWod.usuario_id).apellido
      foto=Usuario.find_by_id(singleWod.usuario_id).foto
      hash["nombre"]=author
      hash["foto"]=foto
      hash["created_at"]=singleWod.created_at.strftime("%m/%d/%Y")
      hash["likes"]=Like.where(:wod_root_id=>singleWod.id,:wod_item_id=>nil).length
      hash["comments"]=Comentario.where(:wod_root_id=>singleWod.id,:wod_item_id=>nil).length
      @finalwod.push(hash)
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @finalwod.to_json }
    end
  end
  # GET /wod_roots/new
  # GET /wod_roots/new.json
  def new
    @wod_root = WodRoot.new
    @ejercicios=Ejercicio.all
    if session[:cuser].tipo!=1
      @ejercicios=Ejercicio.where("networkid='"+session[:cuser].networkid.to_s+"' or globalavailable=1")
    end
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @wod_root }
    end
  end

  # GET /wod_roots/1/edit
  def edit
    @ejercicios=Ejercicio.all
    if session[:cuser].tipo!=1
      @ejercicios=Ejercicio.where("networkid='"+session[:cuser].networkid.to_s+"' or globalavailable=1")
    end
    @wod_root = WodRoot.find(params[:id])
  end

  # POST /wod_roots
  # POST /wod_roots.json
  def create
    if params[:wod_root][:portada]!=nil
      puts params[:wod_root][:portada].original_filename
      path = File.join(Rails.root + "public/images", params[:wod_root][:portada].original_filename)
      # write the file
      File.open(path, "w+b") { |f| f.write(params[:wod_root][:portada].read) }
      # limpiamos
      params[:wod_root][:portada]=params[:wod_root][:portada].original_filename
    end
    @wod_root = WodRoot.new(params[:wod_root])
    @wod_root.usuario_id=session[:cuser].id
    @wod_root.networkid=session[:cuser].networkid
    respond_to do |format|
      if @wod_root.save
        if params[:workoutl] == '' || params[:workoutl] == nil
          params[:workoutl] = '[]'
        end
        hashWorkouts=JSON.parse(params[:workoutl])
        # borrar todos los anteriores
        erasePrevious=WodItem.where(:wod_id=>@wod_root.id).destroy_all
        hashWorkouts.each do |singleWorkout|
          newWorkOutItem=WodItem.new
          newWorkOutItem.workout_id=singleWorkout
          newWorkOutItem.wod_id=@wod_root.id
          newWorkOutItem.save
        end
        format.html { redirect_to "/ejercicios", notice: 'WOD creado con éxito' }
        format.json { render json: @wod_root, status: :created, location: @wod_root }
      else
        format.html { render action: "new" }
        format.json { render json: @wod_root.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /wod_roots/1
  # PUT /wod_roots/1.json
  def update
    if params[:wod_root][:portada]!=nil
      puts params[:wod_root][:portada].original_filename
      path = File.join(Rails.root + "public/images", params[:wod_root][:portada].original_filename)
      # write the file
      File.open(path, "w+b") { |f| f.write(params[:wod_root][:portada].read) }
      # limpiamos
      params[:wod_root][:portada]=params[:wod_root][:portada].original_filename
    end
    @wod_root = WodRoot.find(params[:id])

    respond_to do |format|
      if @wod_root.update_attributes(params[:wod_root])
        if params[:workoutl]!=""
          hashWorkouts=JSON.parse(params[:workoutl])
          # borrar todos los anteriores
          erasePrevious=WodItem.where(:wod_id=>@wod_root.id).destroy_all
          hashWorkouts.each do |singleWorkout|
            newWorkOutItem=WodItem.new
            newWorkOutItem.workout_id=singleWorkout
            newWorkOutItem.wod_id=@wod_root.id
            newWorkOutItem.save
          end  
        end
        
        format.html { redirect_to @wod_root, notice: 'Wod root was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @wod_root.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /wod_roots/1
  # DELETE /wod_roots/1.json
  def destroy
    @wod_root = WodRoot.find(params[:id])
    @wod_root.destroy

    respond_to do |format|
      format.html { redirect_to wod_roots_url }
      format.json { head :no_content }
    end
  end
end
