# encoding: utf-8
class RewardsController < ApplicationController
  # GET /rewards
  # GET /rewards.json
  def index
    if session[:cuser].nil?
      redirect_to "/"
      return
    end
    unless [1, 2, 6].include?(session[:cuser].tipo)
      redirect_to "/"
      return
    end

    if session[:cuser].tipo != 1
      @rewards = Reward.where(networkid: session[:cuser].networkid)
      @won_rewards = RecompensaObtenida.where(networkid: session[:cuser].networkid)
    else 
      @rewards = Reward.all
      @won_rewards = RecompensaObtenida.all
    end

    # Filtros para historial de recompensas
    if params[:search].present?
      search = params[:search]
      nombre = search[:nombre].present? ? search[:nombre] : nil
      apellido = search[:apellido].present? ? search[:apellido] : nil
      recompensa = search[:recompensa].present? ? search[:recompensa] : nil

      # Filtrar usuarios por nombre y/o apellido
      if nombre || apellido
        usuarios = Usuario.where("nombre LIKE :nombre", nombre: "%#{nombre}%")
        usuarios = usuarios.where("apellido LIKE :apellido", apellido: "%#{apellido}%") if apellido
        @won_rewards = @won_rewards.where(usuarios_id: usuarios.pluck(:id))
      end

      # Filtrar por nombre de recompensa
      if recompensa
        rewards = Reward.where("titulo LIKE ?", "%#{recompensa}%")
        @won_rewards = @won_rewards.where(rewards_id: rewards.pluck(:id))
      end
    end

    # Paginación con 30 ítems por página
    @won_rewards = @won_rewards.order(created_at: :desc).paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @rewards }
    end
  end


  # GET /rewards/1
  # GET /rewards/1.json
  def show
    @reward = Reward.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @reward }
    end
  end

  # GET /rewards/new
  # GET /rewards/new.json
  def new
    @reward = Reward.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @reward }
    end
  end

  # GET /rewards/1/edit
  def edit
    @reward = Reward.find(params[:id])
  end

  # POST /rewards
  # POST /rewards.json
  def create
    @reward = Reward.new(reward_params)
    @reward.networkid=session[:cuser].networkid
    respond_to do |format|
      if @reward.save
        format.html { redirect_to "/rewards", notice: 'Se creó correctamente una recompensa.' }
        format.json { render json: @reward, status: :created, location: @reward }
      else
        format.html { render action: "new" }
        format.json { render json: @reward.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /rewards/1
  # PUT /rewards/1.json
  def update
    @reward = Reward.find(params[:id])

    respond_to do |format|
      if @reward.update(reward_params)
        format.html { redirect_to "/rewards", notice: 'La recompensa se actualizo correctamente.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @reward.errors, status: :unprocessable_entity }
      end
    end
  end

  def reward_params
    params.require(:reward).permit(:titulo, :descripcion, :asistencia, :numpagos)
  end

  # DELETE /rewards/1
  # DELETE /rewards/1.json
  def destroy
    @reward = Reward.find(params[:id])
    @reward.destroy

    respond_to do |format|
      format.html { redirect_to rewards_url }
      format.json { head :no_content }
    end
  end
end
