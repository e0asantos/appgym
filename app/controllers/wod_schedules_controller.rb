class WodSchedulesController < ApplicationController
  # GET /wod_schedules
  # GET /wod_schedules.json
  def index
    @wod_schedules = WodSchedule.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @wod_schedules }
    end
  end

  # GET /wod_schedules/1
  # GET /wod_schedules/1.json
  def show
    @wod_schedule = WodSchedule.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @wod_schedule }
    end
  end

  # GET /wod_schedules/new
  # GET /wod_schedules/new.json
  def new
    @wod_schedule = WodSchedule.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @wod_schedule }
    end
  end

  # GET /wod_schedules/1/edit
  def edit
    @wod_schedule = WodSchedule.find(params[:id])
  end

  # POST /wod_schedules
  # POST /wod_schedules.json
  def create
    @wod_schedule = WodSchedule.new(params[:wod_schedule])

    respond_to do |format|
      if @wod_schedule.save
        format.html { redirect_to @wod_schedule, notice: 'Wod schedule was successfully created.' }
        format.json { render json: @wod_schedule, status: :created, location: @wod_schedule }
      else
        format.html { render action: "new" }
        format.json { render json: @wod_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /wod_schedules/1
  # PUT /wod_schedules/1.json
  def update
    @wod_schedule = WodSchedule.find(params[:id])

    respond_to do |format|
      if @wod_schedule.update_attributes(params[:wod_schedule])
        format.html { redirect_to @wod_schedule, notice: 'Wod schedule was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @wod_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /wod_schedules/1
  # DELETE /wod_schedules/1.json
  def destroy
    @wod_schedule = WodSchedule.find(params[:id])
    @wod_schedule.destroy

    respond_to do |format|
      format.html { redirect_to wod_schedules_url }
      format.json { head :no_content }
    end
  end
end
