class AsistenciaController < ApplicationController
  before_action :require_login
  # GET /asistencia
  # GET /asistencia.json
  def index
    if session[:cuser].tipo>3
      redirect_to "/"
      return
    end
    @asistencia = Asistencium.all
    if session[:cuser].tipo>1
      @asistencia=Asistencium.where(:networkid=>session[:networkid])
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @asistencia }
    end
  end

  def apiController
    
  end

  # GET /asistencia/1
  # GET /asistencia/1.json
  def show
    @asistencium = Asistencium.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @asistencium }
    end
  end

  # GET /asistencia/new
  # GET /asistencia/new.json
  def new
    @asistencium = Asistencium.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @asistencium }
    end
  end

  # GET /asistencia/1/edit
  def edit
    @asistencium = Asistencium.find(params[:id])
  end

  def startWod
    response=[]
    horaCero=Time.now.change({min: 0,sec:0 })-15.minutes
    usuarios=Asistencium.where("created_at >= ?",horaCero).where(:user_id=>params[:iniciarwod]).where(:networkid=>session[:cuser].networkid,:transition=>"enter")

    usuarios.each do |singleEntry|
      singleEntry.startwod=Time.now
      singleEntry.save
    end
    respond_to do |format|
      format.json { render json: response.to_json }
    end
  end

  def endWod
    response=Hash.new
    horaCero=Date.today
    usuarios=Asistencium.where("created_at >= ?",horaCero).where(:user_id=>params[:wodstop]).where(:networkid=>session[:cuser].networkid,:transition=>"enter")
    puts usuarios.inspect
    usuarios.each do |singleEntry|
      if singleEntry.endwod==nil && singleEntry.startwod != nil
        singleEntry.endwod=Time.now
        response["minutos"]=TimeDifference.between(singleEntry.startwod, singleEntry.endwod).in_minutes
        singleEntry.save  
      end
    end

    respond_to do |format|
      format.json { render json: response.to_json }
    end
  end

  def activeWod
    response=Hash.new

    existencia=Asistencium.where(:user_id=>session[:cuser].id).where(:transition=>"enter").where("startwod is not null and endwod is null")
    if existencia.length>0
      response["activo"]=true
    else
      response["activo"]=false
    end

    respond_to do |format|
      format.json { render json: response.to_json }
    end
    
  end

  def listaDeAsistencia
    # traemos la lista de hoy
    horaCero=Time.now.change({min: 0,sec:0 })-15.minutes
    puts "horaCero:"+horaCero.to_s+" ------> "+Time.now.to_s
    asistio=[]
    asistencias=Asistencium.where("created_at >= ?",horaCero).where(:networkid=>session[:cuser].networkid,:transition=>"enter")
    response=[]

    asistencias.each do |singleAsistencia|
      nombre=Usuario.find_by_id(singleAsistencia.user_id)
      if nombre==nil
        nombre=Usuario.new
      end
      if asistio.index(singleAsistencia.user_id)==nil
        usuario=Hash.new
        usuario["nombre"]=nombre.nombre+" "+nombre.apellido
        usuario["pago"]=singleAsistencia.activo
        usuario["avatar"]=nombre.foto
        usuario["id"]=nombre.id
        usuario["entrada"]=singleAsistencia.created_at.strftime(" %I:%M%p")
        response.push(usuario)
        asistio.push(singleAsistencia.user_id)
      end
    end

    respond_to do |format|
      format.json { render json: response.to_json }
    end
  end

  def monthly_chart
    # Get the current date (March 02, 2025, based on your setup)
    current_date = Date.today
    # Calculate the start date (6 months ago)
    start_date = current_date - 5.months

    # Build an array of the last 6 months (e.g., Sep 2024 to Feb 2025)
    months = (0..5).map { |i| (current_date - i.months).strftime('%b %Y') }.reverse

    # Fetch Asistencia records based on session[:cuser]
    if session[:cuser][:tipo] == 1
      # Tipo 1 sees all records
      records = Entrada.where(created_at: start_date.beginning_of_month..current_date.end_of_month)
    else
      # Non-tipo 1 sees only their networkid
      records = Entrada.where(
        networkid: session[:cuser][:networkid],
        created_at: start_date.beginning_of_month..current_date.end_of_month
      )
    end

    # Group records by month and count them
    counts_by_month = records.group_by { |r| r.created_at.strftime('%b %Y') }
                             .transform_values(&:count)
                             .tap { |h| months.each { |m| h[m] ||= 0 } } # Ensure all months are included, even with 0

    # Format data for Chart.js
    chart_data = {
      labels: months, # e.g., ["Sep 2024", "Oct 2024", ..., "Feb 2025"]
      datasets: [{
        label: 'Asistencias',
        data: months.map { |month| counts_by_month[month] } # e.g., [5, 10, 8, 12, 7, 9]
      }]
    }

    render json: chart_data
  end


  # Example authentication check (adjust to your setup)
  def require_login
    redirect_to "/" unless session[:cuser].present?
  end

  def estadistica
    workWeek=[Date.today.monday,Date.today.monday+1,Date.today.monday+2,Date.today.monday+3,Date.today.monday+4,Date.today.monday+5]
    # puts "------today"
    # puts Date.today.monday
    # puts "------today"
    finalStats=[]
    cIndex=0
    workWeek.each do |singleDay|
      minutos=0
      puts session[:cuser].id
      asistencias=Asistencium.where('created_at>=?',singleDay).where(:user_id=>session[:cuser].id).where('created_at<=?',singleDay+1.day)
      puts asistencias.inspect
      elementos=asistencias.index_by(&:transition).keys.uniq
      if elementos.length==1 and elementos.join("").index("enter")!=nil
        futuro=asistencias.first.arrival_time+3.hours
        if Time.now>futuro
          # hay que insertar un elemento de fin en 1 hora
          finTiempo=Asistencium.new
          finTiempo.user_id=session[:cuser].id
          finTiempo.establecimiento_id=asistencias.first.establecimiento_id
          finTiempo.networkid=asistencias.first.networkid
          finTiempo.activo=asistencias.first.activo
          finTiempo.arrival_time=asistencias.first.arrival_time+1.hour
          finTiempo.minutos=60
          finTiempo.transition="exit"
          finTiempo.save
          minutos=60
        else
          minutos=TimeDifference.between(asistencias.first.arrival_time, Time.now).in_minutes
        end
      end
      if elementos.length==1 and elementos.join("").index("exit")!=nil
          # hay que insertar un elemento de principio en 1 hora anterior
          finTiempo=Asistencium.new
          finTiempo.user_id=session[:cuser].id
          finTiempo.establecimiento_id=asistencias.last.establecimiento_id
          finTiempo.networkid=asistencias.last.networkid
          finTiempo.activo=asistencias.last.activo
          finTiempo.arrival_time=asistencias.last.arrival_time-1.hour
          finTiempo.transition="enter"
          finTiempo.save
          minutos=60
      end
      if minutos==0
        waitFor='enter'
        fechaInicio=nil
        asistencias=Asistencium.where('created_at>=?',singleDay).where(:user_id=>session[:cuser].id).where('created_at<=?',singleDay+1.day)
        asistencias.each do |singleAsistencia|
          # debemos empezar con una entrada
          if singleAsistencia.transition==waitFor
            if fechaInicio!=nil
              minutos=minutos+TimeDifference.between(fechaInicio, singleAsistencia.arrival_time).in_minutes
            else
              fechaInicio=singleAsistencia.arrival_time
            end

            if waitFor=="enter"
              waitFor="exit"
            else
              waitFor="enter"
              fechaInicio=nil
            end
          end
        end
        finalStats.push(minutos)
      end
      puts "------asistencia"
      puts asistencias.inspect
      # caso 1, solo hay 1 enter

      # caso 2, hay varios enter

      # caso 3 hay 1 exit

      # caso 4, hay varios exit

      # caso 5, varios enter seguidos y varios exit seguidos

      # caso 6, hay 
    end

    respond_to do |format|
      format.json { render json: finalStats.to_json}
    end

  end

  # POST /asistencia
  # POST /asistencia.json
  def asistencium_params
    params.require(:asistencium).permit(:beacon, :major,:minor,:transition)
  end

  def create
    
    @asistencium = Asistencium.new
    @asistencium.transition=params[:transition]
    @asistencium.user_id=session[:cuser].id
    if @asistencium.transition=="exit"
      # buscamos el ultimo enter para calcular el tiempo total
      lastEnter=Asistencium.where(:transition=>"enter",:user_id=>session[:cuser].id).last
      minutos=TimeDifference.between(lastEnter.created_at, Time.now).in_minutes
      @asistencium.minutos=minutos
    end
    # buscamos el beacon a quien le pertenece
    establecimiento=Establecimiento.where(:beacon=>params[:beacon],:major=>params[:major],:minor=>params[:minor]).last
    if @asistencium.arrival_time == nil or @asistencium.arrival_time == ''
      @asistencium.arrival_time=Time.now
      @asistencium.save
    end
    if establecimiento==nil
      @asistencium.activo=0
    else 
      @asistencium.establecimiento_id=establecimiento.id
      @asistencium.networkid=session[:cuser].networkid
      @asistencium.arrival_time=Time.now
      @asistencium.save
      # revisar si esta permitido estar aqui
      paquete=Entrada.where(:establecimientos_id=>establecimiento.id).last
      rewardExecuted = 0
      if paquete!=nil
        if paquete.paquete==session[:cuser].paquete && paquete.paquete != nil
          # aqui por ultimo verificamos que exista un pago
          pagoActivo=Pago.where(:usuarios_id=>session[:cuser].id).where('vence>=?',Time.now)
          if pagoActivo.length>0 or (session[:cuser].finalfecha>=DateTime.now and session[:cuser].finalfecha!=nil)
            @asistencium.activo=1  
            rewardService(session[:cuser],establecimiento.id, 1)
          else
            @asistencium.activo=0
          end
        else
          @asistencium.activo=0  
        end
      else 
        @asistencium.activo=0
      end
    end
    if rewardExecuted == 0
      rewardService(session[:cuser],establecimiento.id, 1)
    end
    respond_to do |format|
      if @asistencium.save
        format.html { redirect_to @asistencium, notice: 'Asistencium was successfully created.' }
        format.json { render json: @asistencium, status: :created, location: @asistencium }
      else
        format.html { render action: "new" }
        format.json { render json: @asistencium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /asistencia/1
  # PUT /asistencia/1.json
  def update
    @asistencium = Asistencium.find(params[:id])

    respond_to do |format|
      if @asistencium.update(asistencium_params)
        format.html { redirect_to @asistencium, notice: 'Asistencium was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @asistencium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /asistencia/1
  # DELETE /asistencia/1.json
  def destroy
    @asistencium = Asistencium.find(params[:id])
    @asistencium.destroy

    respond_to do |format|
      format.html { redirect_to asistencia_url }
      format.json { head :no_content }
    end
  end
end
