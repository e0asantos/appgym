#encoding: utf-8
class NotificacionesController < ApplicationController
  # GET /notificaciones
  # GET /notificaciones.json
  def index
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
     if session[:cuser].tipo!=1 and session[:cuser].tipo!=2 and session[:cuser].tipo!=4
      redirect_to "/"
      return
    end
    @notificaciones = Notificacione.all
    @notificacione=Notificacione.where(:networkid=>session[:cuser].networkid).last
    puts @notificacione.inspect
    puts "=========1"+session[:cuser].networkid.to_s
    if @notificacione==nil
      @notificacione=Notificacione.new
      @notificacione.networkid=session[:cuser].networkid
      @notificacione.save
    end
    @correo=Correo.new
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @notificaciones }
    end
  end

  # GET /notificaciones/1
  # GET /notificaciones/1.json
  def show
    @notificacione = Notificacione.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @notificacione }
    end
  end

  # GET /notificaciones/new
  # GET /notificaciones/new.json
  def new
    @notificacione = Notificacione.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @notificacione }
    end
  end

  # GET /notificaciones/1/edit
  def edit
    @notificacione = Notificacione.find(params[:id])
  end

  # POST /notificaciones
  # POST /notificaciones.json
  def create
    @notificacione = Notificacione.new(params[:notificacione])

    respond_to do |format|
      if @notificacione.save
        format.html { redirect_to "/notificaciones", notice: 'Se creó información de notificación.' }
        format.json { render json: @notificacione, status: :created, location: @notificacione }
      else
        format.html { render action: "new" }
        format.json { render json: @notificacione.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /notificaciones/1
  # PUT /notificaciones/1.json
  def update
    @notificacione = Notificacione.find(params[:id])

    respond_to do |format|
      if @notificacione.update_attributes(params[:notificacione])
        format.html { redirect_to "/notificaciones", notice: 'Se actualizó la información de notificaciones.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @notificacione.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notificaciones/1
  # DELETE /notificaciones/1.json
  def destroy
    @notificacione = Notificacione.find(params[:id])
    @notificacione.destroy

    respond_to do |format|
      format.html { redirect_to notificaciones_url }
      format.json { head :no_content }
    end
  end
end
