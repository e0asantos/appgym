class LikesController < ApplicationController
  # GET /likes
  # GET /likes.json
  def index
    @likes = Like.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @likes }
    end
  end

  # GET /likes/1
  # GET /likes/1.json
  def show
    @like = Like.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @like }
    end
  end

  # GET /likes/new
  # GET /likes/new.json
  def new
    @like = Like.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @like }
    end
  end

  # GET /likes/1/edit
  def edit
    @like = Like.find(params[:id])
  end

  # POST /likes
  # POST /likes.json
  def create
    @like = Like.new(like_params)
    @like.usuario_id=session[:cuser].id
    # @like.establecimiento_id = 
    @like.wod_root_id = params[:wod_root_id]
    @like.wod_item_id = params[:wod_item_id]
    # buscar si ya existe este like
    puts "====== LIKES"
    puts @like.inspect
    puts "====== LIKES"
    prevLike=Like.where(:wod_item_id=>@like.wod_item_id,:establecimiento_id=>@like.establecimiento_id,:usuario_id=>@like.usuario_id)
    if prevLike.length>0
      # ya existe, entonces lo quitamos
      prevLike.last.destroy
      respond_to do |format|
        format.json { render json: "OK".to_json}
      end
      return
    end
    respond_to do |format|
      if @like.save
        format.html { redirect_to @like, notice: 'Like was successfully created.' }
        format.json { render json: @like, status: :created, location: @like }
      else
        format.html { render action: "new" }
        format.json { render json: @like.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /likes/1
  # PUT /likes/1.json
  def update
    @like = Like.find(params[:id])

    respond_to do |format|
      if @like.update(like_params)
        format.html { redirect_to @like, notice: 'Like was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @like.errors, status: :unprocessable_entity }
      end
    end
  end

  def like_params
    params.require(:like).permit(:wod_item_id, :wod_root_id)
  end

  # DELETE /likes/1
  # DELETE /likes/1.json
  def destroy
    @like = Like.find(params[:id])
    @like.destroy

    respond_to do |format|
      format.html { redirect_to likes_url }
      format.json { head :no_content }
    end
  end
end
