class NutritionsController < ApplicationController
  # GET /nutritions
  # GET /nutritions.json
  def index
    @nutritions = Nutrition.where(:nutritionist_id=>[nil,session[:cuser].id])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @nutritions }
    end
  end

  def getMyAppointments
    @nutrition = Nutrition.where(:user_id=>session[:cuser].id).where("expire_on >= ? or expire_on is NULL",Time.now)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @nutrition }
    end
  end

  # GET /nutritions/1
  # GET /nutritions/1.json
  def show
    @nutrition = Nutrition.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @nutrition }
    end
  end

  # GET /nutritions/new
  # GET /nutritions/new.json
  def new
    @nutrition = Nutrition.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @nutrition }
    end
  end

  # GET /nutritions/1/edit
  def edit
    @nutrition = Nutrition.find(params[:id])
  end

  # POST /nutritions
  # POST /nutritions.json
  def create
    @nutrition = Nutrition.new(nutrition_params)
    @nutrition.description = params[:description]
    new_datetime = DateTime.parse(params[:appointment]).strftime("%Y-%m-%d") + ' ' + DateTime.parse(params[:hora]).strftime('%H:%M:%S')
    @nutrition.appointment = new_datetime
    @nutrition.online = params[:online]
    @nutrition.user_id=session[:cuser].id
    respond_to do |format|
      if @nutrition.save
        format.html { redirect_to @nutrition, notice: 'Nutrition was successfully created.' }
        format.json { render json: @nutrition, status: :created, location: @nutrition }
      else
        format.html { render action: "new" }
        format.json { render json: @nutrition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /nutritions/1
  # PUT /nutritions/1.json
  def update
    @nutrition = Nutrition.find(params[:id])
    @nutritionc = Nutrition.new(nutrition_params)
    if @nutritionc.feedback=="" or @nutritionc.feedback==" "
      @nutritionc.feedback=nil
    end
    @nutrition.feedback=@nutritionc.feedback
    @nutrition.comments=@nutritionc.comments


    puts "====>"+@nutritionc.feedback.inspect
    # revisar si no esta asignado
    if @nutrition.nutritionist_id==nil
      @nutrition.nutritionist_id=session[:cuser].id
      # asignarle status 1
      @nutrition.status=1
    end
    if @nutritionc.feedback!=nil and @nutritionc.feedback!="" and @nutritionc.feedback!=" "
      # generar fecha de fin a 3 semanas
      @nutrition.expire_on=Time.now+3.weeks
      @nutrition.status=2
      @nutrition.grasa=@nutritionc.grasa
      @nutrition.azucar=@nutritionc.azucar
      @nutrition.calorias=@nutritionc.calorias
      @nutrition.proteina=@nutritionc.proteina
    end

    respond_to do |format|
      if @nutrition.save
        format.html { redirect_to nutritions_url, notice: 'Nutrition was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @nutrition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nutritions/1
  # DELETE /nutritions/1.json
  def destroy
    @nutrition = Nutrition.find(params[:id])
    @nutrition.destroy

    respond_to do |format|
      format.html { redirect_to nutritions_url }
      format.json { head :no_content }
    end
  end

  def nutrition_params
    permitted = params.require(:nutrition).permit(
      :appointment, :description,:hora,:online)
    permitted
  end
end
