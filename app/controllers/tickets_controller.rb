class TicketsController < ApplicationController
  # GET /tickets
  # GET /tickets.json
  def index
    @tickets = Ticket.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tickets }
    end
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    @ticket = nil
    if session[:cuser].tipo == isAdmin()
      @ticket=Pago.find(params[:id])
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @ticket=Pago.where(:networkid=>session[:cuser].networkid,:id=>params[:id]).last
    else
      @ticket=Pago.find(params[:id]).where(:usuarios_id=>session[:cuser].id)
    end
    if @ticket==nil
      redirect_to "/"
      return
    end
    @establecimiento = Establecimiento.where(:id=>Usuario.find_by_id(@ticket.usuarios_id).gymorigin).last
    @isshow=false
    

    respond_to do |format|
      format.html {render :layout=>false}# show.html.erb
      format.json { render json: @ticket }
    end
    


  end

  # GET /tickets/new
  # GET /tickets/new.json
  def new
    @ticket = Ticket.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ticket }
    end
  end

  # GET /tickets/1/edit
  def edit
    @ticket = Ticket.find(params[:id])
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Ticket.new(params[:ticket])

    respond_to do |format|
      if @ticket.save
        format.html { redirect_to @ticket, notice: 'Ticket was successfully created.' }
        format.json { render json: @ticket, status: :created, location: @ticket }
      else
        format.html { render action: "new" }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tickets/1
  # PUT /tickets/1.json
  def update
    @ticket = Ticket.find(params[:id])

    respond_to do |format|
      if @ticket.update_attributes(params[:ticket])
        format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket = Ticket.find(params[:id])
    @ticket.destroy

    respond_to do |format|
      format.html { redirect_to tickets_url }
      format.json { head :no_content }
    end
  end
end
