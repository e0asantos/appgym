class DashboardsController < ApplicationController
  # CSRF protection with exceptions for specific actions
  protect_from_forgery with: :exception, except: [:salesInfo, :salesInfo2, :salesInfo3]

  # Removed enable_sync (see notes below)

  def dummy
    redirect_to root_path and return
  end

  def salesInfo
    fecha_base = DateTime.new(Time.now.year, Time.now.month, 1)
    ventas = 5.times.map do |counter|
      inicio_periodo = fecha_base - counter.months
      fin_periodo = fecha_base - (counter - 1).month
      {
        period: inicio_periodo.strftime('%Y %m'),
        sales: Pago.where(networkid: session[:cuser].networkid)
                   .where(created_at: inicio_periodo...fin_periodo)
                   .sum(:cantidad).to_f,
        profit: Pago.where(networkid: session[:cuser].networkid)
                    .where(created_at: inicio_periodo...fin_periodo)
                    .sum(:descuento).to_f
      }
    end

    respond_to do |format|
      format.html
      format.json { render json: ventas }
    end
  end

  def salesInfo2
    fecha_base = DateTime.new(Time.now.year, 12, 1)
    ventas = 12.times.map do |counter|
      inicio_periodo = fecha_base - counter.months
      fin_periodo = fecha_base - (counter - 1).month
      Usuario.where(created_at: inicio_periodo...fin_periodo).count
    end.reverse

    respond_to do |format|
      format.html
      format.json { render json: ventas }
    end
  end

  def salesInfo3
    fecha_base = DateTime.new(Time.now.year, 12, 1)
    ventas = 12.times.map do |counter|
      inicio_periodo = fecha_base - counter.months
      fin_periodo = fecha_base - (counter - 1).month
      Pago.where(networkid: session[:cuser].networkid)
          .where(created_at: inicio_periodo...fin_periodo)
          .count
    end.reverse

    respond_to do |format|
      format.html
      format.json { render json: ventas }
    end
  end

  def index
    return redirect_to "/usuarios/login" unless session[:cuser]

    @dashboards = Dashboard.all
    @dashboard = Dashboard.new
    calculate_daily_cut
    calculate_member_activity

    respond_to do |format|
      format.html
      format.json { render json: @dashboards }
    end
  end

  def calculate_member_activity
    @current_month_start = DateTime.new(Time.now.year, Time.now.month, 1)
    @current_month_end = @current_month_start.end_of_month

    # Nuevos este mes
    @nuevos = if session[:cuser].tipo == 1
                Usuario.where(created_at: @current_month_start..@current_month_end).count.to_i
              else
                Usuario.where(networkid: session[:cuser].networkid)
                       .where(created_at: @current_month_start..@current_month_end)
                       .count.to_i
              end

    # Pagos este mes (non-inscription packages)
    pagos_query = session[:cuser].tipo == 1 ? Pago.all : Pago.where(networkid: session[:cuser].networkid)
    @pagos = pagos_query.joins("LEFT JOIN paquetes ON paquetes.id = pagos.paquete")
                        .where('pagos.created_at >= ? AND pagos.created_at <= ?', @current_month_start, @current_month_end)
                        .where('paquetes.inscripcion = 0 OR paquetes.inscripcion IS NULL')
                        .count.to_i
  end

  def show
    @dashboard = Dashboard.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @dashboard }
    end
  end

  def new
    @dashboard = Dashboard.new
    respond_to do |format|
      format.html
      format.json { render json: @dashboard }
    end
  end

  def edit
    @dashboard = Dashboard.find(params[:id])
  end

  def create
    @dashboard = Dashboard.new(dashboard_params)
    respond_to do |format|
      if @dashboard.save
        format.html { redirect_to @dashboard, notice: 'Dashboard was successfully created.' }
        format.json { render json: @dashboard, status: :created, location: @dashboard }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @dashboard.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @dashboard = Dashboard.find(params[:id])
    respond_to do |format|
      if @dashboard.update(dashboard_params)
        format.html { redirect_to @dashboard, notice: 'Dashboard was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @dashboard.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @dashboard = Dashboard.find(params[:id])
    @dashboard.destroy
    respond_to do |format|
      format.html { redirect_to dashboards_url, notice: 'Dashboard was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def dashboard_params
    params.require(:dashboard).permit(:user_in) # Adjust based on your Dashboard model attributes
  end

  def calculate_daily_cut
    fecha_inicio = DateTime.now.beginning_of_day
    fecha_fin = DateTime.now.end_of_day

    @thiscut = {
      'efectivo' => 0,
      'tarjeta' => 0,
      'deuda' => 0,
      'morosos' => 0,
      'total' => 0,
      'terminal' => 0,
      'internet' => 0,
      'pagos' => []
    }

    # Convenio entries
    Usuario.where(isaservice: 1).each do |tercero|
      Entrada.where(networkid: session[:cuser].networkid)
             .where(created_at: fecha_inicio..fecha_fin)
             .where(usuarios_id: tercero.id)
             .each do |_entrada|
        @thiscut['pagos'] << {
          'nombre' => "#{tercero.nombre} #{tercero.apellido}",
          'por' => 'Convenio',
          'monto' => '50',
          'metodo' => 'Convenio'
        }
      end
    end

    # Process payments by gateway
    payment_types = {
      'openpay' => { method: 'Tarjeta', condition: ->(p) { p.autorizacion.present? } },
      'openpay_internet' => { method: 'Internet', condition: ->(p) { p.autorizacion.nil? } },
      'terminal' => { method: 'Terminal', condition: ->(_) { true } },
      nil => { method: 'Efectivo', condition: ->(_) { true } }
    }

    payment_types.each do |gateway, config|
      next if gateway == 'openpay_internet' && payment_types.key?('openpay') # Handle separately
      query = Pago.where(networkid: session[:cuser].networkid)
                  .where(created_at: fecha_inicio..fecha_fin)
      query = gateway == 'openpay_internet' ? query.where(gateway: 'openpay', autorizacion: nil) : query.where(gateway: gateway)
      query.each do |single_pago|
        usuario = Usuario.find_by(id: single_pago.usuarios_id)
        next unless usuario && config[:condition].call(single_pago)
        autorizador = Usuario.find_by(id: single_pago.autorizacion)
        @thiscut['pagos'] << {
          'nombre' => "#{usuario.nombre} #{usuario.apellido}",
          'por' => autorizador ? "#{autorizador.nombre} #{autorizador.apellido}" : 'N/A',
          'monto' => single_pago.cantidad,
          'metodo' => config[:method]
        }
      end
    end

    Rails.logger.debug @thiscut.inspect
  end
end