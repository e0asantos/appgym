class DiccionariosController < ApplicationController
  # GET /diccionarios
  # GET /diccionarios.json
  def index
    @diccionarios = Diccionario.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @diccionarios }
    end
  end

  # GET /diccionarios/1
  # GET /diccionarios/1.json
  def show
    @diccionario = Diccionario.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @diccionario }
    end
  end

  # GET /diccionarios/new
  # GET /diccionarios/new.json
  def new
    @diccionario = Diccionario.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @diccionario }
    end
  end

  # GET /diccionarios/1/edit
  def edit
    @diccionario = Diccionario.find(params[:id])
  end

  # POST /diccionarios
  # POST /diccionarios.json
  def create
    @diccionario = Diccionario.new(params[:diccionario])

    respond_to do |format|
      if @diccionario.save
        format.html { redirect_to @diccionario, notice: 'Diccionario was successfully created.' }
        format.json { render json: @diccionario, status: :created, location: @diccionario }
      else
        format.html { render action: "new" }
        format.json { render json: @diccionario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /diccionarios/1
  # PUT /diccionarios/1.json
  def update
    @diccionario = Diccionario.find(params[:id])

    respond_to do |format|
      if @diccionario.update_attributes(params[:diccionario])
        format.html { redirect_to @diccionario, notice: 'Diccionario was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @diccionario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diccionarios/1
  # DELETE /diccionarios/1.json
  def destroy
    @diccionario = Diccionario.find(params[:id])
    @diccionario.destroy

    respond_to do |format|
      format.html { redirect_to diccionarios_url }
      format.json { head :no_content }
    end
  end
end
