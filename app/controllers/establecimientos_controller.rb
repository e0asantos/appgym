#encoding: utf-8
class EstablecimientosController < ApplicationController
  # GET /establecimientos
  # GET /establecimientos.json
  def index
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    # if session[:cuser].tipo!=1
    #   redirect_to "/"
    #   return
    # end
    withAccess=Access.where(:adduser=>1,:user_id=>session[:cuser].id).index_by(&:establecimiento_id).keys
    @establecimientos = Establecimiento.where(:networkid=>session[:cuser].networkid)
    if session[:cuser].tipo==isAdmin()
      @establecimientos=Establecimiento.all
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @establecimientos }
    end
  end

  # GET /establecimientos/1
  # GET /establecimientos/1.json
  def show

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @establecimiento }
    end
  end

  # GET /establecimientos/new
  # GET /establecimientos/new.json
  def new
    @establecimiento = Establecimiento.new
    @scheduleTable=Hash.new
    @scheduleTable["mon"]=[]
    @scheduleTable["tue"]=[]
    @scheduleTable["wed"]=[]
    @scheduleTable["thu"]=[]
    @scheduleTable["fri"]=[]
    @scheduleTable["sat"]=[]
    @scheduleTable["sun"]=[]
    puts @scheduleTable.inspect
    @selectedScheduleTable=[]
    @groupedBy=[]
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @establecimiento }
    end
  end

  # GET /establecimientos/1/edit
  def edit
    @establecimiento = Establecimiento.find(params[:id])
    if session[:cuser].tipo == isAdmin()
      # @establecimiento = Establecimiento.find(params[:id])
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @establecimiento = Establecimiento.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last
    else
      redirect_to "/"
      return
    end
    @scheduleTable=Hash.new
    @scheduleTable["mon"]=BoxSchedule.where(:establecimiento_id=>params[:id],:weekday=>"mon").index_by(&:timetable).keys
    @scheduleTable["tue"]=BoxSchedule.where(:establecimiento_id=>params[:id],:weekday=>"tue").index_by(&:timetable).keys
    @scheduleTable["wed"]=BoxSchedule.where(:establecimiento_id=>params[:id],:weekday=>"wed").index_by(&:timetable).keys
    @scheduleTable["thu"]=BoxSchedule.where(:establecimiento_id=>params[:id],:weekday=>"thu").index_by(&:timetable).keys
    @scheduleTable["fri"]=BoxSchedule.where(:establecimiento_id=>params[:id],:weekday=>"fri").index_by(&:timetable).keys
    @scheduleTable["sat"]=BoxSchedule.where(:establecimiento_id=>params[:id],:weekday=>"sat").index_by(&:timetable).keys
    @scheduleTable["sun"]=BoxSchedule.where(:establecimiento_id=>params[:id],:weekday=>"sun").index_by(&:timetable).keys
    puts @scheduleTable.inspect
    @selectedScheduleTable=BoxSchedule.where(:establecimiento_id=>params[:id]).index_by(&:weekday).keys
    @groupedBy=[]
    cdays=["mon","tue","wed","thu","fri","sat","sun"]
    remainingdays=["mon","tue","wed","thu","fri","sat","sun"]
    currentg=0
    cdays.each do |sday|
      if @scheduleTable[sday].length>0 and remainingdays.index(sday)!=nil
        @groupedBy.push([sday])
        remainingdays.delete(sday)
        remainingdays.each do |remain|
          if ((@scheduleTable[sday] & @scheduleTable[remain]) == @scheduleTable[sday]) and @scheduleTable[sday].length==@scheduleTable[remain].length
            @groupedBy[@groupedBy.length-1].push(remain)
            remainingdays.delete(remain)
          end
        end
      end
    end
    puts "--grupo"
    puts @groupedBy.inspect
    puts "--grupo"
    @establecimiento = Establecimiento.find(params[:id])
  end

  # POST /establecimientos
  # POST /establecimientos.json
  def create
    unless [isAdmin, isManager, isReceptionist].include?(session[:cuser].tipo)
      redirect_to "/"
      return
    end
  
    # Handle file upload if present
    uploaded_filename = nil
    if params[:establecimiento][:foto].present?
      uploaded_filename = params[:establecimiento][:foto].original_filename
      path = Rails.root.join("public", "images", uploaded_filename)
      File.binwrite(path, params[:establecimiento][:foto].read)
    end
  
    @establecimiento = Establecimiento.new(establecimiento_params(uploaded_filename))
    uniqID = SecureRandom.hex
    @establecimiento.uniq = uniqID[0..3]
    @establecimiento.networkid = session[:cuser].networkid
  
    respond_to do |format|
      if @establecimiento.save
        @permissionLevel = Access.new
        @permissionLevel.user_id = session[:cuser].id
        @permissionLevel.establecimiento_id = @establecimiento.id
        @permissionLevel.level_access = 30
        @permissionLevel.save
  
        regenerateLocationSchedule(@establecimiento, params[:fulltimetable])
        format.html { redirect_to "/establecimientos", notice: 'Se creó un Establecimiento correctamente.' }
        format.json { render json: @establecimiento, status: :created, location: @establecimiento }
      else
        format.html { render action: "new", status: :unprocessable_entity }
        format.json { render json: @establecimiento.errors, status: :unprocessable_entity }
      end
    end
  end

  def create_old
    if session[:cuser].tipo == isAdmin() or session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      
    else
      redirect_to "/"
      return
    end
    @establecimiento = Establecimiento.new(params[:establecimiento])
    if params[:establecimiento][:foto]!=nil
      puts params[:establecimiento][:foto].original_filename
      path = File.join(Rails.root + "public/images", params[:establecimiento][:foto].original_filename)
      # write the file
      File.open(path, "w+b") { |f| f.write(params[:establecimiento][:foto].read) }
      # limpiamos
      params[:establecimiento][:foto]=params[:establecimiento][:foto].original_filename
      @establecimiento.foto=params[:establecimiento][:foto]
    end
    uniqID=SecureRandom.hex
    @establecimiento.uniq=uniqID[0..3]
    @establecimiento.networkid=session[:cuser].networkid
    if params[:establecimiento][:openpayprueba] == 'on' or params[:establecimiento]['openpayprueba'] == 'on'
      @establecimiento.openpayprueba == 1      
    end
    respond_to do |format|
      if @establecimiento.save
        @permissionLevel=Access.new
        @permissionLevel.user_id=session[:cuser].id
        @permissionLevel.establecimiento_id=@establecimiento.id
        @permissionLevel.level_access=30
        @permissionLevel.save

        regenerateLocationSchedule(@establecimiento,params[:fulltimetable])
        format.html { redirect_to "/establecimientos", notice: 'Se creó un Establecimiento correctamente.' }
        format.json { render json: @establecimiento, status: :created, location: @establecimiento }
      else
        format.html { render action: "new" }
        format.json { render json: @establecimiento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /establecimientos/1
  # PUT /establecimientos/1.json
  def update
    @establecimiento = find_establecimiento
  
    # Redirect if no valid establecimiento is found (non-admin/manager/receptionist)
    return redirect_to "/" unless @establecimiento
  
    # Handle file upload if present
    if params[:establecimiento][:foto].present?
      filename = params[:establecimiento][:foto].original_filename
      path = Rails.root.join("public", "images", filename)
      File.binwrite(path, params[:establecimiento][:foto].read)
      # Set the filename in permitted params later
    end
  
    respond_to do |format|
      if @establecimiento.update(establecimiento_params(filename))
        regenerateLocationSchedule(@establecimiento, params[:fulltimetable])
        format.html { redirect_to "/establecimientos", notice: 'Establecimiento correctamente editado.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit", status: :unprocessable_entity }
        format.json { render json: @establecimiento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /establecimientos/1
  # DELETE /establecimientos/1.json
  def destroy
    @establecimiento = Establecimiento.find(params[:id])
    if session[:cuser].tipo == isAdmin()
      # @establecimiento = Establecimiento.find(params[:id])
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @establecimiento = Establecimiento.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last
    else
      redirect_to "/"
      return
    end
    @establecimiento.destroy

    respond_to do |format|
      format.html { redirect_to establecimientos_url }
      format.json { head :no_content }
    end
  end
  
  private
  
  def find_establecimiento
    case session[:cuser]&.tipo
    when isAdmin
      Establecimiento.find(params[:id])
    when isManager, isReceptionist
      Establecimiento.where(id: params[:id], networkid: session[:cuser].networkid).last
    else
      nil
    end
  end
  
  def establecimiento_params(uploaded_filename = nil)
    permitted = params.require(:establecimiento).permit(
      :beacon,:major,:minor,:nombre, :calle, :numero, :colonia, :servicios, :telefono, :email, 
      :descripcion, :coordenadas, :openpayprueba, :openpayid, :openpaypub, :openpaypri, :foto
    )
    # Convert openpayprueba to boolean
    permitted[:openpayprueba] = permitted[:openpayprueba] == "on"
    # Set foto to filename if uploaded
    permitted[:foto] = uploaded_filename if uploaded_filename
    permitted
  end
  
  def update_old

    @establecimiento = Establecimiento.find(params[:id])
    if session[:cuser].tipo == isAdmin()
      # @establecimiento = Establecimiento.find(params[:id])
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @establecimiento = Establecimiento.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last
    else
      redirect_to "/"
      return
    end
    
    if params[:establecimiento][:foto]!=nil
      puts params[:establecimiento][:foto].original_filename
      path = File.join(Rails.root + "public/images", params[:establecimiento][:foto].original_filename)
      # write the file
      File.open(path, "w+b") { |f| f.write(params[:establecimiento][:foto].read) }
      # limpiamos
      params[:establecimiento][:foto]=params[:establecimiento][:foto].original_filename
    end
    if params[:establecimiento][:openpayprueba] == "on" or params[:establecimiento]['openpayprueba'] == "on"
      params[:establecimiento][:openpayprueba] = 1
    else 
      params[:establecimiento][:openpayprueba] = 0
    end

    
    respond_to do |format|
      if @establecimiento.update(params[:establecimiento])
        regenerateLocationSchedule(@establecimiento,params[:fulltimetable])
        format.html { redirect_to "/establecimientos", notice: 'Establecimiento correctamente editado.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @establecimiento.errors, status: :unprocessable_entity }
      end
    end
  end

  def regenerateLocationSchedule(location,timetable)
    # borrartodos los horarios previos
    previousRecords=BoxSchedule.where(:establecimiento_id=>location.id).destroy_all
    if timetable!=""
      hashtime=JSON.parse(timetable)
        puts "---XTIME"
        puts hashtime.inspect
        puts "XTIME--"
        hashtime.each do |singleDay|
          singleDay[1].each do |singleHourAvailable|
            scheduleToRecord=BoxSchedule.new
            scheduleToRecord.weekday=singleDay[0]
            scheduleToRecord.networkid=location.networkid
            scheduleToRecord.timetable=singleHourAvailable
            scheduleToRecord.establecimiento_id=location.id
            scheduleToRecord.save
          end
        end
    end
    
  end

  
end
