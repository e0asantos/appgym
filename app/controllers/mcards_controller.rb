class McardsController < ApplicationController
  # GET /mcards
  # GET /mcards.json
  def index
    @mcards = Mcard.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @mcards }
    end
  end

  # GET /mcards/1
  # GET /mcards/1.json
  def show
    @mcard = Mcard.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @mcard }
    end
  end

  # GET /mcards/new
  # GET /mcards/new.json
  def new
    @mcard = Mcard.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @mcard }
    end
  end

  # GET /mcards/1/edit
  def edit
    @mcard = Mcard.find(params[:id])
  end

  # POST /mcards
  # POST /mcards.json
  def create
    @mcard = Mcard.new(params[:mcard])

    respond_to do |format|
      if @mcard.save
        format.html { redirect_to @mcard, notice: 'Mcard was successfully created.' }
        format.json { render json: @mcard, status: :created, location: @mcard }
      else
        format.html { render action: "new" }
        format.json { render json: @mcard.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /mcards/1
  # PUT /mcards/1.json
  def update
    @mcard = Mcard.find(params[:id])

    respond_to do |format|
      if @mcard.update_attributes(params[:mcard])
        format.html { redirect_to @mcard, notice: 'Mcard was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @mcard.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mcards/1
  # DELETE /mcards/1.json
  def destroy
    @mcard = Mcard.find(params[:id])
    @mcard.destroy

    respond_to do |format|
      format.html { redirect_to mcards_url }
      format.json { head :no_content }
    end
  end
end
