class EntrenamientosController < ApplicationController
  # GET /entrenamientos
  # GET /entrenamientos.json
  def index
    @entrenamientos = Entrenamiento.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @entrenamientos }
    end
  end

  # GET /entrenamientos/1
  # GET /entrenamientos/1.json
  def show
    @entrenamiento = Entrenamiento.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @entrenamiento }
    end
  end

  # GET /entrenamientos/new
  # GET /entrenamientos/new.json
  def new
    @entrenamiento = Entrenamiento.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @entrenamiento }
    end
  end

  # GET /entrenamientos/1/edit
  def edit
    @entrenamiento = Entrenamiento.find(params[:id])
  end

  # POST /entrenamientos
  # POST /entrenamientos.json
  def create
    @entrenamiento = Entrenamiento.new(params[:entrenamiento])

    respond_to do |format|
      if @entrenamiento.save
        format.html { redirect_to @entrenamiento, notice: 'Entrenamiento was successfully created.' }
        format.json { render json: @entrenamiento, status: :created, location: @entrenamiento }
      else
        format.html { render action: "new" }
        format.json { render json: @entrenamiento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /entrenamientos/1
  # PUT /entrenamientos/1.json
  def update
    @entrenamiento = Entrenamiento.find(params[:id])

    respond_to do |format|
      if @entrenamiento.update_attributes(params[:entrenamiento])
        format.html { redirect_to @entrenamiento, notice: 'Entrenamiento was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @entrenamiento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entrenamientos/1
  # DELETE /entrenamientos/1.json
  def destroy
    @entrenamiento = Entrenamiento.find(params[:id])
    @entrenamiento.destroy

    respond_to do |format|
      format.html { redirect_to entrenamientos_url }
      format.json { head :no_content }
    end
  end
end
