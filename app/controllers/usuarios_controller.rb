#encoding: utf-8
require 'sendgrid-ruby'
include SendGrid
class UsuariosController < ApplicationController
  helper_method :age
  protect_from_forgery except: [:mostActiveUsers,:createToken,:coinlessLogin,:coinlessRegister,:coinlessPrice,:getCoinlessPrice]
  
  def coinlessPrice
    resultProcess="OK"
    prev=Usuario.where(:email=>"precio").last
    if prev==nil
      usuario=Usuario.new
      usuario.email="precio"
      usuario.nombre="30"
      usuario.save
    end
    prev.nombre=params[:precio]
    respond_to do |format|
        format.json { render json: resultProcess.to_json }
    end    
  end

  def statsByUser
    # this method will generate the stats for the user starting with excercises made, visits so far, visits this year and time so far, time this year
    finalStats=Hash.new
    finalStats["excercises"]=20
    finalStats["visits"]=0
    finalStats["visitsThisYear"]=0
    finalStats["time"]=0
    finalStats["timeThisYear"]=0
    # number of consecutive days acumulated in the gym, use the entradas table, search the last 10 days and then review if they are consecutive
    consecutiveDays=0
    lastTenDays=Entrada.where(:usuarios_id=>session[:cuser].id).order('created_at DESC').limit(10)
    lastTenDays.each do |singleDay|
      if consecutiveDays==0
        consecutiveDays=1
      else
        if lastTenDays[lastTenDays.index(singleDay)-1].created_at.to_date==singleDay.created_at.to_date+1.day
          consecutiveDays=consecutiveDays+1
        end
      end
    end
    finalStats["consecutiveDays"]=consecutiveDays

    
    # first we get the excercises
    # to do
    # second we get the visits
    totalVisits=Asistencium.where(:user_id=>session[:cuser].id).length
    finalStats["visits"]=totalVisits
    # third we get the visits this year
    totalVisitsThisYear=Asistencium.where(:user_id=>session[:cuser].id).where('created_at>=?',Date.today.beginning_of_year).length
    finalStats["visitsThisYear"]=totalVisitsThisYear
    # fourth we get the time
    totalMinutes=0
    asistencias=Asistencium.where(:user_id=>session[:cuser].id)
    asistencias.each do |singleAsistencia|
      if singleAsistencia.minutos!=nil
        totalMinutes=totalMinutes+singleAsistencia.minutos
      end
    end
    finalStats["time"]=totalMinutes
    # fifth we get the time this year
    totalMinutesThisYear=0
    asistenciasThisYear=Asistencium.where(:user_id=>session[:cuser].id).where('created_at>=?',Date.today.beginning_of_year)
    asistenciasThisYear.each do |singleAsistencia|
      if singleAsistencia.minutos!=nil
        totalMinutesThisYear=totalMinutesThisYear+singleAsistencia.minutos
      end
    end
    finalStats["timeThisYear"]=totalMinutesThisYear
    respond_to do |format|
      format.json { render json: finalStats.to_json}
    end
  end

  def statsByUser_old(idUser)
    workWeek=[Date.today.monday,Date.today.monday+1,Date.today.monday+2,Date.today.monday+3,Date.today.monday+4,Date.today.monday+5]
    # puts "------today"
    # puts Date.today.monday
    # puts "------today"
    # primero seleccionamos las asistencias del usuario
    finalStats=[]
    cIndex=0
    workWeek.each do |singleDay|
      minutos=0
      asistencias=Asistencium.where('created_at>=?',singleDay).where(:user_id=>session[:cuser].id).where('created_at<=?',singleDay+1.day)
      puts asistencias.inspect
      elementos=asistencias.index_by(&:transition).keys.uniq
      if elementos.length==1 and elementos.join("").index("enter")!=nil
        futuro=asistencias.first.arrival_time+3.hours
        if Time.now>futuro
          # hay que insertar un elemento de fin en 1 hora
          finTiempo=Asistencium.new
          finTiempo.user_id=session[:cuser].id
          finTiempo.establecimiento_id=asistencias.first.establecimiento_id
          finTiempo.networkid=asistencias.first.transition
          finTiempo.activo=asistencias.first.activo
          finTiempo.arrival_time=asistencias.first.arrival_time+1.hour
          finTiempo.transition="enter"
          finTiempo.save
          minutos=60
        else
          minutos=TimeDifference.between(asistencias.first.arrival_time, Time.now).in_minutes
        end
      end
      if elementos.length==1 and elementos.join("").index("exit")!=nil
          # hay que insertar un elemento de principio en 1 hora anterior
          finTiempo=Asistencium.new
          finTiempo.user_id=session[:cuser].id
          finTiempo.establecimiento_id=asistencias.last.establecimiento_id
          finTiempo.networkid=asistencias.last.transition
          finTiempo.activo=asistencias.last.activo
          finTiempo.arrival_time=asistencias.last.arrival_time-1.hour
          finTiempo.transition="exit"
          finTiempo.save
          minutos=60
      end
      if minutos==0
        waitFor="enter"
        fechaInicio=nil
        asistencias.each do |singleAsistencia|
          # debemos empezar con una entrada
          if singleAsistencia.transition==waitFor
            if fechaInicio!=nil
              minutos=minutos+TimeDifference.between(fechaInicio, singleAsistencia.arrival_time).in_minutes
            else
              fechaInicio=singleAsistencia.arrival_time
            end

            if waitFor=="enter"
              waitFor="exit"
            else
              waitFor="enter"
              fechaInicio=nil
            end
          end
        end
        finalStats.push(minutos)
      end
      puts "------asistencia"
      puts asistencias.inspect
      end

    respond_to do |format|
      format.json { render json: finalStats.to_json}
    end
  end

  def mostActiveUsers
    resultProcess=Hash.new

    # buscar las asistencias
    inicioSemana=Date.today.monday
    finDeSemana=Date.today.monday+6
    puts session[:cuser].inspect
    asistencias=Asistencium.where('created_at>=?',inicioSemana).where('created_at <= ?',finDeSemana).where(:networkid=>session[:cuser].networkid)
    asistencias.each do |singleAsistencia|
      if resultProcess[singleAsistencia.user_id]==nil
        resultProcess[singleAsistencia.user_id]=Hash.new
        resultProcess[singleAsistencia.user_id]["minutos"]=0;
        resultProcess[singleAsistencia.user_id]["picture"]=Usuario.find_by_id(singleAsistencia.user_id).foto;
        resultProcess[singleAsistencia.user_id]["nombre"]=Usuario.find_by_id(singleAsistencia.user_id).nombre+" "+Usuario.find_by_id(singleAsistencia.user_id).apellido
      end
      if singleAsistencia.minutos!=nil
        resultProcess[singleAsistencia.user_id]["minutos"]=resultProcess[singleAsistencia.user_id]["minutos"]+singleAsistencia.minutos
      end
    end
    puts resultProcess.inspect
    mediumResult=[]
    resultProcess.each do |singleIn|
      mediumResult.push(singleIn[1])
    end
    sorted = mediumResult.sort_by { |k| k["minutos"] }
    puts sorted.inspect
    # sorteamos los valores
    puts mediumResult.inspect

    respond_to do |format|
        format.json { render json: sorted.take(6).to_json }
    end
  end

  def getCoinlessPrice

    prev=Usuario.where(:email=>"precio").last
    if prev==nil
      usuario=Usuario.new
      usuario.email="precio"
      usuario.nombre="30"
      usuario.save
    end
    

    respond_to do |format|
        format.json { render json: prev.to_json }
    end 
  end

  def coinlessRegister
    resultProcess="COIN_OK"
    usuario=Usuario.where(:email=>params[:muser],:password=>params[:mpwd]).last
    if usuario!=nil
      resultProcess="COIN_FAIL"
    else
      nuevo=Usuario.new
      nuevo.email=params[:muser]
      nuevo.password=params[:mpwd]
      nuevo.nombre=params[:mname]
      nuevo.apellido=params[:mlastname]
      nuevo.color_coche=params[:color]
      nuevo.modelo_coche=params[:modelo]
      nuevo.marca_coche=params[:marca]
      nuevo.placa=params[:placa]
      nuevo.save
    end
    respond_to do |format|
        format.json { render json: resultProcess.to_json }
    end
  end
  def coinlessLogin
    usuario=Usuario.where(:email=>params[:muser],:password=>params[:mpwd]).last
    resultProcess=nil
    if usuario!=nil
      resultProcess="COIN_ONLINE"  
    else
      resultProcess="COIN_REJECT"
    end
    
    respond_to do |format|
        format.json { render json: resultProcess.to_json }
    end
  end

  def downloadBeacons
    responseHash=Hash.new
    responseHash["content"]="welcome"
    responseHash["type"]="OK"
    beacons=Establecimiento.where("beacon is not null")
    respond_to do |format|
        format.json { render json: beacons.to_json  }
    end
  end

  def downloadPlacesFromNetwork
    responseHash=Hash.new
    responseHash["content"]="welcome"
    responseHash["type"]="OK"
    beacons=Establecimiento.where(:networkid=>session[:cuser].networkid)
    respond_to do |format|
        format.json { render json: beacons.to_json  }
    end
  end

  def loginMobile
    puts params.inspect
    responseHash=Hash.new
    responseHash["content"]="welcome"
    responseHash["type"]="OK"
    thisUser=Usuario.where(:email=>params[:email],:password=>params[:password]).last
    session[:cuser]=thisUser
    responseHash["content"]=thisUser

    if thisUser==nil
      responseHash["content"]="Nombre de usuario o contraseña inválidos"
      responseHash["type"]="error"
    end

    respond_to do |format|
        format.json { render json: responseHash.to_json  }
    end
  end

  def createMobile
    responseHash=Hash.new
    responseHash["content"]="Cuenta creada con éxito, ahora puedes iniciar sesión"
    responseHash["type"]="OK"
    @usuario = Usuario.new
    @usuario.nombre=params[:nombre]
    @usuario.apellido=params[:apellido]
    @usuario.password=params[:password]
    @usuario.email=params[:email]
    @usuario.sexo=params[:sex]
    @usuario.nacimiento=params[:birth]
    @usuario.tipo=6

    # buscar el network id y si existe, asignarlo
    if Usuario.find_by_networkid(params[:networkid])!=nil
      @usuario.networkid=params[:networkid]
    end
    
    if @usuario.numsocio==nil
      r = Random.new
      @usuario.numsocio=r.rand(100...9999)
    end
    if Usuario.where(:email=>@usuario.email).length>0
        responseHash["content"]='El correo del usuario que intentas registrar ya esta ocupado'
        responseHash["type"]="error"
    else
      @usuario.save
    end
    respond_to do |format|
        format.json { render json: responseHash.to_json  }
    end
  end

  def createToken
    respuesta=Hash.new
    respuesta[:token]=form_authenticity_token.to_s
    respuesta[:me]=session[:cuser]
    respond_to do |format|
        format.json { render json: respuesta.to_json  }
      end
  end

  def welcomeUser
    # supertoken e5dfc84e
    # gym 1 suitch token:709e5c87
    urltoe= "https://www.suitch.network/metric/execJSON?supertoken=e5dfc84e&token=709e5c87&command=on"
    uri = URI(urltoe)
    from_service=Net::HTTP.get(uri)
    respond_to do |format|
      format.json { render json: "ok".to_json }
    end
  end

  def openDoor
    # verify if the device is WIFI
    device = Device.find_by_uid(params[:uid])
    if device != nil and device.pluggedas == 2 and (session[:cuser].tipo == isAdmin() or device.networkid == session[:cuser].networkid)
      urltoe= "https://www.suitch.network/devices/send?token="+device.uid+"&secret_token="+device.secret+"&content=off"
      uri = URI(urltoe)
      from_service=Net::HTTP.get(uri)
      respond_to do |format|
        format.json { render json: "ok".to_json }
      end
      return
    end
    respond_to do |format|
      format.json { render json: "not_found".to_json }
    end
  end

  def closeDoor
    device = Device.find_by_uid(params[:uid])
    if device != nil and device.pluggedas == 2 and (session[:cuser].tipo == isAdmin() or device.networkid == session[:cuser].networkid)
      urltoe= "https://www.suitch.network/devices/send?token="+device.uid+"&secret_token="+device.secret+"&content=on"
      uri = URI(urltoe)
      from_service=Net::HTTP.get(uri)
      respond_to do |format|
        format.json { render json: "ok".to_json }
      end
      return
    end
    respond_to do |format|
      format.json { render json: "not_found".to_json }
    end
  end

  def fakeUser
    entrada=Entrada.new
    entrada.usuarios_id=params[:usuario].to_i
    @newPushValue=Usuario.find_by_id(params[:usuario].to_i)
    @newPushValue.save
    render :nothing=>true

  end

  def buscarNombre
    usuario = nil
    if session[:cuser].tipo == isAdmin()
      usuario=Usuario.find_by_numsocio(params[:usuario].to_i)
    else 
      usuario=Usuario.where(:networkid=>session[:cuser].networkid, :numsocio=>params[:usuario].to_i).last
    end
    respuesta=Hash.new
    respuesta['nombre']="No se encontró usuario."
    respuesta['plan']=0
    respuesta['planDesc']="Plan: Sin plan actualmente"
    respuesta['deuda']="Sin deuda este mes"
    respuesta['inscripcion']="NOT_PAID"
    respuesta["type"]="error"
    respuesta["log_errors"]=[]
    respuesta['photo'] = ''            
    if usuario!=nil
      respuesta["type"]="OK"
      respuesta['photo']=usuario.foto
      formaDePago=getPaymentDetails(usuario)
      respuesta['opid']=formaDePago.openpayid
      respuesta['oppub']=formaDePago.openpaypub
      fechaBase = DateTime.new(Time.now.strftime("%Y").to_i,Time.now.strftime("%m").to_i,1)
      if usuario.paquete!="" and usuario.paquete!=nil
        if Paquete.where(:id=>usuario.paquete).last!=nil
          pagosCorriendo=Pago.where('vence > ?',fechaBase).where(:usuarios_id=>usuario.id).sum(:cantidad_original)
          if pagosCorriendo.to_f<Paquete.where(:id=>usuario.paquete).last.precio.to_f
            respuesta['deuda']="Deuda del mes $"+(Paquete.where(:id=>usuario.paquete).last.precio.to_f-pagosCorriendo.to_f).to_s
            respuesta["log_errors"].push("suscription_not_paid")
          end
        else
          respuesta["log_errors"].push("package_not_active")
        end
      else
        respuesta['deuda']="Adeudo"
        respuesta["log_errors"].push("not_package")
      end
      pagosInscripciones=Pago.where(:usuarios_id=>usuario.id).where('paquete is NOT NULL').where("pausa=0 or pausa IS NULL").index_by(&:paquete).keys
      
      # aqui hacemos la busqueda para verificar si el paquete es de caracter inscripcion
      pagosInscripciones.each do |singleInscripcion|
        if Paquete.where(:id=>singleInscripcion,:inscripcion=>1).length>0
          respuesta['inscripcion']="OK_PAID"
        end
      end
      respuesta['nombre']=usuario.nombre+" "+usuario.apellido
      respuesta['plan']=usuario.paquete
      if usuario.paquete!=nil and usuario.paquete!=""
        respuesta['planDesc']='Plan: '+Paquete.where(:id=>usuario.paquete).last.nombre
      end
    end
    puts respuesta.inspect

    respond_to do |format|
      format.json { render json: respuesta }
    end
  end
  def age(dob)
    now = Time.now.utc.to_date
    now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
  end
  # GET /usuarios
  # GET /usuarios.json

  def index
    if session[:cuser].nil?
      redirect_to "/"
      return
    end
    unless [1, 2, 4].include?(session[:cuser].tipo)
      redirect_to "/"
      return
    end
  
    current_page = params[:page] || 1
    @establecimientos = Establecimiento.where(networkid: session[:cuser].networkid)
  
    @usuarios = case session[:cuser].tipo
                when 1 # Admin
                  base_query = Usuario.all
                when 2 # Manager
                  base_query = Usuario.where(networkid: session[:cuser].networkid)
                when 4 # Receptionist
                  base_query = Usuario.where(gymorigin: session[:cuser].gymorigin)
                end
  
    # Apply search filter if query is present
    if params[:query].present?
      search_term = "%#{params[:query].strip}%"
      @usuarios = @usuarios.where(
        "CONCAT(nombre, ' ', apellido) LIKE ? OR nombre LIKE ? OR apellido LIKE ?",
        search_term, search_term, search_term
      )
    end
  
    @usuarios = @usuarios.paginate(page: current_page, per_page: 18)
    @custom_paginate_renderer = custom_paginate_renderer
  
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @usuarios }
    end
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
    if session[:cuser]==nil
      redirect_to "/"
      return
    end

    @usuario = Usuario.find(params[:id])
    if session[:cuser].tipo == isAdmin()
      
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @usuario = Usuario.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last
    else
      redirect_to "/"
      return
    end
    # issue with usuario
    @usuario.nacimiento = @usuario.nacimiento+1.day
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @usuario }
    end
  end

  # GET /usuarios/new
  # GET /usuarios/new.json
  def new
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    @usuario = Usuario.new
    if session[:cuser].tipo == isAdmin()
      
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      
    else
      redirect_to "/"
      return
    end
    @allowedPlaces=nil
    @paquetesDisponibles=Paquete.where('inscripcion is null or inscripcion=0')
    if session[:cuser].tipo==1
      @allowedPlaces=Establecimiento.all
    else
      @paquetesDisponibles=Paquete.where(:networkid=>session[:cuser].networkid).where('inscripcion is null or inscripcion=0')
      withAccess=Access.where(:adduser=>1,:user_id=>session[:cuser].id).index_by(&:establecimiento_id).keys
      puts withAccess.inspect
      # @allowedPlaces=Establecimiento.where(:id=>withAccess)
      @allowedPlaces=Establecimiento.where(:networkid=>session[:cuser].networkid)
      if @allowedPlaces.length==0
        redirect_to "/"
        return
      end
    end
    
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @usuario }
    end
  end

  # GET /usuarios/1/edit
  def edit
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    if session[:cuser].tipo == isAdmin()
      
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      
    else
      redirect_to "/"
      return
    end
    withAccess=Access.where(:adduser=>1,:user_id=>session[:cuser].id).index_by(&:establecimiento_id).keys
    @allowedPlaces = nil
    @paquetesDisponibles=Paquete.where(:networkid=>session[:cuser].networkid).where('inscripcion is null or inscripcion=0')
    if session[:cuser].tipo==isAdmin()
      @allowedPlaces=Establecimiento.all
    elsif session[:cuser].tipo==isManager() or session[:cuser].tipo==isReceptionist()
      @allowedPlaces=Establecimiento.where(:networkid=>session[:cuser].networkid)
    else
      @allowedPlaces=Establecimiento.where(:id=>session[:cuser].gymorigin)
    end

    if session[:cuser].tipo==isAdmin() or session[:cuser].tipo==isManager() or session[:cuser].tipo==isReceptionist()
      @usuario = Usuario.find(params[:id])
      @usuario.nacimiento = @usuario.nacimiento+1.day
    else 
      if Access.where(:user_id=>session[:cuser].id,:adduser=>1).length>0 or session[:cuser].tipo==1 or session[:cuser].tipo==2
        @usuario = Usuario.find(session[:cuser].id)
        @usuario.nacimiento = @usuario.nacimiento+1.day
      else 
        puts session[:cuser].id.to_s+"=="+params[:id].to_s
        if session[:cuser].id.to_i==params[:id].to_i
         @usuario = Usuario.find(params[:id]) 
         @usuario.nacimiento = @usuario.nacimiento+1.day
         @allowedPlaces=Establecimiento.where(:networkid=>session[:cuser].networkid)
        else
          redirect_to "/"
          return
        end
      end
    end
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    if session[:cuser].nil?
      redirect_to "/"
      return
    end
  
    unless [isAdmin(), isManager(), isReceptionist()].include?(session[:cuser].tipo)
      redirect_to "/"
      return
    end
  
    if params[:usuario][:foto].present?
      path = Rails.root.join("public/images", params[:usuario][:foto].original_filename)
      File.open(path, "w+b") { |f| f.write(params[:usuario][:foto].read) }
      params[:usuario][:foto] = params[:usuario][:foto].original_filename
    end
  
    @usuario = Usuario.new(usuario_params)
    @usuario.nombre = @usuario.nombre.camelize
    @usuario.apellido = @usuario.apellido.camelize
  
    if Usuario.exists?(email: @usuario.email)
      redirect_to "/usuarios/new", notice: 'El correo del usuario que intentas registrar ya está ocupado'
      return
    end
  
    @usuario.numsocio ||= rand(100...9999)
    @usuario.isaservice = params[:usuario][:isaservice] == "on" ? 1 : 0
  
    if params[:imageToForm].present?
      pathb = Rails.root.join("public/images", "#{@usuario.nombre}#{@usuario.id}.png")
      File.open(pathb, "w+b") { |f| f.write(Base64.decode64(params[:imageToForm][21..])) }
      @usuario.foto = "#{@usuario.nombre}#{@usuario.id}.png"
    end
  
    if @usuario.save
      if session[:cuser].tipo == 1 && @usuario.tipo == 2
        @usuario.networkid = rand(100...99999)
      elsif [2, 4].include?(session[:cuser].tipo)
        @usuario.networkid = session[:cuser].networkid
      end
      @usuario.save
  
      redirect_to "/pagos?socio=#{@usuario.numsocio}", notice: 'Se creó un usuario correctamente.'
    else
      render action: "new"
    end
  end
  
  
  

  # PUT /usuarios/1
  # PUT /usuarios/1.json
  def update
    if session[:cuser].nil?
      redirect_to "/"
      return
    end
  
    @usuario = Usuario.find_by(id: params[:id])
  
    if session[:cuser].tipo == isAdmin()
      # Admin can update any user
    elsif [isManager(), isReceptionist()].include?(session[:cuser].tipo)
      @usuario = Usuario.find_by(id: params[:id], networkid: session[:cuser].networkid)
    elsif session[:cuser] != nil
      # the user can update itself
      @usuario = Usuario.find_by(id: session[:cuser].id)
    else
      redirect_to "/"
      return
    end
  
    return redirect_to "/", alert: "Usuario no encontrado" if @usuario.nil?
  
    # Handle file upload
    fileUpload = nil
    if params[:usuario][:foto].present? && !params[:usuario][:foto].is_a?(String)
      filename = params[:usuario][:foto].original_filename
      path = Rails.root.join("public/images", filename)
      fileUpload = filename
      File.open(path, "w+b") { |f| f.write(params[:usuario][:foto].read) }
      @usuario.foto = filename
    end
  
    # Handle base64 image
    if params[:imageToForm].present?
      save_base64_image(params[:imageToForm], @usuario)
    end
  
    if params[:usuario][:imageToForm].present?
      save_base64_image(params[:usuario][:imageToForm], @usuario)
    end
  
    # Update attributes except file-related ones
    if @usuario.update(usuario_params)
      @usuario.isaservice = params[:usuario][:isaservice] == "on" ? 1 : 0
      @usuario.nacimiento += 1.day  # Fix birthday issue
      if fileUpload != nil
        @usuario.foto = fileUpload
      end
      @usuario.save
  
      respond_to do |format|
        format.html { redirect_to "/usuarios", notice: 'El usuario se modificó correctamente.' }
        format.json { render json: @usuario, status: :ok }
      end
    else
      respond_to do |format|
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def save_base64_image(image_data, usuario)
    return if image_data.blank?
  
    filename = "#{usuario.nombre}#{usuario.id}.png"
    path = Rails.root.join("public/images", filename)
  
    image_data = image_data.to_s
    image_data = image_data[21..] if image_data.start_with?("data")
  
    File.open(path, "w+b") { |f| f.write(Base64.decode64(image_data)) }
    usuario.foto = filename
  end
  
  def update_old
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    @usuario = Usuario.find(params[:id])
    if session[:cuser].tipo == isAdmin()
      
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @usuario = Usuario.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last
    else
      redirect_to "/"
      return
    end
    # @usuario.save(params[:usuario][:foto])
    puts "-------"
    if params[:usuario][:foto]!=nil
      
      if params[:usuario][:foto].is_a? String
        
      else
        puts params[:usuario][:foto].original_filename
        path = File.join(Rails.root + "public/images", params[:usuario][:foto].original_filename)
        # write the file
        File.open(path, "w+b") { |f| f.write(params[:usuario][:foto].read) }
        # limpiamos
        @usuario.foto=params[:usuario][:foto].original_filename
        params[:usuario][:foto]=params[:usuario][:foto].original_filename
      end
    end
    if params[:imageToForm]!="" and params.has_key?(:imageToForm)
       pathb = File.join(Rails.root + "public/images", @usuario.nombre+@usuario.id.to_s+'.png')
       if params[:imageToForm].index("data")!=nil
        File.open(pathb, 'w+b') {|f| f.write(Base64.decode64(params[:imageToForm].to_s[21..-1]))}  
       else 
        File.open(pathb, 'w+b') {|f| f.write(Base64.decode64(params[:imageToForm]))}
       end
       params.delete :imageToForm
      
      @usuario.foto=@usuario.nombre+@usuario.id.to_s+'.png'
    end
    if params[:usuario][:imageToForm]!="" and params[:usuario].has_key?(:imageToForm)
       pathb = File.join(Rails.root + "public/images", @usuario.nombre+@usuario.id.to_s+'.png')
       if params[:usuario][:imageToForm].index("data")!=nil
        File.open(pathb, 'w+b') {|f| f.write(Base64.decode64(params[:usuario][:imageToForm].to_s[21..-1]))}  
       else 
        File.open(pathb, 'w+b') {|f| f.write(Base64.decode64(params[:usuario][:imageToForm]))}
       end
       params[:usuario].delete :imageToForm
      
      @usuario.foto=@usuario.nombre+@usuario.id.to_s+'.png'
      @usuario.save
    end
  # buscamos complejos
  # permisos=Entrada.where(:usuarios_id=>params[:id])
  # permisos.each do |singlePermiso|
  #   singlePermiso.destroy

  # end
  # Establecimiento.all.each do |singleEstablecimiento|
  #   if params['establecimiento-'+singleEstablecimiento.id.to_s]!=nil
  #     # buscamos si ya existe
  #     permiso=Entrada.where(:usuarios_id=>params[:id],:establecimientos_id=>singleEstablecimiento.id).last
  #     if permiso==nil
  #       permiso=Entrada.new
  #     end
  #     if params['establecimiento-'+singleEstablecimiento.id.to_s]=="on"
  #       permiso.establecimientos_id=singleEstablecimiento.id
  #       permiso.usuarios_id=params[:id]
  #       permiso.save
  #     else
  #       permiso.destroy
  #     end
  #   end
  
  # end
    puts "-------"
    # enviar mail
    arusuario=[]
    usobjeto=Hash.new
    usobjeto['email']=@usuario.email
    usobjeto['name']=@usuario.nombre+" "+@usuario.apellido
    usobjeto['type']='to'
    
    arusuario.push(usobjeto)
    # problem with birthday
    @usuario.nacimiento = @usuario.nacimiento+1.day
    # sendMailToAddress(arusuario,"Tu contraseña ha cambiado a :"+@usuario.password,"Cambio de contraseña")
    respond_to do |format|
      if @usuario.update_attributes(params[:usuario].except(:imageToForm,:foto))
        
        if params[:usuario][:isaservice]=="on"
          @usuario.isaservice=1
        else
          @usuario.isaservice=0
        end
        @usuario.save
        format.html { redirect_to "/usuarios", notice: 'El usuario se modifico correctamente.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  def sendMailToAddress_bak(adresses,message,title)
    mandrill = Mandrill::API.new 'zC7yPLbPFNbpYqWLbxs3IA'
              message = {"html"=>'',
                   "text"=>message,
                   "subject"=>title,
                   "from_email"=>"enviofactura@cfdi.ws",
                   "from_name"=>"Crossfit MX - Correo",
                   "to"=>
                      adresses,
                   
                   "important"=>false,
                   "track_opens"=>nil,
                   "track_clicks"=>nil,
                   "auto_text"=>nil,
                   "auto_html"=>nil,
                   "inline_css"=>nil,
                   "url_strip_qs"=>nil,
                   "preserve_recipients"=>nil,
                   "view_content_link"=>nil,
                   "tracking_domain"=>nil,
                   "signing_domain"=>nil,
                   "return_path_domain"=>nil,
                   "merge"=>true,
                   "global_merge_vars"=>[{"name"=>"merge1", "content"=>"merge1 content"}],
                   "merge_vars"=>
                      [{"rcpt"=>"recipient.email@example.com",
                          "vars"=>[{"name"=>"merge2", "content"=>"merge2 content"}]}],
                   "tags"=>["password-resets"],
                   "attachments"=>
                      [],
                   "images"=>
                      []}
                  async = false
                  ip_pool = "Main Pool"
                  send_at = DateTime.now-2.hours
                  # result = mandrill.messages.send message, async, ip_pool, send_at
                  # puts result.inspect
  end

  def sendMailToAddress(arrayEmail,contenido,titulo)
    clientMail = SendGrid::API.new(api_key: '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')
    singleMail = SendGrid::Mail.new
    singleMail.from = Email.new(email: 'noreply@consultware.mx')
    singleMail.subject = 'SPORTFIT - ' + titulo
    personalization = Personalization.new
      finalArrayEmail=[]
      arrayEmail.each do |singleEmail|
        personalization.to=Email.new(email: singleEmail, name: "Deportista")
      end
      singleMail.personalizations=personalization
      content = SendGrid::Content.new(type: 'text/plain', value: contenido)
      singleMail.contents = content;
      
  
      # puts clientMail.send(mail) 
      response=clientMail.client.mail._('send').post(request_body: singleMail.to_json)
      puts response.status_code
      puts response.body
      puts response.headers
  end

  def login
    # Add any logic here if needed (e.g., setting instance variables for the view)
    # If no logic is needed, this can be left empty; Rails will automatically render the view
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    @usuario = Usuario.find(params[:id])
    if session[:cuser].tipo == isAdmin()
      
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @usuario = Usuario.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last
    else
      redirect_to "/"
      return
    end
    @usuario.destroy

    respond_to do |format|
      format.html { redirect_to usuarios_url }
      format.json { head :no_content }
    end
  end
  private
  
  def usuario_params
    params.require(:usuario).permit(:tipo, :gymorigin, :numsocio, :paquete, :nombre, :apellido, :sexo,
                                    :email, :password, :peso, :altura, :casillero, :nacimiento, :notas,
                                    :calle, :numero, :colonia, :telefonocelular, :contactoEmpresa,
                                    :contactoOcupacion, :nfc, :finalfecha, :foto, :isaservice, :biometric,:cpostal,:estados,:ciudad)
  end
end
