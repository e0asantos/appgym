#encoding: utf-8
require 'mail'
require 'sendgrid-ruby'
include SendGrid
class CorreosController < ApplicationController
  # GET /correos
  # GET /correos.json
  def index
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    @correos = Correo.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @correos }
    end
  end

  # GET /correos/1
  # GET /correos/1.json
  def show
    @correo = Correo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @correo }
    end
  end

  # GET /correos/new
  # GET /correos/new.json
  def new
    @correo = Correo.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @correo }
    end
  end

  # GET /correos/1/edit
  def edit
    @correo = Correo.find(params[:id])
  end

  def contactus
    if (params[:yourname].length <40 and params[:email].length<40 and params[:email].length<40) and (params[:yourname].length >5 and params[:email].length>5 and params[:email].length>5)
      sendMailToAddress(["asb.studios@gmail.com"], params[:yourname] + '('+params[:email]+')' + " con telefono "+ params[:yoursubject], "INTERESADO")
      respond_to do |format|
        format.html { redirect_to "/", notice: 'Los correos se estan enviando.' }
        format.json { render json: {status:"mail_sent", into:'#wpcf7-f1253-p1252-o1',message:' Te llamaremos a la brevedad, hemos recibido tu información.'}.to_json }
      end
    else 
      respond_to do |format|
        format.html { redirect_to "/", notice: 'Ocurrio un error.' }
        format.json { render json: {status:"mail_sent", into:'#wpcf7-f1253-p1252-o1',message:' Ocurrio un error.'}.to_json }
      end
    end
    
  end

  # POST /correos
  # POST /correos.json
  def create
    @correo = Correo.new(params[:correo])
    @correo.usuarioscomo=params[:optionsRadios]

    userToSend=nil
    if session[:cuser].tipo==isReceptionist() or session[:cuser].tipo==isManager()
      userToSend=Usuario.where(:networkid=>session[:cuser].networkid)
    elsif session[:cuser].tipo==isAdmin()
      userToSend=Usuario.all
    else
      redirect_to "/"
      return
    end
    # sacamos el usuario y enviamos mail
    if params[:optionsRadios]=="usuarios_todos"

      arusuario=[]
      userToSend.each do |singleUser|
        if singleUser.email!=nil and singleUser.email!=""
          puts "---enviando mail:"+singleUser.email
          usobjeto=Hash.new
          usobjeto['email']=singleUser.email
          usobjeto['name']=singleUser.nombre+" "+singleUser.apellido
          usobjeto['type']='to'
          arusuario.push(usobjeto)
        end
      end
      puts arusuario.inspect
      # sendMailToAddress(arusuario,@correo.contenido,@correo.titulo)
      # sendMandrill()
    elsif params[:optionsRadios]=="usuarios_activos"
      pagos=Pago.where(['vence >= ?', DateTime.now]).where(:networkid=>session[:cuser].networkid)
      indices=pagos.index_by(&:usuarios_id).keys
      arusuario=[]
      userToSend.where(:id=>indices).each do |singleUser|
        if singleUser.email!=nil and singleUser.email!=""
          
        
          puts "---enviando mail:"+singleUser.email
          usobjeto=Hash.new
          usobjeto['email']=singleUser.email
          usobjeto['name']=singleUser.nombre+" "+singleUser.apellido
          usobjeto['type']='to'
          arusuario.push(usobjeto)
        end
      end
      puts arusuario.inspect
      # sendMailToAddress(arusuario,@correo.contenido,@correo.titulo)
    elsif params[:optionsRadios]=="usuarios_inactivos"
      pagosNoVencidos=Pago.where(['vence >= ?', DateTime.now]).where(:networkid=>session[:cuser].networkid)
      indicesNoVencidos=pagosNoVencidos.index_by(&:usuarios_id).keys
      indicesNoVencidosString=indicesNoVencidos.join(" ")
      pagosVencidos=Pago.where(['vence < ?', DateTime.now]).where(['usuarios_id not in (?)', indicesNoVencidos]).where(:networkid=>session[:cuser].networkid)
      indicesPagosVencidos=pagosVencidos.index_by(&:usuarios_id).keys
      puts pagosVencidos.inspect
      
      arusuario=[]
      userToSend.where(:id=>indicesPagosVencidos).each do |singleUser|
        if singleUser.email!=nil and singleUser.email!=""
          puts "---enviando mail:"+singleUser.email
          usobjeto=Hash.new
          usobjeto['email']=singleUser.email
          usobjeto['name']=singleUser.nombre+" "+singleUser.apellido
          usobjeto['type']='to'
          arusuario.push(usobjeto)
        end
      end
      puts arusuario.inspect
      # sendMailToAddress(arusuario,@correo.contenido,@correo.titulo)
    end
        
      

    respond_to do |format|
      if @correo.save
        format.html { redirect_to "/notificaciones", notice: 'Los correos se estan enviando.' }
        format.json { render json: @correo, status: :created, location: @correo }
      else
        format.html { render action: "new" }
        format.json { render json: @correo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /correos/1
  # PUT /correos/1.json
  def update
    @correo = Correo.find(params[:id])

    respond_to do |format|
      if @correo.update_attributes(params[:correo])
        format.html { redirect_to "/notificaciones", notice: 'Los correos se estan enviando.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @correo.errors, status: :unprocessable_entity }
      end
    end
  end

  def sendMailToAddress_bak(adresses,message,title)
    mandrill = Mandrill::API.new 'zC7yPLbPFNbpYqWLbxs3IA'
              message = {"html"=>'',
                   "text"=>message,
                   "subject"=>title,
                   "from_email"=>"enviofactura@cfdi.ws",
                   "from_name"=>"Gym - Correo",
                   "to"=>
                      adresses,
                   
                   "important"=>false,
                   "track_opens"=>nil,
                   "track_clicks"=>nil,
                   "auto_text"=>nil,
                   "auto_html"=>nil,
                   "inline_css"=>nil,
                   "url_strip_qs"=>nil,
                   "preserve_recipients"=>nil,
                   "view_content_link"=>nil,
                   "tracking_domain"=>nil,
                   "signing_domain"=>nil,
                   "return_path_domain"=>nil,
                   "merge"=>true,
                   "global_merge_vars"=>[{"name"=>"merge1", "content"=>"merge1 content"}],
                   "merge_vars"=>
                      [{"rcpt"=>"recipient.email@example.com",
                          "vars"=>[{"name"=>"merge2", "content"=>"merge2 content"}]}],
                   "tags"=>["password-resets"],
                   "attachments"=>
                      [],
                   "images"=>
                      []}
                  async = false
                  ip_pool = "Main Pool"
                  send_at = DateTime.now-2.hours
                  result = mandrill.messages.send message, async, ip_pool, send_at
                  puts result.inspect
  end

  def sendMailToAddress(arrayEmail,contenido,titulo)
    clientMail = SendGrid::API.new(api_key: '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')
    singleMail = SendGrid::Mail.new
    singleMail.from = Email.new(email: 'noreply@consultware.mx')
    singleMail.subject = 'SPORTFIT - ' + titulo
    personalization = Personalization.new
      finalArrayEmail=[]
      arrayEmail.each do |singleEmail|
        personalization.to=Email.new(email: singleEmail, name: "Deportista")
      end
      singleMail.personalizations=personalization
      content = SendGrid::Content.new(type: 'text/plain', value: contenido)
      singleMail.contents = content;
      
  
      # puts clientMail.send(mail) 
      response=clientMail.client.mail._('send').post(request_body: singleMail.to_json)
      puts response.status_code
      puts response.body
      puts response.headers
  end

  

  # DELETE /correos/1
  # DELETE /correos/1.json
  def destroy
    @correo = Correo.find(params[:id])
    @correo.destroy

    respond_to do |format|
      format.html { redirect_to correos_url }
      format.json { head :no_content }
    end
  end
end
