class WodItemsController < ApplicationController
  # GET /wod_items
  # GET /wod_items.json
  def index
    @wod_items = WodItem.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @wod_items }
    end
  end

  # GET /wod_items/1
  # GET /wod_items/1.json
  def show
    @wod_item = WodItem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @wod_item }
    end
  end

  # GET /wod_items/new
  # GET /wod_items/new.json
  def new
    @wod_item = WodItem.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @wod_item }
    end
  end

  # GET /wod_items/1/edit
  def edit
    @wod_item = WodItem.find(params[:id])
  end

  # POST /wod_items
  # POST /wod_items.json
  def create
    @wod_item = WodItem.new(params[:wod_item])

    respond_to do |format|
      if @wod_item.save
        format.html { redirect_to @wod_item, notice: 'Wod item was successfully created.' }
        format.json { render json: @wod_item, status: :created, location: @wod_item }
      else
        format.html { render action: "new" }
        format.json { render json: @wod_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /wod_items/1
  # PUT /wod_items/1.json
  def update
    @wod_item = WodItem.find(params[:id])

    respond_to do |format|
      if @wod_item.update_attributes(params[:wod_item])
        format.html { redirect_to @wod_item, notice: 'Wod item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @wod_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /wod_items/1
  # DELETE /wod_items/1.json
  def destroy
    @wod_item = WodItem.find(params[:id])
    @wod_item.destroy

    respond_to do |format|
      format.html { redirect_to wod_items_url }
      format.json { head :no_content }
    end
  end
end
