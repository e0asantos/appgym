class BoxSchedulesController < ApplicationController
  # GET /box_schedules
  # GET /box_schedules.json
  def index
    @box_schedules = BoxSchedule.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @box_schedules }
    end
  end

  # GET /box_schedules/1
  # GET /box_schedules/1.json
  def show
    @box_schedule = BoxSchedule.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @box_schedule }
    end
  end

  # GET /box_schedules/new
  # GET /box_schedules/new.json
  def new
    @box_schedule = BoxSchedule.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @box_schedule }
    end
  end

  # GET /box_schedules/1/edit
  def edit
    @box_schedule = BoxSchedule.find(params[:id])
  end

  # POST /box_schedules
  # POST /box_schedules.json
  def create
    @box_schedule = BoxSchedule.new(params[:box_schedule])

    respond_to do |format|
      if @box_schedule.save
        format.html { redirect_to @box_schedule, notice: 'Box schedule was successfully created.' }
        format.json { render json: @box_schedule, status: :created, location: @box_schedule }
      else
        format.html { render action: "new" }
        format.json { render json: @box_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /box_schedules/1
  # PUT /box_schedules/1.json
  def update
    @box_schedule = BoxSchedule.find(params[:id])

    respond_to do |format|
      if @box_schedule.update_attributes(params[:box_schedule])
        format.html { redirect_to @box_schedule, notice: 'Box schedule was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @box_schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /box_schedules/1
  # DELETE /box_schedules/1.json
  def destroy
    @box_schedule = BoxSchedule.find(params[:id])
    @box_schedule.destroy

    respond_to do |format|
      format.html { redirect_to box_schedules_url }
      format.json { head :no_content }
    end
  end
end
