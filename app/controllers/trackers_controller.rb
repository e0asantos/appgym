class TrackersController < ApplicationController
  # GET /trackers
  # GET /trackers.json
  def index
    @trackers = Tracker.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @trackers }
    end
  end

  # GET /trackers/1
  # GET /trackers/1.json
  def show
    @tracker = Tracker.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tracker }
    end
  end

  # GET /trackers/new
  # GET /trackers/new.json
  def new
    @tracker = Tracker.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tracker }
    end
  end

  # GET /trackers/1/edit
  def edit
    @tracker = Tracker.find(params[:id])
  end

  def weightStats
    puts session[:cuser].inspect
    @trackers = Tracker.where(:caracteristica=>"peso",:usuario_id=>session[:cuser].id).index_by(&:valor).keys

    respond_to do |format|
      format.json{ render json: @trackers}
    end
  end

  # POST /trackers
  # POST /trackers.json
  def create
    @tracker = Tracker.new(tracker_params)
    @tracker.usuario_id=session[:cuser].id
    @tracker.caracteristica = params[:caracteristica]
    @tracker.valor = params[:valor]
    respond_to do |format|
      if @tracker.save
        format.html { redirect_to @tracker, notice: 'Tracker was successfully created.' }
        format.json { render json: @tracker, status: :created, location: @tracker }
      else
        format.html { render action: "new" }
        format.json { render json: @tracker.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /trackers/1
  # PUT /trackers/1.json
  def update
    @tracker = Tracker.find(params[:id])

    respond_to do |format|
      if @tracker.update(tracker_params)
        format.html { redirect_to @tracker, notice: 'Tracker was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @tracker.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trackers/1
  # DELETE /trackers/1.json
  def destroy
    @tracker = Tracker.find(params[:id])
    @tracker.destroy

    respond_to do |format|
      format.html { redirect_to trackers_url }
      format.json { head :no_content }
    end
  end

  def tracker_params()
    permitted = params.require(:tracker).permit(
      :caracteristica, :valor)
    permitted
  end
end
