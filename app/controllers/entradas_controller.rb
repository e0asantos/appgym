require 'pusher'
require 'thread'

class EntradasController < ApplicationController
  # CSRF protection with exception for logWithCard
  protect_from_forgery with: :exception, except: [:logWithCard]

  # Removed enable_sync (see notes below)

  before_action :setup_pusher, only: [:logWithCard, :logWithKeypad]

  private

  def setup_pusher
    Pusher.app_id = ENV['PUSHER_APP_ID'] || '95011'
    Pusher.key = ENV['PUSHER_KEY'] || '038727943ef67c486a35'
    Pusher.secret = ENV['PUSHER_SECRET'] || 'ef4fbb7ecffe7b7f8fed'
    Pusher.cluster = 'mt1'
    Pusher.logger = Rails.logger
    Pusher.encrypted = true
  end

  public

  def logWithUser
    entrando=Usuario.where(:email=>params[:username],:password=>params[:password])
    if entrando.length==1

      session[:cuser]=entrando.last
      if session[:cuser].tipo==1
        redirect_to "/dashboards"  
      else 
        redirect_to "/pagos"
      end
      
      return
    else
      redirect_to "/"
    end
  end

  def showKeypad
    @establecimiento_id=params[:uid]
    @isshow = false
    render :keypadlogin
  end

  def showNFC
    @isshow = false
    render :nfclogin
  end

  def showBiometric
    @isshow = false
    render :biometriclogin
  end

  def logOut
    session[:cuser]=nil
    redirect_to "/"
    return
  end

  def logWithCard
    usuario, establecimiento = find_user_and_establecimiento
    channel_hash = determine_channel_hash

    if usuario
      entrada = Entrada.create!(establecimientos_id: establecimiento, usuarios_id: usuario.id)

      if usuario.isaservice == 1 && (usuario.finalfecha.nil? || usuario.finalfecha >= DateTime.now)
        update_dashboard_signal(usuario)
        trigger_pusher(channel_hash, usuario, 'OK')
        return render json: 'OK'
      end

      handle_rewards_and_payment(usuario)
      pago_activo = Pago.where(usuarios_id: usuario.id).where('vence >= ?', Time.now).exists?
      if pago_activo || (usuario.finalfecha.present? && usuario.finalfecha >= DateTime.now)
        trigger_suitch_network if params[:device].present?
        trigger_pusher(channel_hash, usuario, 'OK')
        return render json: 'OK'
      else
        trigger_pusher(channel_hash, usuario, 'FAIL')
        return render json: 'NOT'
      end
    end

    trigger_pusher_not_found(channel_hash)
    render json: 'NOT'
  end

  def logWithKeypad
    usuario = if session[:cuser].tipo == isAdmin
      Usuario.find_by(numsocio: params[:usuario].to_i)
    else
      Usuario.find_by(networkid: session[:cuser].networkid, numsocio: params[:usuario].to_i)
    end
    handle_rewards_and_payment(usuario)
    respuesta = default_response
    if usuario
      respuesta = build_keypad_response(usuario)
      trigger_suitch_network_keypad if respuesta['log_errors'].empty? && respuesta['type'] != 'error'
    end

    respond_to do |format|
      format.json { render json: respuesta }
    end
  end

  def logWithBeacon
    # Placeholder for future implementation
  end

  # Standard CRUD actions
  def index
    return redirect_to root_path unless session[:cuser]
    @entradas = []
    @isshow = false
    respond_to do |format|
      format.html
      format.json { render json: @entradas }
    end
  end

  def show
    @entrada = Entrada.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @entrada }
    end
  end

  def new
    @entrada = Entrada.new
    respond_to do |format|
      format.html
      format.json { render json: @entrada }
    end
  end

  def edit
    @entrada = Entrada.find(params[:id])
  end

  def create
    @entrada = Entrada.new(entrada_params)
    respond_to do |format|
      if @entrada.save
        format.html { redirect_to @entrada, notice: 'Entrada was successfully created.' }
        format.json { render json: @entrada, status: :created, location: @entrada }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @entrada.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @entrada = Entrada.find(params[:id])
    respond_to do |format|
      if @entrada.update(entrada_params)
        format.html { redirect_to @entrada, notice: 'Entrada was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @entrada.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @entrada = Entrada.find(params[:id])
    @entrada.destroy
    respond_to do |format|
      format.html { redirect_to entradas_url, notice: 'Entrada was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def entrada_params
    params.require(:entrada).permit(:establecimientos_id, :usuarios_id, :networkid, :paquete) # Adjust as needed
  end

  def find_user_and_establecimiento
    if params[:device].present?
      device_type = params[:device]['type']
      if device_type == 'nfc'
        [Usuario.find_by(nfc: params[:device]['result']), params[:place]]
      elsif device_type == 'bio'
        biometric_value = "#{params[:device]['productId']}#{params[:device]['result']}"
        [Usuario.find_by(biometric: biometric_value), params[:place]]
      end
    else
      [Usuario.find_by(id: params[:userin]), Establecimiento.find_by(major: params[:major])&.id]
    end
  end

  def determine_channel_hash
    return 'njksdfwe23' unless session[:cuser]
    session[:cuser].gymorigin.present? ? Establecimiento.find_by(id: session[:cuser].gymorigin)&.uniq : session[:cuser].networkid
  end

  def update_dashboard_signal(usuario)
    update_signal = Dashboard.find_by(id: 1)
    update_signal&.update!(user_in: usuario.id)
  end

  def handle_rewards_and_payment(usuario)
    Rails.logger.info "Handling rewards and payment for user: #{usuario.id}"
    asistencias = Entrada.where(usuarios_id: usuario.id).count
    pagos_count = Pago.where(usuarios_id: usuario.id).count
    recompensas = Reward.where(asistencia: asistencias,networkid: usuario.networkid)

    recompensas.each do |single_reward|
      next if RecompensaObtenida.exists?(rewards_id: single_reward.id) and single_reward.numpagos < pagos_count
      Rails.logger.info "Found a matching reward: #{single_reward.titulo}"
      # verify if the payments are enough to get the reward
      RecompensaObtenida.create!(
        usuarios_id: usuario.id,
        rewards_id: single_reward.id,
        networkid: usuario.networkid
      )
      send_mail_to_user(usuario, single_reward)
    end

    update_dashboard_signal(usuario)
  end

  def send_mail_to_user(usuario, reward)
    sendMailToAddress(
      [{ 'email' => usuario.email, 'name' => "#{usuario.nombre} #{usuario.apellido}", 'type' => 'to' }],
      reward.descripcion,
      reward.titulo)
  end

  def trigger_pusher(channel_hash, usuario, status)
    Pusher.trigger(channel_hash, 'issue-subscription', {
      message: '',
      avatar: "/images/#{usuario.foto}",
      name: usuario.nombre,
      lastname: usuario.apellido,
      status: status
    })
  end

  def trigger_pusher_not_found(channel_hash)
    Pusher.trigger(channel_hash, 'issue-subscription', {
      message: 'No se encontró identificador',
      avatar: 'https://placehold.co/200x150/EFEFEF/AAAAAA?text=No+registrado',
      name: 'No registrado',
      lastname: '',
      status: 'NOT'
    })
  end

  def trigger_suitch_network
    origin_devices = Device.where(uid: params[:device]['deviceId'], networkid: session[:cuser].networkid)
    origin_devices.each do |single_device|
      next unless single_device.post_action
      suitch = Device.find_by(id: single_device.post_action)
      next unless suitch
      Rails.logger.info "Suitch UID: #{suitch.uid}"
      Thread.new do
        toggle_suitch(suitch, 'on')
        sleep 6
        toggle_suitch(suitch, 'off')
      end
    end
  end

  def toggle_suitch(suitch, state)
    url = "https://www.suitch.network/devices/send?token=#{suitch.uid}&secret_token=#{suitch.secret}&content=#{state}"
    uri = URI(url)
    Net::HTTP.get(uri)
  end

  def default_response
    {
      'nombre' => 'No se encontró usuario.',
      'plan' => 0,
      'planDesc' => 'Plan: Sin plan actualmente',
      'deuda' => 'Sin deuda este mes',
      'inscripcion' => 'NOT_PAID',
      'type' => 'error',
      'log_errors' => [],
      'photo' => ''
    }
  end

  def build_keypad_response(usuario)
    respuesta = {
      'type' => 'OK',
      'photo' => usuario.foto,
      'nombre' => "#{usuario.nombre} #{usuario.apellido}",
      'plan' => usuario.paquete,
      'planDesc' => usuario.paquete.present? ? "Plan: #{Paquete.find_by(id: usuario.paquete)&.nombre}" : 'Plan: Sin plan',
      'deuda' => 'Sin deuda este mes',
      'inscripcion' => 'NOT_PAID',
      'log_errors' => []
    }
    forma_de_pago = getPaymentDetails(usuario)
    respuesta['opid'] = forma_de_pago&.openpayid
    respuesta['oppub'] = forma_de_pago&.openpaypub
    establecimiento = Establecimiento.find_by(uniq: params[:uid])
    if establecimiento == nil
      establecimiento = -1
    else
      establecimiento = establecimiento.id
    end
    entrada = Entrada.new(
          usuarios_id: usuario.id,
          networkid: usuario.networkid,
          paquete: usuario.paquete,
          establecimientos_id: establecimiento,
          status:'success'
        )
    if usuario.paquete.present?
      paquete = Paquete.find_by(id: usuario.paquete)
      if paquete
        fecha_base = DateTime.new(Time.now.year, Time.now.month, 1)
        pagos_corriendo = Pago.where('vence > ?', fecha_base).where(usuarios_id: usuario.id).sum(:cantidad_original)
        deuda = paquete.precio.to_f - pagos_corriendo.to_f

        # find if the user is allowed to enter to the gym
        allowedGym = Entrada.where(usuarios_id: nil).where(establecimientos_id: establecimiento,:paquete=>usuario.paquete).count
        
        if deuda > 0
          entrada.update(status:'fail:suscription_not_paid')
          respuesta['deuda'] = "Deuda del mes $#{deuda}"
          respuesta['log_errors'] << 'suscription_not_paid'
          entrada.save!
        elsif allowedGym <= 0
          entrada.update(status:'fail:blocked_by_package')
          respuesta['log_errors'] << 'package_not_active'
          entrada.save!
        else
          entrada.save!
        end
      else
        respuesta['log_errors'] << 'package_not_active'
        entrada.update(status:'fail:package_not_active')
        entrada.save!
      end
    else
      respuesta['deuda'] = 'Adeudo'
      respuesta['log_errors'] << 'not_package'
      entrada.update(status:'fail:not_package')
      entrada.save!
    end

    pagos_inscripciones = Pago.where(usuarios_id: usuario.id).where.not(paquete: nil).where('pausa = 0 OR pausa IS NULL').pluck(:paquete)
    respuesta['inscripcion'] = 'OK_PAID' if Paquete.where(id: pagos_inscripciones, inscripcion: 1).exists?

    respuesta
  end

  def trigger_suitch_network_keypad
    origin_devices = Device.where(action: 5, networkid: session[:cuser].networkid)
    origin_devices.each do |single_device|
      next unless single_device.post_action
      suitch = Device.find_by(id: single_device.post_action)
      next unless suitch
      Rails.logger.info "Suitch UID: #{suitch.uid}"
      Thread.new do
        toggle_suitch(suitch, 'on')
        sleep 6
        toggle_suitch(suitch, 'off')
      end
    end
  end
end