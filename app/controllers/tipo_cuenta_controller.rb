class TipoCuentaController < ApplicationController
  # GET /tipo_cuenta
  # GET /tipo_cuenta.json
  def index
    @tipo_cuenta = TipoCuentum.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tipo_cuenta }
    end
  end

  # GET /tipo_cuenta/1
  # GET /tipo_cuenta/1.json
  def show
    @tipo_cuentum = TipoCuentum.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tipo_cuentum }
    end
  end

  # GET /tipo_cuenta/new
  # GET /tipo_cuenta/new.json
  def new
    @tipo_cuentum = TipoCuentum.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tipo_cuentum }
    end
  end

  # GET /tipo_cuenta/1/edit
  def edit
    @tipo_cuentum = TipoCuentum.find(params[:id])
  end

  # POST /tipo_cuenta
  # POST /tipo_cuenta.json
  def create
    @tipo_cuentum = TipoCuentum.new(params[:tipo_cuentum])

    respond_to do |format|
      if @tipo_cuentum.save
        format.html { redirect_to @tipo_cuentum, notice: 'Tipo cuentum was successfully created.' }
        format.json { render json: @tipo_cuentum, status: :created, location: @tipo_cuentum }
      else
        format.html { render action: "new" }
        format.json { render json: @tipo_cuentum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tipo_cuenta/1
  # PUT /tipo_cuenta/1.json
  def update
    @tipo_cuentum = TipoCuentum.find(params[:id])

    respond_to do |format|
      if @tipo_cuentum.update_attributes(params[:tipo_cuentum])
        format.html { redirect_to @tipo_cuentum, notice: 'Tipo cuentum was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @tipo_cuentum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tipo_cuenta/1
  # DELETE /tipo_cuenta/1.json
  def destroy
    @tipo_cuentum = TipoCuentum.find(params[:id])
    @tipo_cuentum.destroy

    respond_to do |format|
      format.html { redirect_to tipo_cuenta_url }
      format.json { head :no_content }
    end
  end
end
