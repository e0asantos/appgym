class PaquetesController < ApplicationController
  # GET /paquetes
  # GET /paquetes.json
  def index
    @paquetes = Paquete.all
    if session[:cuser].tipo!=1
      @paquetes=Paquete.where(:networkid=>session[:cuser].networkid)
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @paquetes }
    end
  end

  # GET /paquetes/1
  # GET /paquetes/1.json
  def show
    @paquete = Paquete.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @paquete }
    end
  end

  # GET /paquetes/new
  # GET /paquetes/new.json
  def new
    @paquete = Paquete.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @paquete }
    end
  end

  # GET /paquetes/1/edit
  def edit
    @paquete = Paquete.find(params[:id])
  end

  # POST /paquetes
  # POST /paquetes.json
  def create
    @paquete = Paquete.new(paquete_params)
    @paquete.networkid=session[:cuser].networkid
    respond_to do |format|
      if @paquete.save
        Establecimiento.all.each do |singleEstablecimiento|
    if params['establecimiento-'+singleEstablecimiento.id.to_s]!=nil
      # buscamos si ya existe
      permiso=Entrada.new
      if permiso==nil
        permiso=Entrada.new
      end
      puts "----->"+params['establecimiento-'+singleEstablecimiento.id.to_s]
      if params['establecimiento-'+singleEstablecimiento.id.to_s]=="on"
        permiso.establecimientos_id=singleEstablecimiento.id
        permiso.paquete=@paquete.id
        permiso.save
      else
        permiso.destroy
      end
    end
  
  end
        format.html { redirect_to "/paquetes", notice: 'Paquete was successfully created.' }
        format.json { render json: @paquete, status: :created, location: @paquete }
      else
        format.html { render action: "new" }
        format.json { render json: @paquete.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /paquetes/1
  # PUT /paquetes/1.json
  def update_old
    @paquete = Paquete.find(params[:id])
    # borrar todos los establecimientos del paquete
    Entrada.where(:paquete=>@paquete.id).destroy_all
    respond_to do |format|
      if @paquete.update(params[:paquete])
        if params[:paquete][:inscripcion] == 'on' or params[:paquete]['inscripcion'] == 'on'
          @paquete.inscripcion = 1
          @paquete.save
        else 
          @paquete.inscripcion = 0
          @paquete.save
        end
        Establecimiento.all.each do |singleEstablecimiento|
          if params['establecimiento-'+singleEstablecimiento.id.to_s]!=nil
            # buscamos si ya existe
            permiso=Entrada.new
            if permiso==nil
              permiso=Entrada.new
            end
            puts "----->"+params['establecimiento-'+singleEstablecimiento.id.to_s]
            if params['establecimiento-'+singleEstablecimiento.id.to_s]=="on"
              permiso.establecimientos_id=singleEstablecimiento.id
              permiso.paquete=@paquete.id
              permiso.save
            else
              permiso.destroy
            end
          end 
        end
        format.html { redirect_to "/paquetes", notice: 'Paquete was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @paquete.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @paquete = Paquete.find(params[:id])
  
    # Destroy all existing Entrada records for this paquete
    Entrada.where(paquete: @paquete.id).destroy_all
  
    respond_to do |format|
      if @paquete.update(paquete_params)
        # Handle the inscripcion checkbox (convert 'on' to boolean)
        @paquete.update(inscripcion: params[:paquete][:inscripcion] == 'on')
  
        # Update Entrada records for each Establecimiento
        Establecimiento.all.each do |single_establecimiento|
          param_key = "establecimiento-#{single_establecimiento.id}"
          if params[param_key] == 'on'
            Entrada.create!(
              establecimientos_id: single_establecimiento.id,
              paquete: @paquete.id
            )
          end
        end
  
        format.html { redirect_to "/paquetes", notice: 'Paquete was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit", status: :unprocessable_entity }
        format.json { render json: @paquete.errors, status: :unprocessable_entity }
      end
    end
  end
  
  
  # DELETE /paquetes/1
  # DELETE /paquetes/1.json
  def destroy
    @paquete = Paquete.find(params[:id])
    @paquete.destroy
    
    respond_to do |format|
      format.html { redirect_to paquetes_url }
      format.json { head :no_content }
    end
  end
  private
  
  def paquete_params
    params.require(:paquete).permit(:nombre, :precio, :inscripcion, :meses, :horario, :horarioto)
  end
end
