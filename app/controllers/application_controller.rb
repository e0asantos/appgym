require 'uri'
require 'net/http'
require 'net/https'
require 'sendgrid-ruby'

class ApplicationController < ActionController::Base
  # CSRF protection (updated to Rails 7 default behavior)
  protect_from_forgery with: :exception

  # Helper methods for views
  helper_method :getPaymentDetails, :isAdmin, :isClient, :isConvenio, :isManager, :isNutritionist, :isReceptionist, :isTrainer

  # Instance variable accessor
  attr_accessor :newPushValue

  # Updated from after_filter to after_action
  after_action :set_access_control_headers, if: -> { Rails.env.development? }

  # Removed enable_sync (see notes below)

  def set_access_control_headers
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Request-Method'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'
    response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT'
  end

  def mobileNotification(token_array, message)
    body_to_send = {
      'tokens' => token_array,
      'profile' => Rails.env.development? ? 'development' : 'production',
      'notification' => { 'message' => message, 'title' => 'Coinless' }
    }

    uri = URI.parse('https://api.ionic.io/push/notifications')
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true
    req = Net::HTTP::Post.new(
      uri.path,
      {
        'Content-Type' => 'application/json',
        'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwYzYwNDMyNS05ZjBiLTQ5MTgtODU3My01OTlkNDRmM2I5ZjkifQ.yEon0qOkm9-1R3f8RoyjFefb6UuK3LA3ElCQzvOybNo'
      }
    )
    req.body = body_to_send.to_json
    res = https.request(req)
    Rails.logger.debug "Response #{res.code} #{res.message}: #{res.body}"
  end

  def initialize
    super # Calls ActionController::Base initialize
    Rails.logger.debug 'APPLICATION_CONTROLLER initialized'
    @newPushValue = Dashboard.find_by(id: 1) # Use find_by instead of where/last
  end

  def getBoxesAllowed
    # Empty method; add logic if needed
  end

  def changeFromPush(msg)
    self.newPushValue = msg # Use setter method provided by attr_accessor
  end

  def getPaymentDetails(pagoTarjeta)
    tmpUser=Usuario.where(:id=>pagoTarjeta.id).last
    cuentaOpenPay=nil
    if tmpUser.gymorigin!=nil
      cuentaOpenPay=Establecimiento.where(:id=>tmpUser.gymorigin).where('openpayid IS NOT NULL').last
      if cuentaOpenPay==nil
        cuentaOpenPay=Establecimiento.where('openpayid is NOT NULL').last
      end

    else
      cuentaOpenPay=Establecimiento.where('openpayid is NOT NULL').last
    end
    return cuentaOpenPay
  end

  def rewardService(usuarioid, establecimientoid, inline_return = 0)
    usuario = Usuario.find_by(id: usuarioid)
    establecimiento = establecimientoid

    return render json: 'NOT' unless usuario

    entrada = Entrada.new(
      establecimientos_id: establecimiento,
      usuarios_id: usuario.id,
      networkid: usuario.networkid,
      paquete: usuario.paquete
    )
    entrada.save!

    if usuario.isaservice == 1 && (usuario.finalfecha.nil? || usuario.finalfecha >= DateTime.now)
      update_signal = Dashboard.find_by(id: 1)
      update_signal&.update!(user_in: usuario.id)
      return render json: 'OK'
    end

    # Check rewards
    asistencias = Entrada.where(usuarios_id: usuario.id).count
    pagos_count = Pago.where(usuarios_id: usuario.id).count
    recompensas = Reward.where(asistencia: asistencias, numpagos: [0, pagos_count])

    recompensas.each do |single_reward|
      next if RecompensaObtenida.exists?(rewards_id: single_reward.id)

      nueva_r = RecompensaObtenida.create!(
        usuarios_id: usuario.id,
        rewards_id: single_reward.id,
        networkid: usuario.networkid
      )
      sendMailToAddress(
        [{ 'email' => usuario.email, 'name' => "#{usuario.nombre} #{usuario.apellido}", 'type' => 'to' }],
        single_reward.descripcion,
        single_reward.titulo
      )
    end

    update_signal = Dashboard.find_by(id: 1)
    update_signal&.update!(user_in: usuario.id)

    pago_activo = Pago.where(usuarios_id: usuario.id).where('vence >= ?', Time.now).exists?
    if pago_activo || (usuario.finalfecha.present? && usuario.finalfecha >= DateTime.now)
      inline_return.zero? ? render(json: 'OK') : 'OK'
    else
      inline_return.zero? ? render(json: 'NOT') : 'NOT'
    end
  end

  def sendMailToAddress(array_email, contenido, titulo)
    client_mail = SendGrid::API.new(api_key: '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')
    single_mail = SendGrid::Mail.new
    single_mail.from = SendGrid::Email.new(email: 'noreply@consultware.mx')
    single_mail.subject = titulo # Updated to use dynamic title

    personalization = SendGrid::Personalization.new
    array_email.each do |single_email|
      cuenta = Usuario.find_by(email: single_email['email'])
      nombre = cuenta ? "#{cuenta.nombre} #{cuenta.apellido}".strip : 'Deportista :)'
      personalization.add_to(SendGrid::Email.new(email: single_email['email'], name: nombre))
    end
    single_mail.add_personalization(personalization)

    single_mail.add_content(SendGrid::Content.new(type: 'text/plain', value: contenido))

    response = client_mail.client.mail._('send').post(request_body: single_mail.to_json)
    Rails.logger.debug "SendGrid Response: #{response.status_code} #{response.body}"
  end

  # Removed sendMailToAddress_bak (unused Mandrill code)

  def custom_paginate_renderer
    Class.new(WillPaginate::ActionView::LinkRenderer) do
      def container_attributes
        { class: 'pagination justify-content-end mb-0' }
      end

      def page_number(page)
        classes = page == current_page ? 'page-item active' : 'page-item'
        tag(:li, link(page, page, rel: rel_value(page), class: 'page-link'), class: classes)
      end

      def previous_page
        num = @collection.current_page > 1 ? @collection.current_page - 1 : nil
        previous_or_next_page(num, '<i class="fa fa-angle-left"></i><span class="sr-only">Previous</span>')
      end

      def next_page
        num = @collection.current_page < total_pages ? @collection.current_page + 1 : nil
        previous_or_next_page(num, '<i class="fa fa-angle-right"></i><span class="sr-only">Next</span>')
      end

      def previous_or_next_page(page, text)
        classes = page ? 'page-item' : 'page-item disabled'
        target = page || '#'
        tag(:li, link(text, target, class: 'page-link'), class: classes)
      end
    end
  end

  # Role check methods (unchanged but could be refactored into an enum or concern)
  def isAdmin
    1
  end

  def isManager
    2
  end

  def isTrainer
    3
  end

  def isReceptionist
    4
  end

  def isNutritionist
    5
  end

  def isClient
    6
  end

  def isConvenio
    7
  end
end