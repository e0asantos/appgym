#encoding: utf-8
require 'sendgrid-ruby'
include SendGrid
require 'mail'
require 'openpay'
class PagosController < ApplicationController
  protect_from_forgery except: :incomingFromGateway



  def corteCaja

     if session[:cuser].tipo!=isAdmin() and session[:cuser].tipo!=isManager()
      redirect_to "/"
      return
    end

    fechaInicio=DateTime.parse(params[:finicio])
    fechaFin=DateTime.parse(params[:ffin])
    fechaFin=fechaFin.change({ hour: 23, min: 59, sec: 59 })
    if params[:empleado]=="" or params[:empleado]==nil
      # usamos el usuario personal
      params[:empleado]=session[:cuser].id
    end

    thiscut=Hash.new
    thiscut['efectivo']=0
    thiscut['tarjeta']=0
    thiscut['deuda']=0
    thiscut['morosos']=0
    thiscut['total']=0
    thiscut['terminal']=0
    thiscut['convenio']=0
    thiscut['pagos']=[]

    # identificamos las entradas de terceros
    Usuario.where(:isaservice=>1).each do |singleTercero|
      Entrada.where(:networkid=>session[:cuser].networkid).where('created_at >= ?',fechaInicio).where('created_at <= ?',fechaFin).where(:usuarios_id=>singleTercero.id).each do |singleEsteUsuario|
        objef=Hash.new
        objef['nombre']=singleTercero.nombre+" "+singleTercero.apellido
        objef['por']="Convenio"
        objef['monto']="50"
        objef['fecha']=singleEsteUsuario.created_at.strftime('%d-%m-%Y %H:%M')
        objef['metodo']="Convenio"
        thiscut['convenio']=thiscut['convenio']+50
        thiscut['pagos'].push(objef)
      end
    end

    thiscut['efectivo']=Pago.where(:networkid=>session[:cuser].networkid).where('created_at >= ?',fechaInicio).where('created_at <= ?',fechaFin).where(:autorizacion=>params[:empleado],:gateway=>nil).sum(:cantidad).to_f
    Pago.where(:networkid=>session[:cuser].networkid).where('created_at >= ?',fechaInicio).where('created_at <= ?',fechaFin).where(:autorizacion=>params[:empleado],:gateway=>nil).each do |singlePago|
      if Usuario.find_by_id(singlePago.usuarios_id)!=nil
        objef=Hash.new
        objef['nombre']=Usuario.find_by_id(singlePago.usuarios_id).nombre+" "+Usuario.find_by_id(singlePago.usuarios_id).apellido
      objef['fecha']=singlePago.created_at.strftime('%d-%m-%Y %H:%M')
      objef['monto']=singlePago.cantidad
      objef['metodo']="Efectivo"
      thiscut['pagos'].push(objef)
    end
    end
    if thiscut['efectivo']==nil
      thiscut['efectivo']=0
    end
    thiscut['tarjeta']=Pago.where(:networkid=>session[:cuser].networkid).where('created_at >= ?',fechaInicio).where('created_at <= ?',fechaFin).where(:autorizacion=>params[:empleado]).where(:gateway=>'openpay').sum(:cantidad).to_f
    Pago.where(:networkid=>session[:cuser].networkid).where('created_at >= ?',fechaInicio).where('created_at <= ?',fechaFin).where(:autorizacion=>params[:empleado]).where(:gateway=>'openpay').each do |singlePago|
      if Usuario.find_by_id(singlePago.usuarios_id)!=nil
        objef=Hash.new
        objef['nombre']=Usuario.find_by_id(singlePago.usuarios_id).nombre+" "+Usuario.find_by_id(singlePago.usuarios_id).apellido
      objef['fecha']=singlePago.created_at.strftime('%d-%m-%Y %H:%M')
      objef['monto']=singlePago.cantidad
      objef['metodo']="Tarjeta"
      thiscut['pagos'].push(objef)
    end
    end
    if thiscut['tarjeta']==nil
      thiscut['tarjeta']=0
    end
    
    thiscut['terminal']=Pago.where(:networkid=>session[:cuser].networkid).where('created_at >= ?',fechaInicio).where('created_at <= ?',fechaFin).where(:autorizacion=>params[:empleado]).where(:gateway=>'terminal').sum(:cantidad).to_f
    Pago.where(:networkid=>session[:cuser].networkid).where('created_at >= ?',fechaInicio).where('created_at <= ?',fechaFin).where(:autorizacion=>params[:empleado]).where(:gateway=>'terminal').each do |singlePago|
      if Usuario.find_by_id(singlePago.usuarios_id)!=nil
        objef=Hash.new
        objef['nombre']=Usuario.find_by_id(singlePago.usuarios_id).nombre+" "+Usuario.find_by_id(singlePago.usuarios_id).apellido
        objef['fecha']=singlePago.created_at.strftime('%d-%m-%Y %H:%M')
        objef['monto']=singlePago.cantidad
        objef['metodo']="Terminal"
        thiscut['pagos'].push(objef)
      end
    end
    if thiscut['terminal']==nil
      thiscut['terminal']=0
    end
    # para conseguir la deuda, traemos todos los pagos activos y luego los clientes
    pagosActivos=Pago.where(:networkid=>session[:cuser].networkid).where('created_at >= ?',fechaInicio).where('created_at <= ?',fechaFin).where(:autorizacion=>params[:empleado])
    # despues se buscan los usuarios de los pagos
    thiscut['deuda']=Pago.where(:networkid=>session[:cuser].networkid).where('created_at >= ?',fechaInicio).where('created_at <= ?',fechaFin).where(:autorizacion=>params[:empleado]).where('gateway is NOT NULL').sum(:cantidad)
    if thiscut['deuda']==nil
      thiscut['deuda']=0
    end
    thiscut['total']=thiscut['efectivo']+thiscut['tarjeta']+thiscut['terminal']+thiscut['convenio']
    puts thiscut.inspect


    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: thiscut.to_json }
    end
  end



  def incomingFromGateway
    if params[:type]=="charge.succeeded"
      # {"type"=>"charge.created"charge.failed, "event_date"=>"2014-10-07T13:30:42-05:00", "transaction"=>{"id"=>"trhfa3hm8clr5b8avldh", "amount"=>20.44, "authorization"=>nil, "method"=>"store", "operation_type"=>"in", "transaction_type"=>"charge", "status"=>"in_progress", "currency"=>"MXN", "creation_date"=>"2014-10-07T13:30:42-05:00", "operation_date"=>"2014-10-07T13:30:42-05:00", "description"=>"Third party sale", "error_message"=>nil, "order_id"=>"123", "payment_method"=>{"type"=>"store", "reference"=>"000020TRHFA3HM8CLR5B8AVLDH0020441", "barcode_url"=>"https://sandbox-api.openpay.mx/barcode/000020TRHFA3HM8CLR5B8AVLDH0020441?width=1&height=45&text=false"}}, "pago"=>{}}
        pagoEnPausaOn=Pago.find_by_id(params[:transaction][:order_id])
        if pagoEnPausaOn == nil
          render :nothing => true, :status => 200
          return
        end
        pagoEnPausaOn.pausa=0
        pagoEnPausaOn.vence=DateTime.now+pagoEnPausaOn.restantes.days+5.days
        pagoEnPausaOn.restantes=0
        pagoEnPausaOn.networkid=Usuario.find_by_id(Pago.find_by_id(params[:transaction][:order_id]).usuarios_id).networkid
        pagoEnPausaOn.save

        createReward(Usuario.find_by_id(Pago.find_by_id(params[:transaction][:order_id]).usuarios_id).numsocio)
        # hay que revisar que este pago no tenga suscripcion, si es asi, como fue exitoso el pago, descongelar la suscripcion
        suscripcionActivate=Pago.where(:gateway=>params[:transaction][:order_id]).last
        if suscripcionActivate!=nil
          suscripcionActivate.pausa=0
          suscripcionActivate.networkid = pagoEnPausaOn.networkid
          suscripcionActivate.save
        end
        configm=Notificacione.find_by_id(1)
        if configm.mailtoadmin
          arusuario=[]
          
          Usuario.where(:tipo=>[isAdmin(), isManager()],:networkid=>pagoEnPausaOn.networkid).each do |singleAdmin|
            usobjeto=Hash.new
            usobjeto['email']=singleAdmin.email
            usobjeto['name']=singleAdmin.nombre+" "+singleAdmin.apellido
            usobjeto['type']='to'
            arusuario.push(usobjeto)
          end
          
          sendMailToAddress(arusuario,"Pago realizado con tarjeta de número de socio "+Usuario.find_by_id(Pago.find_by_id(params[:transaction][:order_id]).usuarios_id).numsocio.to_s+" la nota de pago la puede encontrar en http://www.sportfit.app/pagos/"+Pago.find_by_id(params[:transaction][:order_id]).id.to_s+"","Pago en Gym realizado")
        end
        usobjetou=Hash.new
          usobjetou['email']=Usuario.find_by_id(Pago.find_by_id(params[:transaction][:order_id]).usuarios_id).email
          usobjetou['name']=Usuario.find_by_id(Pago.find_by_id(params[:transaction][:order_id]).usuarios_id).nombre+" "+Usuario.find_by_id(Pago.find_by_id(params[:transaction][:order_id]).usuarios_id).apellido
          usobjetou['type']='to'
        sendMailToAddress([usobjetou],"Pago realizado con tarjeta de número de socio "+Usuario.find_by_id(Pago.find_by_id(params[:transaction][:order_id]).usuarios_id).numsocio.to_s+" la nota de pago la puede encontrar en http://www.sportfit.app/pagos/"+Pago.find_by_id(params[:transaction][:order_id]).id.to_s+"","Pago en Gym realizado")
    elsif params[:type]=="verification"
      verif=Reward.new
      verif.descripcion=params[:verification_code]
      verif.save

    end
      
    render :nothing => true, :status => 200
  end
  def makeCardPayment
    #     ID:
    # muen0ypxkc2xgsf6j8sq
    # Llave privada:
    # sk_29d9ae2c53764ac4903b19997d6100be
    # Llave pública:
    # pk_b3f01aada40446adaa1daef8c2ae4207
    # URL:
    # https://sandbox-api.openpay.mx/v1/
    # Parameters: {"token_id"=>"kzenbn1eksqbrom6nl6f", "authenticity_token"=>"951VlIDtB47IrQoJ8XuX6+odbmb9XzhTryzx/z+bv70=", "name"=>"juan perez", "paquete"=>"2", "amount"=>"", "descuento"=>"100", "creditcard"=>"4024007157297937", "digits"=>"611", "month"=>"06", "year"=>"18"}

    # en caso de que sea el cliente quien paga, no manda las llaves
    if params[:inscripcion] ==nil and params[:paquete] ==nil 
      params[:paquete] = session[:cuser].paquete
    end
    pagoTarjeta=Pago.new
    pagoTarjeta.gateway="openpay"
     if session[:cuser].tipo== isAdmin() or session[:cuser].tipo== isReceptionist() or session[:cuser].tipo== isManager()
      pagoTarjeta.usuarios_id=Usuario.where(:numsocio=>params[:numsocio].to_i).last.id  
    else
      pagoTarjeta.usuarios_id=session[:cuser].id
    end
    budgetControl = 0
    useParcialPayment = false
    if params[:amount] != nil and params[:amount] != '' and params[:amount] != '0' and params[:amount] != 0
      budgetControl=params[:amount].to_f
      useParcialPayment = true
    else
      # budget es la suma de la inscripcion y el paquete
      if params[:paquete] != nil and params[:paquete] != '' and params[:paquete] != '0' and params[:paquete] != 0
        budgetControl = Paquete.find_by_id(params[:paquete].to_i).precio.to_f
      end
      if params[:inscripcion]!="0" and params[:inscripcion]!=0 and params[:inscripcion]!=nil
        budgetControl = budgetControl + Paquete.find_by_id(params[:inscripcion].to_i).precio.to_f
      end
    end
    
    pagoTarjeta.cantidad=0
    pagoTarjeta.descuento=0
    pagoTarjeta.autorizacion=session[:cuser].id.to_s
    # if params[:descuento]!="" and params[:paquete]!="0" and params[:amount]!=""
    #   pagoTarjeta.descuento=params[:descuento].to_f
    # end
    nuevaInscripcion=nil
    if params[:inscripcion]!="0" and params[:inscripcion]!=0 and params[:inscripcion]!=nil and params.has_key?(:inscripcion)
      puts "TRATANDO DE INSCRIBIR"
      # entonces estan pagando una inscripcion tambien, asi que hay que crearla
      nuevaInscripcion=Pago.new
      nuevaInscripcion.gateway = 'openpay'
      nuevaInscripcion.autorizacion=session[:cuser].id.to_s
      # nuevaInscripcion.gateway="openpay"
      nuevaInscripcion.paquete=params[:inscripcion].to_i
      nuevaInscripcion.pausa=1
      nuevaInscripcion.restantes=0
      nuevaInscripcion.vence=DateTime.now
      nuevaInscripcion.usuarios_id=Usuario.where(:numsocio=>params[:numsocio].to_i).last.id
      inscripcionPaquete=Paquete.find_by_id(params[:inscripcion].to_i)
      nuevaInscripcion.cantidad_original=inscripcionPaquete.precio
      # hay que ver si traemos descuento y paquete simultaneamente de ser asi entonces no aplicamos el descuento aqui
      nuevaInscripcion.descuento=0
      if params[:descuento]!=""
        nuevaInscripcion.descuento = params[:descuento]
      end
        #   # no selecciona paquete pero si hay descuento y tiene pago parcial
      if (budgetControl-(inscripcionPaquete.precio.to_f - nuevaInscripcion.descuento.to_f ))>=0
        nuevaInscripcion.cantidad=inscripcionPaquete.precio.to_f - nuevaInscripcion.descuento.to_f
        budgetControl = budgetControl-(inscripcionPaquete.precio.to_f - nuevaInscripcion.descuento.to_f)
      elsif useParcialPayment
        nuevaInscripcion.cantidad=budgetControl
        budgetControl = 0
      else
        redirect_to "/pagos", :notice=>"El pago no procedió por que el descuento es mayor que el pago"
        return
      end
      nuevaInscripcion.save
    end
    
    if params[:paquete]!="0" and params[:paquete]!=0 and params[:paquete]!=nil and params.has_key?(:paquete)
      
      mpaquete=nil
      if session[:cuser].tipo==isAdmin() or session[:cuser].tipo==isManager() or session[:cuser].tipo==isReceptionist()
        pagoTarjeta.paquete=params[:paquete]
        mpaquete=Paquete.find_by_id(params[:paquete].to_i)
      else
        pagoTarjeta.paquete=session[:cuser].paquete
        mpaquete=Paquete.find_by_id(session[:cuser].paquete.to_i)
      end
      pagoTarjeta.vence=DateTime.now+mpaquete.meses.to_i.months
      if params[:meses].to_i.months>0
        # si selecciono mas de los meses, sobreescribir el plan
        pagoTarjeta.vence=DateTime.now+params[:meses].to_i.months
      end
      pagoTarjeta.cantidad_original=mpaquete.precio
      pagoTarjeta.restantes=(pagoTarjeta.vence-DateTime.now)/93600
      pagoTarjeta.pausa=1

      if params[:descuento]!="" and params[:descuento]!=0 and (params[:paquete]!="0" and params[:paquete]!=0) and (params[:inscripcion]=="0" or params[:inscripcion]==0)
        # si tiene descuento y tiene paquete pero no tiene inscripcion, proceder con la consideracion del descuento
        pagoTarjeta.descuento=params[:descuento]
      end
      
      # need some refactor here
      # if useBudgetControl 
      if ((budgetControl-(mpaquete.precio.to_f- pagoTarjeta.descuento.to_f))>=0) or (useParcialPayment and (budgetControl - pagoTarjeta.descuento.to_f)>0)
        pagoTarjeta.cantidad = budgetControl - pagoTarjeta.descuento.to_f
      else
        redirect_to "/pagos", :notice=>"El pago no procedió por que el descuento es mayor que el pago"
        return
      end
      # pagoTarjeta.vence=DateTime.now
      pagoTarjeta.save
    end
    
    
    
    
    # buscamos la cuenta de openpay deacuerdo por el establecimiento
    tmpUser=Usuario.where(:id=>pagoTarjeta.usuarios_id).last
    cuentaOpenPay=nil
    if tmpUser.gymorigin!=nil
      cuentaOpenPay=Establecimiento.where(:id=>tmpUser.gymorigin).where('openpayid IS NOT NULL').last
      if cuentaOpenPay==nil
        cuentaOpenPay=Establecimiento.where('openpayid is NOT NULL').last
      end

    else
      cuentaOpenPay=Establecimiento.where('openpayid is NOT NULL').last
    end
    
    merchant_id=cuentaOpenPay.openpayid
    private_key=cuentaOpenPay.openpaypri
    openpay=OpenpayApi.new(merchant_id,private_key)
    totalACobrar=pagoTarjeta.cantidad
    if nuevaInscripcion!=nil
      puts "TOTAL DE TOTALES:"+totalACobrar.inspect
      puts "TOTAL DE TOTALES2:"+nuevaInscripcion.cantidad.inspect
      totalACobrar=totalACobrar.to_f+nuevaInscripcion.cantidad.to_f
      if params[:paquete]!="0"
        nuevaInscripcion.gateway=pagoTarjeta.id.to_s
      end
      nuevaInscripcion.save
    end
    
    cargoAEnviar={:method=> "card",
      :source_id => params[:token_id],
      :amount => totalACobrar.to_s,
      :description=> "Pago de suscripcion/mensualidad",
      :device_session_id => params[:deviceIdHiddenFieldName],
      :order_id=> params[:paquete]!="0" ? pagoTarjeta.id : nuevaInscripcion.id} 
      #creamos el objeto de cargo
      puts cargoAEnviar.to_json
      charges=openpay.create(:charges)
      begin
        final_charge=charges.create(cargoAEnviar)  
      rescue Exception => e
         redirect_to "/pagos", :notice=>"La tarjeta se rechazó, intente más tarde."
          return
      end
      
      puts "1---------------------"
      puts final_charge["payment_method"]
      puts final_charge
    redirect_to "/pagos", :notice=>"El pago puede tardar hasta 5 minutos en ser reflejado en la cuenta."
    return
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: "ok".to_json }
    end
  end

  def pauseUser
    usuario=params[:userid].to_i
    # hay que ver si tenemos un pago activo
    pagoEnPausa=Pago.where('vence > ?',DateTime.now).where(:usuarios_id=>usuario).last
    pagoEnPausaOn=Pago.where(:pausa=>1).where(:usuarios_id=>usuario).last
    if pagoEnPausa!=nil
      pagoEnPausa.pausa=1
      pagoEnPausa.restantes=(pagoEnPausa.vence-DateTime.now)/93600
      pagoEnPausa.vence=DateTime.now
      pagoEnPausa.save
    elsif pagoEnPausaOn!=nil
      # seguramente no tenemos, hay que ver si el pago esta en pausa
      pagoEnPausaOn.pausa=0
      pagoEnPausaOn.vence=DateTime.now+pagoEnPausaOn.restantes.days
      pagoEnPausaOn.restantes=0
      pagoEnPausaOn.save
    end
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: 'usuario en pausa'.to_json }
    end
  end
  def makePayment
    nuevoPago=Pago.new
    nuevoPago.usuarios_id=Usuario.where(:numsocio=>params[:socio]).last.id
    nuevoPago.vence=DateTime.now+params[:meses].to_i.months
    nuevoPago.networkid=session[:cuser].networkid
    nuevoPago.cantidad=params[:monto]
    nuevoPago.save
    notice="El pago sin tarjeta se creó en el registro."
    
    redirect_to "/pagos"
  end
  def pauseUserIntern(usuariom)
    usuario=usuariom
    # hay que ver si tenemos un pago activo
    pagoEnPausa=nil #Pago.where('vence > ?',DateTime.now).where(:usuarios_id=>usuario)
    pagoEnPausaOn=Pago.where(:pausa=>1).where(:usuarios_id=>usuario)
    if pagoEnPausa!=nil
      pagoEnPausa.pausa=1
      pagoEnPausa.restantes=(pagoEnPausa.vence-DateTime.now)/93600
      pagoEnPausa.vence=DateTime.now
      pagoEnPausa.save
    pagoEnPausaOn.length>0
      # seguramente no tenemos, hay que ver si el pago esta en pausa
      pagoEnPausaOn.pausa=0
      pagoEnPausaOn.vence=DateTime.now+pagoEnPausaOn.restantes.days+5.days
      pagoEnPausaOn.restantes=0
      pagoEnPausaOn.save
    end
  end

  def makePayment
    nuevoPago=Pago.new
    nuevoPago.networkid=session[:cuser].networkid
    nuevoPago.usuarios_id=Usuario.where(:numsocio=>params[:socio]).last.id
    nuevoPago.vence=DateTime.now+params[:meses].to_i.months
    nuevoPago.cantidad=params[:monto]
    nuevoPago.save
    notice="El pago sin tarjeta se creó en el registro."
    
    redirect_to "/pagos"
  end
  def createReward(csocio)
    puts "zzzzzzzzzzzzzzzzzzzzzz"
    # busca la tarjeta nfc a quien le pertenece
    usuario=Usuario.find_by_numsocio(csocio)
    puts usuario.inspect
    if usuario!=nil
      puts "ENCONTRO USUARIO"
      # verificar recompensa
      asistencias=Entrada.where(:usuarios_id=>usuario.id)
      recompensa=Reward.where(:asistencia=>[0,asistencias.length],:numpagos=>[0,Pago.where(:usuarios_id=>usuario.id).length])
      if recompensa.length>0
        puts "ENCONTRO RECOMPENSA"
        # enviar un mail al usuario y anotarlo en recompensas
        recompensa.each do |singleReward|
          if RecompensaObtenida.where(:rewards_id=>singleReward.id,:usuarios_id=>usuario.id).length==0
            # no se ha obtenido, asi que la agregamos
            nuevaR=RecompensaObtenida.new
            nuevaR.usuarios_id=usuario.id
            nuevaR.rewards_id=singleReward.id
            nuevaR.save
            # enviar mail
            arusuario=[]
            usobjeto=Hash.new
            usobjeto['email']=usuario.email
            usobjeto['name']=usuario.nombre+" "+usuario.apellido
            usobjeto['type']='to'
            arusuario.push(usobjeto)
            sendMailToAddress(arusuario,singleReward.descripcion,singleReward.titulo)
          end
        end
      end
    end
  end

  def sendMailToAddress_bak(adresses,message,title)
    mandrill = Mandrill::API.new 'zC7yPLbPFNbpYqWLbxs3IA'
    message = {"html"=>'',
          "text"=>message,
          "subject"=>title,
          "from_email"=>"enviofactura@cfdi.ws",
          "from_name"=>"Gym - Correo",
          "to"=>
            adresses,
          
          "important"=>false,
          "track_opens"=>nil,
          "track_clicks"=>nil,
          "auto_text"=>nil,
          "auto_html"=>nil,
          "inline_css"=>nil,
          "url_strip_qs"=>nil,
          "preserve_recipients"=>nil,
          "view_content_link"=>nil,
          "tracking_domain"=>nil,
          "signing_domain"=>nil,
          "return_path_domain"=>nil,
          "merge"=>true,
          "global_merge_vars"=>[{"name"=>"merge1", "content"=>"merge1 content"}],
          "merge_vars"=>
            [{"rcpt"=>"recipient.email@example.com",
                "vars"=>[{"name"=>"merge2", "content"=>"merge2 content"}]}],
          "tags"=>["password-resets"],
          "attachments"=>
            [],
          "images"=>
            []}
        async = false
        ip_pool = "Main Pool"
        send_at = DateTime.now-2.hours
        # result = mandrill.messages.send message, async, ip_pool, send_at
        # puts result.inspect
  end
  def sendMailToAddress(arrayEmail,contenido,titulo)
    clientMail = SendGrid::API.new(api_key: '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')
    singleMail = SendGrid::Mail.new
    singleMail.from = Email.new(email: 'noreply@consultware.mx')
    singleMail.subject = 'SPORTFIT - ' + titulo
    personalization = Personalization.new
      finalArrayEmail=[]
      arrayEmail.each do |singleEmail|
        personalization.to=Email.new(email: singleEmail, name: "Deportista")
      end
      singleMail.personalizations=personalization
      content = SendGrid::Content.new(type: 'text/plain', value: contenido)
      singleMail.contents = content;
      
  
      # puts clientMail.send(mail) 
      response=clientMail.client.mail._('send').post(request_body: singleMail.to_json)
      puts response.status_code
      puts response.body
      puts response.headers
  end

  # GET /pagos
  # GET /pagos.json
  def index
    if session[:cuser].nil?
      redirect_to "/"
      return
    end

    # Set @paquetes based on user type
    if session[:cuser].tipo != isAdmin()
      @paquetes = Paquete.where(networkid: session[:cuser].networkid)
    else
      @paquetes = Paquete.all
    end

    @socio = params[:socio]

    # Base query for @pagos based on user type
    @pagos = case session[:cuser].tipo
             when isAdmin()
               Pago.all
             when isManager(), isReceptionist()
               Pago.where(networkid: session[:cuser].networkid)
             else
               Pago.where(usuarios_id: session[:cuser].id)
             end

    # Apply filters
    if params[:name].present?
      search_term = "%#{params[:name].strip}%"
      @pagos = @pagos.joins(:usuario).where(
        "CONCAT(usuarios.nombre, ' ', usuarios.apellido) LIKE ? OR usuarios.nombre LIKE ? OR usuarios.apellido LIKE ?",
        search_term, search_term, search_term
      )
    end

    if params[:start_date].present?
      start_date = Date.parse(params[:start_date]).beginning_of_day
      @pagos = @pagos.where('created_at >= ?', start_date)
    end

    if params[:end_date].present?
      end_date = Date.parse(params[:end_date]).end_of_day
      @pagos = @pagos.where('created_at <= ?', end_date)
    end

    # Apply pagination (30 items per page)
    @pagos = @pagos.paginate(page: params[:page], per_page: 30).order(created_at: :desc)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pagos }
    end
  end

  # GET /pagos/1
  # GET /pagos/1.json
  def show
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    @pago = nil
    if session[:cuser].tipo== isAdmin()
      @pago=Pago.find(params[:id])
    elsif session[:cuser].tipo== isReceptionist() or session[:cuser].tipo== isManager()
      @pago=Pago.where(:id=>params[:id],:networkid=>session[:cuser].networkid).last
    else
      @pago=Pago.where(:id=>params[:id],:usuarios_id=>session[:cuser].id).last
    end
    if @pago==nil
      redirect_to "/"
      return
    end
    @establecimiento = Establecimiento.where(:id=>Usuario.find_by_id(@pago.usuarios_id).gymorigin).last
    @isshow=false
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pago }
    end
  end

  # GET /pagos/new
  # GET /pagos/new.json
  def new
    @pago = Pago.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pago }
    end
  end

  # GET /pagos/1/edit
  def edit
    @pago = Pago.find(params[:id])
  end

  # POST /pagos
  # POST /pagos.json
  def create
    if params[:numsocio]=="" or Usuario.where(:numsocio=>params[:numsocio]).last == nil
      redirect_to "/pagos", notice: 'No se ha especificado el socio. Porvafor completa los datos'
      return
    end

    @pago = Pago.new(params[:pago])
    if session[:cuser].tipo != isAdmin() and session[:cuser].tipo != isManager() and session[:cuser].tipo != isReceptionist()
      redirect_to "/"
      return
    end
    nuevaInscripcion=nil
    budgetControl = 0
    useParcialPayment = false
    if params[:monto] != nil and params[:monto] != '' and params[:monto] != '0' and params[:monto] != 0
      budgetControl=params[:monto].to_f
      useParcialPayment = true
    else
      # budget es la suma de la inscripcion y el paquete
      if params[:paquete] != nil and params[:paquete] != '' and params[:paquete] != '0' and params[:paquete] != 0
        budgetControl = Paquete.find_by_id(params[:paquete].to_i).precio.to_f
      end
      if params[:inscripcion]!="0" and params[:inscripcion]!=0 and params[:inscripcion]!=nil
        budgetControl = budgetControl + Paquete.find_by_id(params[:inscripcion].to_i).precio.to_f
      end
    end
    if params[:inscripcion]!="0" and params[:inscripcion]!=0 and params[:inscripcion]!=nil
      # entonces estan pagando una inscripcion tambien, asi que hay que crearla
      nuevaInscripcion=Pago.new
      nuevaInscripcion.networkid=session[:cuser].networkid
      nuevaInscripcion.autorizacion=session[:cuser].id.to_s
      # nuevaInscripcion.gateway="openpay"
      nuevaInscripcion.paquete=params[:inscripcion].to_i
      nuevaInscripcion.pausa=0
      nuevaInscripcion.restantes=0
      nuevaInscripcion.vence=DateTime.now
      nuevaInscripcion.usuarios_id=Usuario.where(:numsocio=>params[:numsocio].to_i).last.id
      inscripcionPaquete=Paquete.find_by_id(params[:inscripcion].to_i)
      nuevaInscripcion.cantidad_original=inscripcionPaquete.precio
      # hay que ver si traemos descuento y paquete simultaneamente de ser asi entonces no aplicamos el descuento aqui
      nuevaInscripcion.descuento=0
      
      if params[:descuento]!=""
        nuevaInscripcion.descuento = params[:descuento]
      end
        #   # no selecciona paquete pero si hay descuento y tiene pago parcial
      if (budgetControl-(inscripcionPaquete.precio.to_f - nuevaInscripcion.descuento.to_f ))>=0
        nuevaInscripcion.cantidad=inscripcionPaquete.precio.to_f - nuevaInscripcion.descuento.to_f
        budgetControl = budgetControl-(inscripcionPaquete.precio.to_f - nuevaInscripcion.descuento.to_f)
      elsif useParcialPayment
        nuevaInscripcion.cantidad=budgetControl
        budgetControl = 0
      else
        redirect_to "/pagos", :notice=>"El pago no procedió por que el descuento es mayor que el pago"
        return
      end
      nuevaInscripcion.save
    end

    @pago.usuarios_id=Usuario.where(:numsocio=>params[:numsocio]).last.id
    @pago.networkid=session[:cuser].networkid
    @pago.descuento=0
    # paqueteSuscrito = Paquete.find_by_id(params[:paquete].to_i)
    if params[:descuento]!="" and params[:descuento]!=0 and (params[:paquete]!="0" and params[:paquete]!=0) and (params[:inscripcion]=="0" or params[:inscripcion]==0)
      # si tiene descuento y tiene paquete pero no tiene inscripcion, proceder con la consideracion del descuento
      @pago.descuento=params[:descuento]
    end
    
    
    if params[:paquete]!="0" and params[:paquete]!=0
      mpaquete=Paquete.find_by_id(params[:paquete].to_i)
      @pago.cantidad_original=mpaquete.precio
      @pago.cantidad=budgetControl
      @pago.paquete=mpaquete.id.to_i
      @pago.vence=DateTime.now+mpaquete.meses.to_i.months
    else
      @pago.cantidad=@pago.cantidad_original
    end
    if params[:meses].to_i.months>0
      # si selecciono mas de los meses, sobreescribir el plan
      @pago.vence=DateTime.now+params[:meses].to_i.months
    end
    # need some refactor here
    # if useBudgetControl 
    if ((budgetControl-(mpaquete.precio.to_f- @pago.descuento.to_f))>=0) or (useParcialPayment and (budgetControl - @pago.descuento.to_f)>0)
      @pago.cantidad = budgetControl - @pago.descuento.to_f
    else
      redirect_to "/pagos", :notice=>"El pago no procedió por que el descuento es mayor que el pago"
      return
    end
    if params[:pago_type]=="1" or params[:pago_type]==1
      @pago.gateway="terminal"
    end
    @pago.autorizacion=session[:cuser].id.to_s
    
    respond_to do |format|
      if params[:paquete]!="0" and params[:paquete]!=0
        @pago.save
      end
      if true
        createReward(params[:socio])
        configm=Notificacione.find_by_id(1)
        if configm.mailtoadmin
          arusuario=[]
          Usuario.where(:tipo=>isManager(), :networkid=>session[:cuser].networkid).each do |singleManager|
            usobjeto=Hash.new
            usobjeto['email']=singleAdmin.email
            usobjeto['name']=singleAdmin.nombre+" "+singleAdmin.apellido
            usobjeto['type']='to'
            arusuario.push(usobjeto)
          end
          sendMailToAddress(arusuario,"Esta recibiendo este mail por que se detectó un pago entrante, la nota del pago la puede encontrar en https://www.sportfit.app/pagos/"+@pago.id.to_s,"Pago en Gym realizado")
        end
        format.html { redirect_to "/pagos", notice: 'El pago se creó correctamente' }
        format.json { render json: @pago, status: :created, location: @pago }
      else
        format.html { render action: "new" }
        format.json { render json: @pago.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pagos/1
  # PUT /pagos/1.json
  def update
    @pago = Pago.find(params[:id])
    if session[:cuser].tipo == isAdmin()
      
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @pago = Pago.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last
    else
      redirect_to "/"
      return
    end
    respond_to do |format|
      if @pago.update_attributes(params[:pago])
        format.html { redirect_to @pago, notice: 'Pago was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @pago.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pagos/1
  # DELETE /pagos/1.json
  def destroy
    @pago = Pago.find(params[:id])
    if session[:cuser].tipo == isAdmin()
      
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @pago = Pago.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last
    else
      redirect_to "/"
      return
    end
    @pago.destroy

    respond_to do |format|
      format.html { redirect_to pagos_url }
      format.json { head :no_content }
    end
  end
end
