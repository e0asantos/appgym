require 'securerandom'
class DevicesController < ApplicationController
  # GET /devices
  # GET /devices.json
  def index
    if session[:cuser].tipo != isAdmin() and session[:cuser].tipo != isManager()
      redirect_to "/"
      return
    end
    @devices = Device.where(:networkid=>session[:cuser].networkid)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @devices }
    end
  end

  # GET /devices/1
  # GET /devices/1.json
  def show
    if session[:cuser].tipo != isAdmin() and session[:cuser].tipo != isManager()
      redirect_to "/"
      return
    end
    @device = Device.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @device }
    end
  end

  # GET /devices/new
  # GET /devices/new.json
  def new
    if session[:cuser].tipo != isAdmin() and session[:cuser].tipo != isManager()
      redirect_to "/"
      return
    end
    @device = Device.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @device }
    end
  end

  # GET /devices/1/edit
  def edit
    if session[:cuser].tipo != isAdmin() and session[:cuser].tipo != isManager()
      redirect_to "/"
      return
    end
    @device = Device.find(params[:id])
  end

  # POST /devices
  # POST /devices.json
  def create
    if session[:cuser].tipo != isAdmin() and session[:cuser].tipo != isManager()
      redirect_to "/"
      return
    end
    deviceAdded = true
    @device = Device.new(params[:device])
    @device.networkid=session[:cuser].networkid
    if @device.uid == nil or @device.uid == ''
      @device.uid = SecureRandom.hex(4)
    end
    # search if there is any prior device with the same serial number
    previousScan = Device.find_by_uid(@device.uid)
    if previousScan != nil
      deviceAdded = false
    else 
      @device.save
    end

    respond_to do |format|
      if deviceAdded
        format.html { redirect_to "/devices", notice: 'Un dispositivo se agregó correctamente' }
        format.json { render json: @device, status: :created, location: @device }
      else
        format.html { redirect_to "/devices", notice: 'No se guardó el dispositivo por qué ya esta registrado.' }
        format.json { render json: @device.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /devices/1
  # PUT /devices/1.json
  def update
    if session[:cuser].tipo != isAdmin() and session[:cuser].tipo != isManager()
      redirect_to "/"
      return
    end
    @device = Device.find(params[:id])
    
    respond_to do |format|
      if @device.update_attributes(params[:device])
        if @device.uid == nil or @device.uid == ''
          @device.uid = SecureRandom.hex(4)
          @device.networkid=session[:cuser].networkid
          @device.save
        end
        format.html { redirect_to "/devices", notice: 'El dispositivo se actualizó correctamente' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @device.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /devices/1
  # DELETE /devices/1.json
  def destroy
    if session[:cuser].tipo != isAdmin() and session[:cuser].tipo != isManager()
      redirect_to "/"
      return
    end
    @device = Device.find(params[:id])
    @device.destroy

    respond_to do |format|
      format.html { redirect_to devices_url }
      format.json { head :no_content }
    end
  end
end
