class CorteCajasController < ApplicationController
  # GET /corte_cajas
  # GET /corte_cajas.json
  def index
    if session[:cuser]==nil
      redirect_to "/login"
      return
    end
    @corte_cajas = CorteCaja.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @corte_cajas }
    end
  end

  # GET /corte_cajas/1
  # GET /corte_cajas/1.json
  def show
    if session[:cuser]==nil
      redirect_to "/login"
      return
    end
    @corte_caja = CorteCaja.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @corte_caja }
    end
  end

  # GET /corte_cajas/new
  # GET /corte_cajas/new.json
  def new
    if session[:cuser]==nil
      redirect_to "/login"
      return
    end
    @corte_caja = CorteCaja.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @corte_caja }
    end
  end

  # GET /corte_cajas/1/edit
  def edit
    @corte_caja = CorteCaja.find(params[:id])
  end

  # POST /corte_cajas
  # POST /corte_cajas.json
  def create
    if session[:cuser]==nil
      redirect_to "/login"
      return
    end
    @corte_caja = CorteCaja.new(params[:corte_caja])

    respond_to do |format|
      if @corte_caja.save
        format.html { redirect_to @corte_caja, notice: 'Corte caja was successfully created.' }
        format.json { render json: @corte_caja, status: :created, location: @corte_caja }
      else
        format.html { render action: "new" }
        format.json { render json: @corte_caja.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /corte_cajas/1
  # PUT /corte_cajas/1.json
  def update
    if session[:cuser]==nil
      redirect_to "/login"
      return
    end
    @corte_caja = CorteCaja.find(params[:id])

    respond_to do |format|
      if @corte_caja.update_attributes(params[:corte_caja])
        format.html { redirect_to @corte_caja, notice: 'Corte caja was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @corte_caja.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /corte_cajas/1
  # DELETE /corte_cajas/1.json
  def destroy
    if session[:cuser]==nil
      redirect_to "/login"
      return
    end
    @corte_caja = CorteCaja.find(params[:id])
    @corte_caja.destroy

    respond_to do |format|
      format.html { redirect_to corte_cajas_url }
      format.json { head :no_content }
    end
  end
end
