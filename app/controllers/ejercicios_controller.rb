class EjerciciosController < ApplicationController
  # GET /ejercicios
  # GET /ejercicios.json
  def index

    @ejercicios = nil
    @wod_roots=nil
    if session[:cuser].tipo != isAdmin()
      @ejercicios=Ejercicio.where("networkid='"+session[:cuser].networkid.to_s+"' or globalavailable=1")
      @wod_roots=WodRoot.where("networkid='"+session[:cuser].networkid.to_s+"' or globalavailable=1")
    else 
      @ejercicios = Ejercicio.all
      @wod_roots=WodRoot.all
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ejercicios }
    end
  end

  # GET /ejercicios/1
  # GET /ejercicios/1.json
  def show
    @ejercicio = Ejercicio.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ejercicio }
    end
  end

  # GET /ejercicios/new
  # GET /ejercicios/new.json
  def new
    @ejercicio = Ejercicio.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ejercicio }
    end
  end

  # GET /ejercicios/1/edit
  def edit
    @ejercicio = Ejercicio.find(params[:id])
  end

  # POST /ejercicios
  # POST /ejercicios.json
  def create
    # Handle file upload if present
    uploaded_filename = process_clip_upload if params[:ejercicio][:clip].present?
  
    @ejercicio = Ejercicio.new(ejercicio_params(uploaded_filename))
    @ejercicio.author_id = session[:cuser].id
    @ejercicio.networkid = session[:cuser].networkid
  
    respond_to do |format|
      if @ejercicio.save
        format.html { redirect_to @ejercicio, notice: 'Ejercicio was successfully created.' }
        format.json { render json: @ejercicio, status: :created, location: @ejercicio }
      else
        format.html { render action: "new", status: :unprocessable_entity }
        format.json { render json: @ejercicio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /ejercicios/1
  # PUT /ejercicios/1.json
  def update
    @ejercicio = Ejercicio.find(params[:id])
    uploaded_filename = process_clip_upload if params[:ejercicio][:clip].present?
    respond_to do |format|
      if @ejercicio.update(ejercicio_params(uploaded_filename))
        format.html { redirect_to @ejercicio, notice: 'Ejercicio was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ejercicio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ejercicios/1
  # DELETE /ejercicios/1.json
  def destroy
    @ejercicio = Ejercicio.find(params[:id])
    @ejercicio.destroy

    respond_to do |format|
      format.html { redirect_to ejercicios_url }
      format.json { head :no_content }
    end
  end
  
  private
  
  def ejercicio_params(uploaded_filename = nil)
    permitted = params.require(:ejercicio).permit(:name, :pesorx, :pesoescalado, :description, :globalavailable, :clip)
    permitted[:clip] = uploaded_filename if uploaded_filename
    permitted
  end
  
  def process_clip_upload
    filename = params[:ejercicio][:clip].original_filename
    path = Rails.root.join("public", "train", filename)
    File.binwrite(path, params[:ejercicio][:clip].read)
    filename # Return the filename to store in the model
  end
  def create_old
    

    if params[:ejercicio][:clip]!=nil
      puts params[:ejercicio][:clip].original_filename
      path = File.join("public/train", params[:ejercicio][:clip].original_filename)
      # write the file
      File.open(path, "wb") { |f| f.write(params[:ejercicio][:clip].read) }
      # limpiamos
      params[:ejercicio][:clip]=params[:ejercicio][:clip].original_filename
      # @ejercicio.clip=params[:clip]
    end
    @ejercicio = Ejercicio.new(params[:ejercicio])
    @ejercicio.author_id=session[:cuser].id
    @ejercicio.networkid=session[:cuser].networkid
    respond_to do |format|
      if @ejercicio.save
        format.html { redirect_to @ejercicio, notice: 'Ejercicio was successfully created.' }
        format.json { render json: @ejercicio, status: :created, location: @ejercicio }
      else
        format.html { render action: "new" }
        format.json { render json: @ejercicio.errors, status: :unprocessable_entity }
      end
    end
  end

  
end
