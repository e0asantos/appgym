class RecompensaObtenidasController < ApplicationController
  # GET /recompensa_obtenidas
  # GET /recompensa_obtenidas.json
  def index
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    @recompensa_obtenidas = RecompensaObtenida.where(:networkid=>session[:cuser].networkid).where("active is NULL")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @recompensa_obtenidas }
    end
  end

  def getMyRewards
    response=RecompensaObtenida.where(:usuarios_id=>session[:cuser].id).where("active is null")
    finalJson=[]
    response.each do |singleReward|
      # singleWod.created_at=singleWod.created_at.strftime("at %I:%M%p")
      hash=singleReward.as_json
      puts singleReward.inspect
      
      reward = Reward.find_by_id(singleReward.rewards_id)
      hash["description"]=reward.descripcion
      hash["title"]=reward.titulo
      hash["place"]= Establecimiento.where(:networkid => reward.networkid).last.nombre
      hash["created_at"]=singleReward.created_at.strftime("%m/%d/%Y")
      finalJson.push(hash)
    end

    respond_to do |format|
      format.json { render json: finalJson.to_json }
    end
  end

  # GET /recompensa_obtenidas/1
  # GET /recompensa_obtenidas/1.json
  def show
    @recompensa_obtenida = RecompensaObtenida.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @recompensa_obtenida }
    end
  end

  # GET /recompensa_obtenidas/new
  # GET /recompensa_obtenidas/new.json
  def new
    @recompensa_obtenida = RecompensaObtenida.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @recompensa_obtenida }
    end
  end

  # GET /recompensa_obtenidas/1/edit
  def edit
    @recompensa_obtenida = RecompensaObtenida.find(params[:id])
  end

  # POST /recompensa_obtenidas
  # POST /recompensa_obtenidas.json
  def create
    @recompensa_obtenida = RecompensaObtenida.new(params[:recompensa_obtenida])

    respond_to do |format|
      if @recompensa_obtenida.save
        format.html { redirect_to @recompensa_obtenida, notice: 'Recompensa obtenida was successfully created.' }
        format.json { render json: @recompensa_obtenida, status: :created, location: @recompensa_obtenida }
      else
        format.html { render action: "new" }
        format.json { render json: @recompensa_obtenida.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /recompensa_obtenidas/1
  # PUT /recompensa_obtenidas/1.json
  def update
    @recompensa_obtenida = RecompensaObtenida.find(params[:id])

    respond_to do |format|
      if @recompensa_obtenida.update_attributes(params[:recompensa_obtenida])
        format.html { redirect_to @recompensa_obtenida, notice: 'Recompensa obtenida was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @recompensa_obtenida.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recompensa_obtenidas/1
  # DELETE /recompensa_obtenidas/1.json
  def destroy
    @recompensa_obtenida = RecompensaObtenida.find(params[:id])
    @recompensa_obtenida.active=1
    @recompensa_obtenida.save
    # @recompensa_obtenida.destroy

    respond_to do |format|
      format.html { redirect_to recompensa_obtenidas_url }
      format.json { head :no_content }
    end
  end
end
