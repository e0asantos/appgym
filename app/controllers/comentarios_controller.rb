class ComentariosController < ApplicationController
  # GET /comentarios
  # GET /comentarios.json
  def index
    @comentarios = Comentario.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @comentarios }
    end
  end

  def getAll
    response=[]
    comentarios=Comentario.where(:wod_root_id=>params[:wod])
    if params.has_key?(:wodItem)
      comentarios=Comentario.where(:wod_root_id=>params[:wod],:wod_item_id=>params[:wodItem])
    end
    comentarios.each do |singleComment|
      comentario=Hash.new
      persona=Usuario.find_by_id(singleComment.usuario_id)
      if persona.nombre==nil
        persona.nombre=""
      end
      if persona.apellido==nil
        persona.apellido=""
      end
      comentario["nombre"]=persona.nombre+" "+persona.apellido
      comentario["comentario"]=singleComment.comentario
      comentario["created_at"]=singleComment.created_at.strftime("%m/%d/%Y")
      comentario["foto"]=persona.foto
      comentario["user_id"]=persona.id
      comentario["id"]=singleComment.id
      response.push(comentario)
    end
    respond_to do |format|
      format.json {render json:response.to_json}
    end
  end

  # GET /comentarios/1
  # GET /comentarios/1.json
  def show
    @comentario = Comentario.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comentario }
    end
  end

  # GET /comentarios/new
  # GET /comentarios/new.json
  def new
    @comentario = Comentario.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @comentario }
    end
  end

  # GET /comentarios/1/edit
  def edit
    @comentario = Comentario.find(params[:id])
  end

  # POST /comentarios
  # POST /comentarios.json
  def create
    @comentario = Comentario.new(comentario_params)
    @comentario.wod_root_id = params[:comentario]["wod_root_id"]
    @comentario.wod_item_id = params[:comentario]["wod_item_id"]
    @comentario.usuario_id=session[:cuser].id
    @comentario.comentario=params[:comentario]["comentario"]
    respond_to do |format|
      if @comentario.save
        format.html { redirect_to @comentario, notice: 'Comentario was successfully created.' }
        format.json { render json: @comentario, status: :created, location: @comentario }
      else
        format.html { render action: "new" }
        format.json { render json: @comentario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /comentarios/1
  # PUT /comentarios/1.json
  def update
    @comentario = Comentario.find(params[:id])

    respond_to do |format|
      if @comentario.update(comentario_params)
        format.html { redirect_to @comentario, notice: 'Comentario was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comentario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comentarios/1
  # DELETE /comentarios/1.json
  def destroy
    if session[:cuser]==nil
      respond_to do |format|
        format.json { render json: "No tienes permisos para borrar este comentario".to_json}
      end
      return
    end
    # verify if the user is the owner of the comment
    @comentario = Comentario.find(params[:id])
    if session[:cuser].tipo == 1
      # admin
    elsif @comentario.usuario_id!=session[:cuser].id
      respond_to do |format|
        format.json { render json: "No tienes permisos para borrar este comentario".to_json}
        format.html { redirect_to "/" }
      end
      return
    end
    @comentario.destroy

    respond_to do |format|
      format.html { redirect_to comentarios_url }
      format.json { render json: "OK".to_json}
    end
  end

  def comentario_params
    permitted = params.require(:comentario).permit(
      :wod_item_id, :wod_root_id,:comentario)
    permitted
  end
end
