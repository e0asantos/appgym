class EventosController < ApplicationController
  # GET /eventos
  # GET /eventos.json
  def index
    @eventos = nil
    if session[:cuser].tipo==1
      @eventos=Evento.all
    else
      @eventos=Evento.where(:networkid=>session[:cuser].networkid)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @eventos }
    end
  end

  def getActiveEvents
    if session[:cuser]==nil
      redirect_to "/"
      return
    end
    @eventos = nil
    if session[:cuser].tipo==1
      @eventos=Evento.where("fecha >= ?",Time.now)
    else
      @eventos=Evento.where("networkid="+session[:cuser].networkid.to_s+" or public=1").where("fecha >= ?",Time.now)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @eventos }
    end
  end

  # GET /eventos/1
  # GET /eventos/1.json
  def show
    if session[:cuser].tipo == isAdmin()
      @evento = Evento.find(params[:id])
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @evento = Evento.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last
    else
      redirect_to "/"
      return
    end
    

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @evento }
    end
  end

  # GET /eventos/new
  # GET /eventos/new.json
  def new
    if session[:cuser].tipo == isAdmin() or session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      
    else
      redirect_to "/"
      return
    end
    @evento = Evento.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @evento }
    end
  end

  # GET /eventos/1/edit
  def edit
    @evento = nil
    if session[:cuser].tipo == isAdmin()
      @evento = Evento.find(params[:id])
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @evento = Evento.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last
    else
      redirect_to "/"
      return
    end
  end

  # POST /eventos
  # POST /eventos.json
  def create
    if params[:evento][:imagen]!=nil
      puts params[:evento][:imagen].original_filename
      path = File.join(Rails.root + "public/images", params[:evento][:imagen].original_filename)
      # write the file
      File.open(path, "w+b") { |f| f.write(params[:evento][:imagen].read) }
      # limpiamos
      params[:evento][:imagen]=params[:evento][:imagen].original_filename
      # @ejercicio.clip=params[:clip]
    end
    @evento = Evento.new(evento_params)
    @evento.networkid=session[:cuser].networkid
    respond_to do |format|
      if @evento.save
        format.html { redirect_to "/eventos", notice: 'Evento se creó exitosamente.' }
        format.json { render json: @evento, status: :created, location: @evento }
      else
        format.html { render action: "new" }
        format.json { render json: @evento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /eventos/1
  # PUT /eventos/1.json
  def update
    if session[:cuser].tipo == isAdmin()
      @evento = Evento.find(params[:id])
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @evento = Evento.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last
    else
      redirect_to "/"
      return
    end
    if params[:evento][:imagen]!=nil
      puts params[:evento][:imagen].original_filename
      path = File.join(Rails.root + "public/images", params[:evento][:imagen].original_filename)
      # write the file
      File.open(path, "w+b") { |f| f.write(params[:evento][:imagen].read) }
      # limpiamos
      params[:evento][:imagen]=params[:evento][:imagen].original_filename
      # @ejercicio.clip=params[:clip]
    end
    @evento = Evento.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last

    respond_to do |format|
      if @evento.update(evento_params)
        format.html { redirect_to @evento, notice: 'Evento was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @evento.errors, status: :unprocessable_entity }
      end
    end
  end

  def evento_params
    params.require(:evento).permit(:evento, :name, :description, :public, :fecha,:imagen)
  end

  # DELETE /eventos/1
  # DELETE /eventos/1.json
  def destroy
    
    if session[:cuser].tipo == isAdmin()
      @evento = Evento.find(params[:id])
    elsif session[:cuser].tipo == isManager() or session[:cuser].tipo == isReceptionist()
      @evento = Evento.where(:id=>params[:id], :networkid=>session[:cuser].networkid).last
    else
      redirect_to "/"
      return
    end
    @evento.destroy

    respond_to do |format|
      format.html { redirect_to eventos_url }
      format.json { head :no_content }
    end
  end
end
