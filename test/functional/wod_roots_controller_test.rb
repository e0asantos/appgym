require 'test_helper'

class WodRootsControllerTest < ActionController::TestCase
  setup do
    @wod_root = wod_roots(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wod_roots)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create wod_root" do
    assert_difference('WodRoot.count') do
      post :create, wod_root: { date: @wod_root.date, description: @wod_root.description, name: @wod_root.name, time: @wod_root.time }
    end

    assert_redirected_to wod_root_path(assigns(:wod_root))
  end

  test "should show wod_root" do
    get :show, id: @wod_root
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @wod_root
    assert_response :success
  end

  test "should update wod_root" do
    put :update, id: @wod_root, wod_root: { date: @wod_root.date, description: @wod_root.description, name: @wod_root.name, time: @wod_root.time }
    assert_redirected_to wod_root_path(assigns(:wod_root))
  end

  test "should destroy wod_root" do
    assert_difference('WodRoot.count', -1) do
      delete :destroy, id: @wod_root
    end

    assert_redirected_to wod_roots_path
  end
end
