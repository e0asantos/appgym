require 'test_helper'

class CorteCajasControllerTest < ActionController::TestCase
  setup do
    @corte_caja = corte_cajas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:corte_cajas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create corte_caja" do
    assert_difference('CorteCaja.count') do
      post :create, corte_caja: { autorizo: @corte_caja.autorizo, fecha_fin: @corte_caja.fecha_fin, fecha_inicio: @corte_caja.fecha_inicio, usuarios_id: @corte_caja.usuarios_id }
    end

    assert_redirected_to corte_caja_path(assigns(:corte_caja))
  end

  test "should show corte_caja" do
    get :show, id: @corte_caja
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @corte_caja
    assert_response :success
  end

  test "should update corte_caja" do
    put :update, id: @corte_caja, corte_caja: { autorizo: @corte_caja.autorizo, fecha_fin: @corte_caja.fecha_fin, fecha_inicio: @corte_caja.fecha_inicio, usuarios_id: @corte_caja.usuarios_id }
    assert_redirected_to corte_caja_path(assigns(:corte_caja))
  end

  test "should destroy corte_caja" do
    assert_difference('CorteCaja.count', -1) do
      delete :destroy, id: @corte_caja
    end

    assert_redirected_to corte_cajas_path
  end
end
