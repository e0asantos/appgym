require 'test_helper'

class BoxSchedulesControllerTest < ActionController::TestCase
  setup do
    @box_schedule = box_schedules(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:box_schedules)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create box_schedule" do
    assert_difference('BoxSchedule.count') do
      post :create, box_schedule: { timetable: @box_schedule.timetable, weekday: @box_schedule.weekday }
    end

    assert_redirected_to box_schedule_path(assigns(:box_schedule))
  end

  test "should show box_schedule" do
    get :show, id: @box_schedule
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @box_schedule
    assert_response :success
  end

  test "should update box_schedule" do
    put :update, id: @box_schedule, box_schedule: { timetable: @box_schedule.timetable, weekday: @box_schedule.weekday }
    assert_redirected_to box_schedule_path(assigns(:box_schedule))
  end

  test "should destroy box_schedule" do
    assert_difference('BoxSchedule.count', -1) do
      delete :destroy, id: @box_schedule
    end

    assert_redirected_to box_schedules_path
  end
end
