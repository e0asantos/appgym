require 'test_helper'

class McardsControllerTest < ActionController::TestCase
  setup do
    @mcard = mcards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mcards)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mcard" do
    assert_difference('Mcard.count') do
      post :create, mcard: { ccv: @mcard.ccv, digits: @mcard.digits, expiration: @mcard.expiration, usuario_id: @mcard.usuario_id }
    end

    assert_redirected_to mcard_path(assigns(:mcard))
  end

  test "should show mcard" do
    get :show, id: @mcard
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mcard
    assert_response :success
  end

  test "should update mcard" do
    put :update, id: @mcard, mcard: { ccv: @mcard.ccv, digits: @mcard.digits, expiration: @mcard.expiration, usuario_id: @mcard.usuario_id }
    assert_redirected_to mcard_path(assigns(:mcard))
  end

  test "should destroy mcard" do
    assert_difference('Mcard.count', -1) do
      delete :destroy, id: @mcard
    end

    assert_redirected_to mcards_path
  end
end
