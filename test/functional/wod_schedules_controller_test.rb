require 'test_helper'

class WodSchedulesControllerTest < ActionController::TestCase
  setup do
    @wod_schedule = wod_schedules(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wod_schedules)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create wod_schedule" do
    assert_difference('WodSchedule.count') do
      post :create, wod_schedule: { hour: @wod_schedule.hour, weekday: @wod_schedule.weekday, wod_id: @wod_schedule.wod_id }
    end

    assert_redirected_to wod_schedule_path(assigns(:wod_schedule))
  end

  test "should show wod_schedule" do
    get :show, id: @wod_schedule
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @wod_schedule
    assert_response :success
  end

  test "should update wod_schedule" do
    put :update, id: @wod_schedule, wod_schedule: { hour: @wod_schedule.hour, weekday: @wod_schedule.weekday, wod_id: @wod_schedule.wod_id }
    assert_redirected_to wod_schedule_path(assigns(:wod_schedule))
  end

  test "should destroy wod_schedule" do
    assert_difference('WodSchedule.count', -1) do
      delete :destroy, id: @wod_schedule
    end

    assert_redirected_to wod_schedules_path
  end
end
