require 'test_helper'

class WodItemsControllerTest < ActionController::TestCase
  setup do
    @wod_item = wod_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wod_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create wod_item" do
    assert_difference('WodItem.count') do
      post :create, wod_item: { reps: @wod_item.reps, time: @wod_item.time, workout_id: @wod_item.workout_id }
    end

    assert_redirected_to wod_item_path(assigns(:wod_item))
  end

  test "should show wod_item" do
    get :show, id: @wod_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @wod_item
    assert_response :success
  end

  test "should update wod_item" do
    put :update, id: @wod_item, wod_item: { reps: @wod_item.reps, time: @wod_item.time, workout_id: @wod_item.workout_id }
    assert_redirected_to wod_item_path(assigns(:wod_item))
  end

  test "should destroy wod_item" do
    assert_difference('WodItem.count', -1) do
      delete :destroy, id: @wod_item
    end

    assert_redirected_to wod_items_path
  end
end
