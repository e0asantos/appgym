require 'test_helper'

class RecompensaObtenidasControllerTest < ActionController::TestCase
  setup do
    @recompensa_obtenida = recompensa_obtenidas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:recompensa_obtenidas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create recompensa_obtenida" do
    assert_difference('RecompensaObtenida.count') do
      post :create, recompensa_obtenida: { id: @recompensa_obtenida.id, rewards_id: @recompensa_obtenida.rewards_id, usuarios_id: @recompensa_obtenida.usuarios_id }
    end

    assert_redirected_to recompensa_obtenida_path(assigns(:recompensa_obtenida))
  end

  test "should show recompensa_obtenida" do
    get :show, id: @recompensa_obtenida
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @recompensa_obtenida
    assert_response :success
  end

  test "should update recompensa_obtenida" do
    put :update, id: @recompensa_obtenida, recompensa_obtenida: { id: @recompensa_obtenida.id, rewards_id: @recompensa_obtenida.rewards_id, usuarios_id: @recompensa_obtenida.usuarios_id }
    assert_redirected_to recompensa_obtenida_path(assigns(:recompensa_obtenida))
  end

  test "should destroy recompensa_obtenida" do
    assert_difference('RecompensaObtenida.count', -1) do
      delete :destroy, id: @recompensa_obtenida
    end

    assert_redirected_to recompensa_obtenidas_path
  end
end
